<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;

class PresupuestoFecha extends Model
{
    protected $table = 'presupuesto_fechas';
    protected $primaryKey = 'id';
    protected $fillable = [
   'fecha',
   'presupuesto_id',
   'presup_numero',
    ];
    


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

     public function presupuesto()
    {
        return $this->belongsTo(Presupuesto::class);
    }
   

   
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_presup')->unique();
            $table->integer('servicio_id');
            $table->string('servicio');
            $table->decimal('servicio_precio', 12, 2);
            $table->decimal('servicio_multiplica', 4, 2);
            $table->integer('subservicio_id');
            $table->string('subservicio');
            $table->decimal('subservicio_precio', 12, 2);
            $table->decimal('subservicio_multiplica', 4, 2);
            $table->integer('provincia_id');
            $table->string('provincia');
            $table->decimal('provincia_precio', 12, 2);
            $table->decimal('provincia_multiplica', 4, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}

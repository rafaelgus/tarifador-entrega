@extends('layouts.app')
@section('content')

<div id="app" class="container pb-5">
    <div class="stepper-horiz">
        <div class="stepper">
            <div class="stepper-icon">
                <span>1</span>
            </div>
            <span class="stepper-text">Definir Servicio</span>
        </div>
        <div class="stepper active">
            <div class="stepper-icon">
                <span>2</span>
            </div>
            <span class="stepper-text">Elegir Fecha</span>
        </div>
        <div class="stepper">
            <div class="stepper-icon">
                <span>3</span>
            </div>
            <span class="stepper-text">Ver Presupuesto</span>
        </div>
        <div class="stepper">
            <div class="stepper-icon">
                <span>4</span>
            </div>
            <span class="stepper-text">Realizar Reserva</span>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Elegir Fecha</div>

                <div class="card-body">
                    <h1>Elegir Fecha</h1>

                    <form method="POST" action="{{ route('opcionfecha.update' , $presup->id) }}">
                        @csrf {{ method_field('PUT') }}
                        <div class="container">
                            <div class="row">
                                <label for="exampleInputDatePicker1">A basic example:</label>
                                <input class="form-control " name="fecha" id="datetimepicker" placeholder="Pick a date" type="date">

                            </div>
                            <div class="">
                                <div class="d-flex flex-row-reverse  bd-highlight">
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-lg mt-3">Siguiente
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card ">
                <div class="card-header">Indicaciones a tener en cuenta:</div>

                <div class="card-body">

                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>



                </div>
            </div>
        </div>
    </div>
    </div
@endsection

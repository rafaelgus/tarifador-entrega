<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Presupuesto;

class OpcionesCocina extends Model
{
    protected $table = 'opciones_cocinas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'coc_arm_int_id',
        'coc_arm_int_name',
        'coc_arm_int_precio',
        'coc_arm_int_multiplica',
        'coc_muebl_int_id',
        'coc_muebl_int_name',
        'coc_muebl_int_precio',
        'coc_muebl_int_multiplica',
        'coc_horno_int_id',
        'coc_horno_int_name',
        'coc_horno_int_precio',
        'coc_horno_int_multiplica',
        'coc_elect_int_id',
        'coc_elect_int_name',
        'coc_elect_int_precio',
        'coc_elect_int_multiplica',
        'coc_azujta_id',
        'coc_azujta_name',
        'coc_azujta_precio',
        'coc_azujta_multiplica',
        'coc_sueljta_id',
        'coc_sueljta_name',
        'coc_sueljta_precio',
        'coc_sueljta_multiplica',
        'presupuesto_id',
        'presup_numero',
        'coc_arm_int_total',
        'coc_mueble_int_total',
        'coc_horno_int_total',
        'coc_elect_int_total',
        'coc_azujta_int_total',
        'coc_sueljta_int_total',
        'coc_secc_presup_total',
    ];



     /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function opcionesCocina()
    {
        return $this->belongsTo(Presupuesto::class);
    }
}


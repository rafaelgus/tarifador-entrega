@extends('layouts.app')
@section('content')
<div id="app" class="container pb-5">
    <div class="stepper-horiz">
        <div class="stepper">
            <div class="stepper-icon">
                <span>1</span>
            </div>
            <span class="stepper-text">Definir Servicio</span>
        </div>
        <div class="stepper ">
            <div class="stepper-icon">
                <span>2</span>
            </div>
            <span class="stepper-text">Elegir Fecha</span>
        </div>
        <div class="stepper active">
            <div class="stepper-icon">
                <span>3</span>
            </div>
            <span class="stepper-text">Ver Presupuesto</span>
        </div>
        <div class="stepper">
            <div class="stepper-icon">
                <span>4</span>
            </div>
            <span class="stepper-text">Realizar Reserva</span>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h1 class="center">Datos del Presupuesto</h1>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="pl-4">Tipo de servicio:
                                <strong>{{ $presupuesto->servicio }}</strong>
                            </p>
                            <p class="pl-4">Tipo de subservicio:
                                <strong>{{ $presupuesto->subservicio }}</strong>
                            </p>
                            <p class="pl-4">Provincia: <span class="pl-2">
                                <strong>{{ $presupuesto->provincia }}</strong>
                            </span>
                            </p>
                            <div>
                                <p class="pl-4">Tipo de Vivienda: <strong>{{ $presupuesto->presupuestoVivienda->tipo_vivienda_name }}</strong>
                                </p>
                                <p class="pl-4">Dimensiones de la vivienda:
                                    <strong>{{ $presupuesto->presupuestoVivienda->dimen_vivienda_name }}</strong> m2
                                </p>
                                <p class="pl-4">Cantidad de estancias:
                                    <strong>{{ $presupuesto->presupuestoVivienda->estanc_vivienda_name }}</strong>
                                </p>

                            </div>
                            <p class="pl-4">Fecha de servicio<span class="pl-2">
                                <strong>{{ Carbon\Carbon::parse($presupuesto->presupuestoFecha->fecha)->format('d-M-Y') }} }}</strong>
                            </span>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="pl-4">Nombre: <span class="pl-2">
                                <strong>{{ $presupuesto->presupuestoSolicitante->nombre}}</strong>
                            </span>
                            </p>
                            <p class="pl-4">Email <span class="pl-2">
                                <strong>{{ $presupuesto->presupuestoSolicitante->email}}</strong>
                            </span>
                            </p>
                            <p class="pl-4">Telefono:
                                <span class="pl-2">
                                    <strong>
                                        {{ $presupuesto->presupuestoSolicitante->telefono }}
                                    </strong>
                                </span>
                            </p>
                            <p class="pl-4">Direccion:
                                <span class="pl-2">
                                    <strong>
                                        {{ $presupuesto->presupuestoSolicitante->direccion }}
                                    </strong>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <h3 class="pt-4">En la limpieza base están incluidos los siguientes servicios:</h3>
            <ul>
                <li>Desplazamientos, útiles, y productos.</li>
                <li>Atención personalizada.</li>
                <li>Seguro de Responsabilidad Civil.</li>
                <li>Un equipo de varios operarios que trabajan de forma simultánea.</li>
                <li>Trabajos realizados en solo una jornada.</li>
            </ul>
            <h3 class="pt-4">Además, está incluida la limpieza de los siguientes elementos cuando los haya en la vivienda:</h3>
            <ul>
                <li>Barrido y/o aspirado y el fregado del suelo de la vivienda.</li>
                <li>Quitar el polvo, telarañas, pelusas etc. de las paredes del inmueble.</li>
                <li>Limpieza del polvo exterior de los muebles de las habitaciones (incluyendo las puertas de los armarios),
                    salón, pasillosy baños sin retirar los objetos.</li>
                <li>La limpieza del vidrio y cristales de los espejos y ventanas normales (con una sola línea de cristales sin
                    contraventanas ni dobles ventanas)</li>
                <li>En la cocina está incluida la limpieza exterior de: encimera y fregadero, armarios, cajones, electrodomésticos
                    y otro mobiliario como sillas y mesas cuando lo haya, la vitrocerámica y los quemadores de gas siempre
                    que estos no estén muy sucios.</li>

            </ul>
            <p>Adicionalmente a lo anterior, usted ha seleccionado los siguientes servicios que puede ver clicando en cada estancia.
                Si quiere modificar alguno de los elementos que ha elegido, puede hacerlo clicando en el botón de abajo de
                la página donde dice “Modificar presupuesto”.
            </p>
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h3 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      COCINA
                    </button>
                        </h3>
                    </div>

                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <ul>
                                <li class="pb-3"><span>Limpieza de las juntas del suelo o unión de las plaquetas:</span><strong class="pl-3">{{ $presupuesto->opcionesCocina->coc_sueljta_name }}</strong></li>
                                <li class="pb-3"><span>Limpieza de las juntas de la pared o unión de las plaquetas:</span><strong class="pl-3">{{ $presupuesto->opcionesCocina->coc_azujta_name }}</strong></li>
                                <li class="pb-3"><span>Retirada de electrodomésticos y embellecedores y limpieza por detrás:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesCocina->coc_elect_int_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza interior de electrodomésticos y si están poco o muy sucios:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesCocina->coc_horno_int_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de la estructura de los cajones y del interior del mueble que los acoge:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesCocina->coc_arm_int_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza interior de armarios y si están llenos o vacíos</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesCocina->coc_muebl_int_name }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h3 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      BAÑOS Y ASEOS
                    </button>
                        </h3>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">

                            <ul>
                                <li class="pb-3"><span>Limpieza de las juntas del suelo o unión de las plaquetas del baño:</span><strong class="pl-3">{{ $presupuesto->opcionesBanoAseo->ban_sueljta_name }}</strong></li>
                                <li class="pb-3"><span>Limpieza de zonas ennegrecidas en sanitarios por hongos o moho:</span><strong class="pl-3">{{ $presupuesto->opcionesBanoAseo->ban_hongos_name}}</strong></li>
                                <li class="pb-3"><span>Limpieza de las juntas de la pared o unión de las plaquetas del baño:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesBanoAseo->ban_azuljta_pared_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de sanitarios, plato de ducha y/o bañera:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesBanoAseo->ban_sanit_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de la mampara en caso de que tuviera:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesBanoAseo->ban_mamp_name}}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza interior de armarios del baño y/o aseo y cuántos son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesBanoAseo->ban_int_arm_name }}</strong>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h3 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      DORMITORIOS, SALON Y PASILLOS
                    </button>
                        </h3>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">

                            <ul>
                                <li class="pb-3"><span>Limpieza de las juntas del suelo en el caso de suelos de gres, mármol o terrazo:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_sueljta_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Retirada de muebles, armarios y limpieza de la parte posterior del mueble, paredes y suelo donde se encuentren:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_mueb_arma_name}}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de libros, adornos, aparatos electrónicos, marcos de foto y cuántos son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_libro_ador_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza del interior de muebles bajos como aparadores, cómodas o cajoneras y cuántos son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_mueb_bajo_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Aplicación de cera líquida en el suelo de parquet:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_suel_parq_name}}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza del interior de la vitrina del salón y/o mueble mural y cuántos son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_vitr_mur_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza del interior de armarios roperos, armarios empotrados y cuántos son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_arm_rop_int_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Fregado de las puertas de la vivienda y cuántas son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_fre_puertas_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Aspirado y vaporización de alfombras y cuántas son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_asp_vap_alfomb_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Aspirado y vaporización de sofás, sillones y colchones:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_asp_vap_colch_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de enchufes, interruptores, radiadores, rodapiés, salidas de aire acondicionado y apliques/lámparas:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesDormitorio->dorm_ench_int_name }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading4">
                        <h3 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                      VENTANAS Y CERRAMIENTOS EXTERIORES
                                    </button>
                        </h3>
                    </div>
                    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                        <div class="card-body">

                            <ul>
                                <li class="pb-3"><span>Limpieza de ventanas grandes, miradores o puertas de cristal y cuántas son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesVentana->vent_panoramica_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de doble o contra ventanas y cuántas son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesVentana->vent_cont_vent_name}}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de marcos y raíles de ventanas normales y cuántos son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesVentana->vent_marco_railes_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de persianas venecianas o mallorquinas y cuántas son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesVentana->vent_persianas_venec_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza interior de las persianas y cuántas son:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesVentana->vent_persianas_name}}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading5">
                        <h3 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                TERRAZAS, EXTERIORES Y TENDEDEROS
                            </button>
                        </h3>
                    </div>
                    <div id="collapse5" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                        <div class="card-body">

                            <ul>
                                <li class="pb-3"><span>Barrido, aspirado y fregado del suelo de la terraza:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesTerraza->terra_suelo_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de barandilla y/o perímetro de la terraza:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesTerraza->terra_baranda_name}}</strong>
                                </li>
                                <li class="pb-3"><span>Limpieza de armarios de la terraza por dentro:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesTerraza->terra_arm_int_ext_name }}</strong>
                                </li>
                                <li class="pb-3"><span>Cepillado de paredes quitando polvo, telarañas y pelusas:</span>
                                    <strong class="pl-3">{{ $presupuesto->opcionesTerraza->terra_cepill_pared_name }}</strong>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <div class="total-border pt-5">
                <div class="row " style="border: solid 2px #4caf50">
                    <div class="col-md-8 pt-3">
                        <h4>
                            Atención, los precios varían en función de la demanda y de la fecha elegida. Si reserva ahora su presupuesto es de:
                        </h4>
                    </div>
                    <div style=" font-size:4em" class="col-md-4 pt-2 d-flex flex-row-reverse">

                        <strong>{{ $presupuesto->total_presupuesto}}  €</strong>
                    </div>
                </div>
            </div>
            <div class="pt-5">
                <div class="row">
                    <div class="col-md-4  pt-5">
                        <div class="pb-3">
                            <a href="{{ route('opcionfecha.index', $presup->id) }}">Deseo cambiar la fecha</a>
                        </div>
                        <a class="btn btn-dark" href="{{ route('opcionpresupuesto.index', $presup->id) }}">MODIFICAR PRESUPUESTO</a>
                    </div>
                    <div class="col-md-8 pt-5 d-flex flex-row-reverse">
                        <button class="btn btn-dark"> Reserve ahora con 40€ y cancele gratis</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

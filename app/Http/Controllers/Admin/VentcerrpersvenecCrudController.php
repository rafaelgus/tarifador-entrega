<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\VentcerrpersvenecRequest as StoreRequest;
use App\Http\Requests\VentcerrpersvenecRequest as UpdateRequest;

/**
 * Class VentcerrpersvenecCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class VentcerrpersvenecCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\Ventcerrpersvenec');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/ventcerrpersvenec');
        $this->crud->setEntityNameStrings('Persiana Veneciana', 'Persianas Venecianas');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

         //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Nombre",
            'type' => 'text'
        ]);
        $this->crud->addColumn([
            'name' => 'precio',
            'label' => "Precio",
            'type' => 'number',
             // 'prefix' => "$",
            'suffix' => " EUR",
            'decimals' => 2,
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Nombre',
            'type' => 'text',

        ]);
        $this->crud->addField([
            'name' => 'precio',
            'label' => 'Precio',
            'type' => 'number',
            'suffix' => " EUR",
            'decimals' => 2,

        ]);

        // add asterisk for fields that are required in VentcerrpersvenecRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}

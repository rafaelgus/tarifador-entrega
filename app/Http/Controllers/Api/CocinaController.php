<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CocinaInteriorCajonesArmario;
use App\Models\CocinaInteriorCajonMueble;
use App\Models\CocinaInteriorElectrAmbiente;
use App\Models\CocinaInteriorHornoNevera;
use App\Models\CocinaJtaAzulejos;
use App\Models\CocinaJtasuelo;

class CocinaController extends Controller
{
    public function cocinainteriorcajones()
    {
        return CocinaInteriorCajonesArmario::get(['id', 'name', 'precio']);
    }
    public function cocinainteriormueble()
    {
        return CocinaInteriorCajonMueble::get(['id', 'name', 'precio']);
    }
    public function cocinainteriorelectr()
    {
        return CocinaInteriorElectrAmbiente::get(['id', 'name', 'precio']);
    }
     public function cocinainteriorhorno()
    {
        return CocinaInteriorHornoNevera::get(['id', 'name', 'precio']);
    }
     public function cocinainteriorazulejo()
    {
        return CocinaJtaAzulejos::get(['id', 'name', 'precio']);
    }
     public function cocinainteriorsuelo()
    {
        return CocinaJtaAzulejos::get(['id', 'name', 'precio']);
    }

}

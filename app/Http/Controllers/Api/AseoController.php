<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\BanoHongos;
use App\Models\BanoIntCajArm;
use App\Models\BanoJtaAzulejo;
use App\Models\BanoJtaSuelo;
use App\Models\BanoMampara;
use App\Models\BanoPlatoDucha;

class AseoController extends Controller
{
    public function banohongos()
    {
        return BanoHongos::get(['id', 'name', 'precio']);
    }
    public function banointcajarm()
    {
        return BanoIntCajArm::get(['id', 'name', 'precio']);
    }
    public function banojtaazulejo()
    {
        return BanoJtaAzulejo::get(['id', 'name', 'precio']);
    }
    public function banojtasuelo()
    {
        return BanoJtaSuelo::get(['id', 'name', 'precio']);
    }
    public function banomampara()
    {
        return BanoMampara::get(['id', 'name', 'precio']);
    }
    public function banoplatoducha()
    {
        return BanoMampara::get(['id', 'name', 'precio']);
    }
}

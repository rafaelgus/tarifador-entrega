<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;

class PresupuestoVivienda extends Model
{
    protected $table = 'presupuesto_viviendas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tipo_vivienda_id',
        'tipo_vivienda_name',
        'tipo_vivienda_precio',
        'tipo_vivienda_multiplica',
        'dimen_vivienda_id',
        'dimen_vivienda_name',
        'dimen_vivienda_precio',
        'dimen_vivienda_multiplica',
        'estanc_vivienda_id',
        'estanc_vivienda_name',
        'estanc_vivienda_precio',
        'estanc_vivienda_multiplica',
        'presupuesto_id',
        'presup_numero',
        'vivi_secc_presup_total'
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function presupuestoVivienda()
    {
        return $this->belongsTo(Presupuesto::class);
    }
}

@extends('layouts.app')
@section('content')
<div id="app" class="container pb-5">
    <div class="stepper-horiz">
        <div class="stepper">
            <div class="stepper-icon">
                <span>1</span>
            </div>
            <span class="stepper-text">Definir Servicio</span>
        </div>
        <div class="stepper ">
            <div class="stepper-icon">
                <span>2</span>
            </div>
            <span class="stepper-text">Elegir Fecha</span>
        </div>
        <div class="stepper active">
            <div class="stepper-icon">
                <span>3</span>
            </div>
            <span class="stepper-text">Ver Presupuesto</span>
        </div>
        <div class="stepper">
            <div class="stepper-icon">
                <span>4</span>
            </div>
            <span class="stepper-text">Realizar Reserva</span>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ingresa tus datos</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('solicitante.update' , $presup->id) }}">
                        @csrf {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombres y Apellidos</label>
                            <input type="text" name="nombre" class="form-control" id="nombre" aria-describedby="nameHelp" placeholder="Escriba su nombre completo">
                            <small id="nameHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email </label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Escriba su correo">
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Numero Telefonico</label>
                            <input type="tel" name="telefono" class="form-control" id="nombre" aria-describedby="phoneHelp" placeholder="Escriba su numero telefonico">
                            <small id="phoneHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Direccion de servicio</label>
                            <input type="text-area" name="direccion" class="form-control" id="nombre" aria-describedby="adressHelp" placeholder="Escriba su la direccion donde se prestara el servicio">
                            <small id="adressHelp" class="form-text text-muted"></small>
                        </div>


                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

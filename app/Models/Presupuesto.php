<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\PresuestoVivienda;
use App\Models\OpcionesCocina;
use App\Models\OpcionesBanoAseo;
use App\Models\OpcionesDormitorio;
use App\Models\OpcionesVentana;
use App\Models\OpcionesTerraza;
use App\Models\PresupuestoFecha;
use App\Models\PresuestoSolicitante;

class Presupuesto extends Model
{
    protected $table = 'presupuestos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'numero_presup',
        'servicio_id',
        'servicio',
        'servicio_precio',
        'subservicio_id',
        'subservicio',
        'subservicio_precio',
        'provincia_id',
        'provincia',
        'provincia_precio',
        'secc_ini_presup_total',
        'total_presupuesto'
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function presupuestoVivienda()
    {
        return $this->hasOne(PresupuestoVivienda::class);
    }

    public function opcionesCocina()
    {
        return $this->hasOne(OpcionesCocina::class);
    }

    public function opcionesBanoAseo()
    {
        return $this->hasOne(OpcionesBanoAseo::class);
    }

    public function opcionesDormitorio()
    {
        return $this->hasOne(OpcionesDormitorio::class);
    }

    public function opcionesVentana()
    {
        return $this->hasOne(OpcionesVentana::class);
    }

    public function opcionesTerraza()
    {
        return $this->hasOne(OpcionesTerraza::class);
    }

    public function presupuestoFecha()
    {
        return $this->hasOne(PresupuestoFecha::class);
    }

    public function presupuestoSolicitante()
    {
        return $this->hasOne(PresupuestoSolicitante::class);
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\OpcionesTerraza;
use Illuminate\Http\Request;

class OpcionesTerrazaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OpcionesTerraza  $opcionesTerraza
     * @return \Illuminate\Http\Response
     */
    public function show(OpcionesTerraza $opcionesTerraza)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OpcionesTerraza  $opcionesTerraza
     * @return \Illuminate\Http\Response
     */
    public function edit(OpcionesTerraza $opcionesTerraza)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OpcionesTerraza  $opcionesTerraza
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpcionesTerraza $opcionesTerraza)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpcionesTerraza  $opcionesTerraza
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpcionesTerraza $opcionesTerraza)
    {
        //
    }
}

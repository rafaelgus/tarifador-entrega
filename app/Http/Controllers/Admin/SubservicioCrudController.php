<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SubservicioRequest as StoreRequest;
use App\Http\Requests\SubservicioRequest as UpdateRequest;

use App\Models\Servicio;

/**
 * Class SubservicioCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SubservicioCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\Subservicio');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/subservicio');
        $this->crud->setEntityNameStrings('subservicio', 'subservicios');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        $this->crud->setColumns(['name']);

        $this->crud->addField([    // TEXT
            'name' => 'name',
            'label' => 'Nombre',
            'type' => 'text',
            'placeholder' => 'Your title here',
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Servicio',
            'type' => 'select2',
            'name' => 'servicio_id',
            'entity' => 'servicio',
            'attribute' => 'name',
            'model' => "App\Models\Servicio",
        ]);
          $this->crud->addField([
            'name' => 'precio',
            'label' => 'Precio base',
            'type' => 'number',

        ]);
          $this->crud->addField([
            'name' => 'multiplica',
            'label' => 'Factor de multiplicación',
            'type' => 'number',

        ]);

        // add asterisk for fields that are required in SubservicioRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}

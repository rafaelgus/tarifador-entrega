<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestoViviendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuesto_viviendas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_vivienda_id');
            $table->string('tipo_vivienda_name');
            $table->decimal('tipo_vivienda_precio', 12, 2);
            $table->decimal('tipo_vivienda_multiplica', 4, 2);
            $table->integer('dimen_vivienda_id');
            $table->string('dimen_vivienda_name');
            $table->decimal('dimen_vivienda_precio', 12, 2);
            $table->decimal('dimen_vivienda_multiplica', 4, 2);
            $table->integer('estanc_vivienda_id');
            $table->string('estanc_vivienda_name');
            $table->decimal('estanc_vivienda_precio', 12, 2);
            $table->decimal('estanc_vivienda_multiplica', 4, 2);
            $table->integer('presupuesto_id');
            $table->string('presup_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuesto_viviendas');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\OpcionesDormitorio;
use Illuminate\Http\Request;

class OpcionesDormitorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OpcionesDormitorio  $opcionesDormitorio
     * @return \Illuminate\Http\Response
     */
    public function show(OpcionesDormitorio $opcionesDormitorio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OpcionesDormitorio  $opcionesDormitorio
     * @return \Illuminate\Http\Response
     */
    public function edit(OpcionesDormitorio $opcionesDormitorio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OpcionesDormitorio  $opcionesDormitorio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpcionesDormitorio $opcionesDormitorio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpcionesDormitorio  $opcionesDormitorio
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpcionesDormitorio $opcionesDormitorio)
    {
        //
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Presupuesto;

class OpcionesBanoAseo extends Model
{
    protected $table = 'opciones_bano_aseos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'ban_int_arm_id',
        'ban_int_arm_name',
        'ban_int_arm_precio',
        'ban_int_arm_multiplica',
        'ban_mamp_id', 'ban_mamp_name',
        'ban_mamp_precio',
        'ban_mamp_multiplica',
        'ban_sanit_id',
        'ban_sanit_name',
        'ban_sanit_precio',
        'ban_sanit_multiplica',
        'ban_azuljta_pared_id',
        'ban_azuljta_pared_name',
        'ban_azuljta_pared_precio',
        'ban_azuljta_pared_multiplica',
        'ban_hongos_id',
        'ban_hongos_name',
        'ban_hongos_precio',
        'ban_hongos_multiplica',
        'ban_sueljta_id',
        'ban_sueljta_name',
        'ban_sueljta_precio',
        'ban_sueljta_multiplica',
        'presupuesto_id',
        'presup_numero',
        'ban_secc_presup_total'
    ];





     /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function opcionesBanoAseo()
    {
        return $this->belongsTo(Presupuesto::class);
    }
}

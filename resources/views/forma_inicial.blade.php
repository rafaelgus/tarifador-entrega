@extends('layouts.app')
@section('content')

<div class="container m-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Presupuesta</div>
                <form method="POST" action="/guardarpresupuesto">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label for="servicio_id">Servicio</label>
                            <select id="servicio_id" name="servicio" class="form-control" required>
                                    <option value="">Selecciona un Servicio</option>
                                    <option></option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label>Sub Servicio</label>
                            <select id="subservicio_id" name="subservicio" class="form-control" required>
                            <option  value="">Selecciona un Sub Servicio</option>
                            <option></option>
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="region_id">Provincia</label>
                            <select name="region" id="region_id" class="form-control" required>
                            <option value="">Selecciona provincia donde se prestara el servicio</option>
                            <option  ></option>
                        </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Siguiente</button>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;

class OpcionesDormitorio extends Model
{
    protected $table = 'opciones_dormitorios';
    protected $primaryKey = 'id';
    protected $fillable = [
        'dorm_ench_int_id',
        'dorm_ench_int_name',
        'dorm_ench_int_precio',
        'dorm_ench_int_multiplica',
        'dorm_asp_vap_colch_id',
        'dorm_asp_vap_colch_name',
        'dorm_asp_vap_colch_precio', '
        dorm_asp_vap_colch_multiplica',
        'dorm_asp_vap_alfomb_id',
        'dorm_asp_vap_alfomb_name',
        'dorm_asp_vap_alfomb_precio',
        'dorm_asp_vap_alfomb_multiplica',
        'dorm_fre_puertas_id',
        'dorm_fre_puertas_name',
        'dorm_fre_puertas_precio',
        'dorm_fre_puertas_multiplica',
        'dorm_arm_rop_int_id',
        'dorm_arm_rop_int_name',
        'dorm_arm_rop_int_precio',
        'dorm_arm_rop_int_multiplica',
        'dorm_vitr_mur_id',
        'dorm_vitr_mur_name',
        'dorm_vitr_mur_precio',
        'dorm_vitr_mur_multiplica',
        'dorm_suel_parq_id',
        'dorm_suel_parq_name',
        'dorm_suel_parq_precio',
        'dorm_suel_parq_multiplica',
        'dorm_mueb_bajo_id',
        'dorm_mueb_bajo_name',
        'dorm_mueb_bajo_precio',
        'dorm_mueb_bajo_multiplica',
        'dorm_mueb_arma_id',
        'dorm_mueb_arma_name',
        'dorm_mueb_arma_precio',
        'dorm_mueb_arma_multiplica',
        'dorm_sueljta_id',
        'dorm_sueljta_name',
        'dorm_sueljta_precio',
        'dorm_sueljta_multiplica',
        'presupuesto_id',
        'presup_numero',
        'dorm_secc_presup_total'
    ];



     /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function opcionesDormitorio()
    {
        return $this->belongsTo(Presupuesto::class);
    }

}

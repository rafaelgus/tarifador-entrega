/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./dynamic-dropdown');


window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('initialform', require('./components/InitialForm.vue'));
Vue.component('presupuesto', require('./components/presupuesto.vue'));



import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueFormWizard from 'vue-form-wizard'
import {
    FormWizard,
    TabContent
} from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)
Vue.use(VueFormWizard)





Vue.use(ElementUI);

const app = new Vue({
    el: '#app',


});

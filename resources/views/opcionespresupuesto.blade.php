@extends('layouts.app')
@section('content')
<div id="app" class="container pb-5">
    <div class="stepper-horiz">
        <div class="stepper active">
            <div class="stepper-icon">
                <span>1</span>
            </div>
            <span class="stepper-text">Definir Servicio</span>
        </div>
        <div class="stepper ">
            <div class="stepper-icon">
                <span>2</span>
            </div>
            <span class="stepper-text">Elegir Fecha</span>
        </div>
        <div class="stepper">
            <div class="stepper-icon">
                <span>3</span>
            </div>
            <span class="stepper-text">Ver Presupuesto</span>
        </div>
        <div class="stepper">
            <div class="stepper-icon">
                <span>4</span>
            </div>
            <span class="stepper-text">Realizar Reserva</span>
        </div>
    </div>
</div>
<form method="POST" action="{{ route('actualizaropciones.presupuesto' , $presup->id) }}">
    @csrf {{ method_field('PUT') }}

    <div class="container pb-3">
        <div class="card ">
            <div class="card-header">
                <div class="p-1">
                    <p class="p-1">Que tipo de vivienda es? <a href="#" data-toggle="popover" data-placement="bottom" data-content="Indique si el tipo de vivienda es Piso, Casa, Chalet, Duplex, Atico">Mas informacion</a>
                    </p>
                    <br>
                    <div class="btn-group-toggle" data-toggle="buttons">
                        @foreach ($viviendatipo as $vivienda)
                        <label class="btn btn-success btn-lg p-2">
                                    <input type="radio" name="vivienda"  class="form-control" value= {{ $vivienda->id. '|' .$vivienda->tipo. '|' .$vivienda->precio. '|' .'1' }} id="option1" autocomplete="off" required > {{$vivienda->tipo}}
                                </label> @endforeach
                    </div>
                </div>
                <div class="p-3">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="p-1">
                    <p class="p-1">¿Cuántos metros2 tiene la vivienda?<a href="#" data-toggle="popover" data-placement="bottom" data-content="Indique los m2 utiles de su vivienda, incluyendo la suma de todas las terrazas y balcones, sta suma no debe ser mayor a 20 m2">Mas informacion</a>
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="dimensionFormControlSelect1">Dimensiones</label>
                                <select placeholder="Pick a size..." class="form-control" name="dimensiones" id="dimensionFormControlSelect1" required>
                                        <option selected >Selecciona de la lista</option>
                                        @foreach ($dimension as $dimen)
                                            <option value={{ $dimen->id. '|' .$dimen->dimension. '|' .$dimen->precio }}>{{ $dimen->dimension }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <p class="p-2">Si su vivienda tiene menos de 35m2, más de 180m2 o su terraza y/o balcones suman más de 20m2
                                clique aquí para un presupuesto personalizado. </p>
                        </div>
                    </div>
                </div>
                <div class="p-3">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="p-1">
                    <p class="p-1">¿Cuántas estancias de todo tipo hay en la vivienda? <a href="#" data-toggle="popover" data-placement="bottom"
                            data-content="Indique aquí el número de estancias de su vivienda entendiendo por estancia cada habitación, dormitorio, salón, cocina, baños, terraza, tendederos o galerías, lavadero, despensas etc.">Mas informacion</a>
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="estanciasFormControlSelect1">Estancias</label>
                                <select class="form-control" name="estancias" id="estanciasFormControlSelect1" required>
                                        <option selected >Selecciona de la lista</option>
                                        @foreach ($estancias as $estancia)
                                            <option value="{{ $estancia->id. '|' .$estancia->cantidad. '|' .$estancia->precio. '|' .'1' }}">{{ $estancia->cantidad }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-3">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    Continuar
                    </button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link " type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        COCINA
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class=" p-1">
                            <p class=" p-1">La limpieza exterior de los armarios y cajones ya está incluida en la limpieza base, ¿quiere
                                además que se limpie su interior? Y en el caso de querer, ¿estos armarios y cajones están
                                llenos o vacíos de enseres? <a href="#" data-toggle="popover" data-placement="bottom" title="Some content inside the popover"
                                    data-content="Se limpia el polvo y suciedad de los armarios y, en caso de estar llenos de objetos o enseres, se vacian para poder limpiar su interior asi, como los cajones, y posteriormente, colocar dichos enseres en su lugar">Mas informacion</a>
                            </p>
                            <div class="btn-group-toggle" data-toggle="buttons">
                                @foreach ($armariococ as $armario)
                                <label class="btn btn-success  m-3">
                                            <input type="radio" name="coc_arm_int"  value= {{ $armario->id. '|' .$armario->name. '|'. $armario->precio. '|' .'1' }} data-id= {{ $armario->id }} data-multiple="1" id="option1" autocomplete="off" required > {{$armario->name}}
                                        </label> @endforeach
                            </div>
                            <div class="p-3">
                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class=" p-1">
                                <p class=" p-1">La limpieza exterior de los cajones del mobiliario ya está incluida en la limpieza base,
                                    ¿quiere además que los cajones sean extraídos de su mueble y se limpie tanto el interior
                                    del cajón como el de su mueble? <a href="#" data-toggle="popover" data-placement="bottom"
                                        title="Some content inside the popover" data-content="Limpieza del polvo y suciedad acumulada en la estructura de los cajones asi; como sus railes, y el interior del propio cajon.  No se desmontaran si estan atornillados y se limpiaran siempre que su extraccion sea facil">Mas informacion</a>
                                </p>
                                <div class=" btn-group-toggle" data-toggle="buttons">
                                    @foreach ($cajoncoc as $cajon)
                                    <label class="btn btn-success  m-3">
                                    <input type="radio" name="coc_muebl_int"  value= {{ $cajon->id. '|' .$cajon->name. '|' .$cajon->precio. '|' .'1' }} id="coc_muebl_int1" autocomplete="off" required> {{$cajon->name}}
                                    </label> @endforeach
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class=" p-1">
                                    <p class=" p-1">La limpieza exterior de horno, campana, nevera y microondas ya está incluida, ¿quiere
                                        que también se limpie su interior? Y en el caso de querer limpiarlos, ¿cómo se encuentran?
                                        Poco sucio es cuando hay algunos restos de comida y no están endurecidos, muy sucio
                                        lleva más de un mes sin limpiarse a fondo y los restos de comida están endurecidos.
                                        <a href="#" data-toggle="popover" data-placement="bottom" title="Some content inside the popover" data-content="Se desmontan filtros/rejillas de la campana para eliminar la grasa acumulada y reseca, igual con el horno y su estructura interior. Se limpia la nevera, baldas, puerta y sus gomas, etc. Atencion, cualquier electrodomestico muy degradado, y en particular los quemadores de gas y el horno, no se garantiza en absoluto que vayan a quedar limpios e impolutos">Mas informacion</a>
                                    </p>
                                    <br>
                                    <div class=" btn-group-toggle" data-toggle="buttons">
                                        @foreach ($hornococ as $horno)
                                        <label class="btn btn-success  m-3">
                                        <input type="radio" name="coc_horno_int" value= {{ $horno->id. '|' .$horno->precio. '|' .'1' }} id="coc_horno_int" autocomplete="off"  required> {{$horno->name}}
                                    </label> @endforeach
                                    </div>
                                    <div class="p-3">
                                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="p-1">
                                        <p class="p-1">¿Quiere que los electrodomésticos y los embellecedores sean retirados y se limpien
                                            debajo de éstos y bajo los muebles, además del suelo y paredes donde están instalados
                                            y encajados?
                                            <a href="#" data-toggle="popover" data-placement="bottom" title="Some content inside the popover" data-content="Siempre que sea posible y no esten atornillados, se mueven los electrodomesticos para limpiar la parte posterior, asi como la zona en la que han estado apoyados desde su instalacion (suelos y paredes que suelen estar muy sucios">Mas informacion</a>
                                        </p>
                                        <br>
                                        <div class="btn-group-toggle" data-toggle="buttons">
                                            @foreach ($electrococ as $electro)
                                            <label class="btn btn-success  m-3">
                                <input type="radio" name="coc_elect_int"  value= {{ $electro->id. '|' .$electro->name. '|' .$electro->precio. '|' .'1' }} id="option1" autocomplete="off" required> {{$electro->name}}
                            </label> @endforeach
                                        </div>
                                        <div class="p-3">
                                            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="p-1">
                                        <p class="p-1">El fregado de azulejos ya está incluido en la limpieza base, ¿quiere además que se
                                            limpien las juntas de los azulejos (unión de las plaquetas)? <a href="#" data-toggle="popover"
                                                data-trigger="hover" data-placement="bottom" title="Some content inside the popover"
                                                data-content="Las juntas son la union de cemento o yeso de los azulejos. Podremos usar tecnicas de cepillado o vaporeta segun los operarios lo consideren">Mas informacion</a>
                                        </p>
                                        <br>
                                        <div class="btn-group-toggle" data-toggle="buttons">
                                            @foreach ($azulejococ as $azulejo)
                                            <label class="btn btn-success  m-3">
                                     <input type="radio" name="coc_azujta"  value= {{ $azulejo->id. '|' .$azulejo->name. '|' .$azulejo->precio. '|' .'1' }} id="option1" autocomplete="off" required > {{$azulejo->name}}
                                </label> @endforeach
                                        </div>
                                        <div class="p-3">
                                            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="p-1">
                                            <p class="p-1">El barrido y/o aspirado y el fregado del suelo ya está incluido en la limpieza
                                                base, ¿quiere además que se limpien las juntas del suelo? <a href="#" data-toggle="popover"
                                                    data-placement="bottom" title="Some content inside the popover" data-content="Las juntas son la union de cemento o yeso de la plaqueta o loseta. Podremos usar tecnicas de cepillado o vaporeta segun los operarios lo consideren">Mas informacion</a>
                                            </p>
                                            <br>
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                @foreach ($jtacoc as $jtasuelo)
                                                <label class="btn btn-success  m-3">
                            <input type="radio" name="coc_sueljta"  value= {{ $jtasuelo->id. '|' .$jtasuelo->name. '|' .$jtasuelo->precio. '|' .'1' }} id="option1" autocomplete="off" required> {{$jtasuelo->name}}
                        </label> @endforeach
                                            </div>
                                        </div>
                                        <div class="p-3">
                                            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    Continuar
                    </button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                aria-controls="collapseTwo">
                        BAÑOS Y ASEOS
                        </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="p-1">
                                <p class="p-1">¿Quiere que se limpie el interior de los armarios y cajones? Si es así, ¿cuántos quiere que
                                    se limpien? <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom"
                                        data-content="colocar texto">Mas informacion</a>
                                </p>
                                <div class="btn-group-toggle" data-toggle="buttons">
                                    @foreach ($banocaj as $cajn)
                                    <label for=" ban_int_arm" class="btn btn-success  m-3">
                                    <input type="radio" name="ban_int_arm"  value= {{ $cajn->id. '|' .$cajn->name. '|' .$cajn->precio. '|' .'1' }} id="ban_int_arm" autocomplete="off" required> {{$cajn->name}}
                                </label> @endforeach
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="p-1">
                                    <p class="p-1">
                                        La limpieza de espejos ya está incluida en la limpieza base, ¿quiere además que se limpie la mampara de la bañera o la ducha,
                                        si tuviera?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion>
                                </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($banomampara as $mampara)
                                        <label for="ban_mamp" class="btn btn-success  m-3">
                                        <input type="radio" name="ban_mamp"  value= {{ $mampara->id. '|' .$mampara->name. '|' .$mampara->precio. '|' .'1' }} id="ban_mamp" autocomplete="off" required> {{$mampara->name}}
                                    </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpien los sanitarios, plato de ducha y/o bañeras?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($banoducha as $ducha)
                                        <label for="ban_sanit" class="btn btn-success  m-3">
                                            <input type="radio" name="ban_sanit"  value= {{ $ducha->id. '|' .$ducha->name. '|' .$ducha->precio. '|' .'1' }} id="ban_sanit" autocomplete="off" required> {{$ducha->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="p1">
                                    <p class="p1">El fregado de azulejos de las paredes ya está incluido en la limpieza base, ¿quiere además
                                        que se limpien las juntas de la pared, en el caso que fueran de azulejos?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($banoazulejo as $jtaazulejo)
                                        <label for="ban_azuljta_pared" class="btn btn-success  m-3">
                                            <input type="radio" name="ban_azuljta_pared"  value= {{ $jtaazulejo->id.'|'.$jtaazulejo->name.'|'.$jtaazulejo->precio.'|'.'1' }} id="ban_azuljta_pared" autocomplete="off" required> {{$jtaazulejo->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p1">
                                    <p class="p1">Quiere limpiar las zonas ennegrecidas en sanitarios por hongos o moho en caso de que
                                        tuviera? Si desea realizar esta limpieza, tenga en cuenta que no se retirará la silicona.
                                        <a data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($banohongo as $hongo)
                                        <label for="ban_hongos" class="btn btn-success  m-3">
                                            <input type="radio" name="ban_hongos"  value= {{ $hongo->id.'|'.$hongo->name.'|'.$hongo->precio.'|'.'1' }} id="ban_hongos" autocomplete="off" required> {{$hongo->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">El barrido y/o aspirado y el fregado del suelo ya está incluido en la limpieza base,
                                        ¿quiere además que se limpien las juntas del suelo?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($banosuelo as $bsuelo)
                                        <label for="ban_sueljta" class="btn btn-success  m-3">
                                            <input type="radio" name="ban_sueljta"  value= {{ $bsuelo->id.'|'.$bsuelo->name.'|'.$bsuelo->precio.'|'.'1' }} id="ban_sueljta" autocomplete="off" required> {{$bsuelo->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"
                                    data-parent="#accordion">
                            Regresar
                            </button>
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            Continuar
                            </button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree">
                        DORMITORIOS, SALON Y PASILLOS
                        </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="p-1">
                                    <p class="p1">La limpieza de paredes quitando el polvo, las telarañas, pelusas etc así como la limpieza
                                        de espejos ya está incluida en la limpieza base, ¿quiere además que se limpien los
                                        enchufes, interruptores, radiadores, rodapiés, salidas de aire acondicionado y apliques/lámparas
                                        de la vivienda?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspenchufes as $denchufe)
                                        <label for="dorm_ench_int" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_ench_int"  value= {{ $denchufe->id.'|'.$denchufe->name.'|'.$denchufe->precio.'|'.'1' }} id="dorm_ench_int" autocomplete="off" required> {{$denchufe->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que sus sofás, sillones y colchones sean aspirados y vaporizados?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspcolchones as $dcolchones)
                                        <label for="dorm_asp_vap_colch" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_asp_vap_colch"  value= {{ $dcolchones->id.'|'.$dcolchones->name.'|'.$dcolchones->precio.'|'.'1' }} id="dorm_asp_vap_colch" autocomplete="off" required> {{$dcolchones->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que sus alfombras sean aspiradas y vaporizadas? Si es así, ¿cuántas?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspalfombras as $dalfombra)
                                        <label for="dorm_asp_vap_alfomb" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_asp_vap_alfomb"  value= {{ $dalfombra->id.'|'.$dalfombra->name.'|'.$dalfombra->precio.'|'.'1'}} id="dorm_asp_vap_alfomb" autocomplete="off" required> {{$dalfombra->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se frieguen las puertas de la vivienda? Si es así, ¿cuántas quiere fregar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dsppuertas as $dpuerta)
                                        <label for="dorm_fre_puertas" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_fre_puertas"  value= {{ $dpuerta->id.'|'.$dpuerta->name.'|'.$dpuerta->precio.'|'.'1' }} id="dorm_fre_puertas" autocomplete="off" required> {{$dpuerta->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpie el interior de los armarios roperos y/o empotrados? Si es así,
                                        ¿cuántos armarios quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspinteriorarmarios as $dintarmario)
                                        <label for="dorm_arm_rop_int" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_arm_rop_int"  value= {{ $dintarmario->id.'|'.$dintarmario->name.'|'.$dintarmario->precio.'|'.'1' }} id="dorm_arm_rop_int" autocomplete="off" required> {{$dintarmario->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpie el interior de la vitrina del salón y/o mueble mural? Si es así,
                                        ¿cuántos muebles quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspvitrina as $dvitrina)
                                        <label for="dorm_vitr_mur" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_vitr_mur"  value= {{ $dvitrina->id.'|'.$dvitrina->name.'|'.$dvitrina->precio.'|'.'1' }} id="dorm_vitr_mur" autocomplete="off" required > {{$dvitrina->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">En el caso de que el material del suelo fuera parquet ¿quiere que le apliquemos cera
                                        líquida?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspparquet as $dparquet)
                                        <label for="dorm_suel_parq" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_suel_parq"  value= {{ $dparquet->id.'|'.$dparquet->name.'|'.$dparquet->precio.'|'.'1' }} id="dorm_suel_parq" autocomplete="off" required> {{$dparquet->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpie el interior de muebles bajos como aparadores, cómodas o cajoneras?
                                        Si es así, ¿cuántos quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspmuebles as $dbjmueble)
                                        <label for="dorm_mueb_bajo" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_mueb_bajo"  value= {{ $dbjmueble->id.'|'.$dbjmueble->name.'|'.$dbjmueble->precio.'|'.'1' }} }} id="dorm_mueb_bajo" autocomplete="off" required> {{$dbjmueble->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpien libros, adornos, aparatos electrónicos o marcos de fotos? Si es
                                        así, ¿cuántos quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dsplibros as $dlibro)
                                        <label for="dorm_libro_ador" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_libro_ador"  value= {{ $dlibro->id.'|'.$dlibro->name.'|'.$dlibro->precio.'|'.'1' }} id="dorm_libro_ador" autocomplete="off" required> {{$dlibro->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se retiren los muebles y/o armarios y se limpie la parte posterior de éstos
                                        así como las paredes y suelo donde se encuentran?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspmuebarmar as $dmuebarmar)
                                        <label for="dorm_mueb_arma" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_mueb_arma"  value= {{  $dmuebarmar->id.'|'.$dmuebarmar->name.'|'.$dmuebarmar->precio.'|'.'1' }} id="dorm_mueb_arma" autocomplete="off" required> {{ $dmuebarmar->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">El barrido y/o aspirado y el fregado del suelo ya está incluido en la limpieza base,
                                        ¿quiere además limpiar las juntas del suelo en caso de que éste fuera de gres, mármol
                                        o terraza?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($dspjtasuelo as $djta)
                                        <label for="dorm_sueljta" class="btn btn-success  m-3">
                                            <input type="radio" name="dorm_sueljta"  value= {{ $djta->id.'|'.$djta->name.'|'.$djta->precio.'|'.'1' }} id="dorm_sueljta" autocomplete="off" required> {{ $djta->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                            Regresar
                            </button>
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapseThree">
                            Continuar
                            </button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading4">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false"
                                    aria-controls="collapse4">
                                VENTANAS Y CERRAMIENTOS EXTERIORES
                                </button>
                            </h5>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpie el interior de las persianas ? Si es así, ¿cuántas quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($ventpers as $vpersiana)
                                        <label class="btn btn-success  m-3">
                                            <input for="vent_persianas"  type="radio" name="vent_persianas"  value= {{ $vpersiana->id.'|'.$vpersiana->name.'|'.$vpersiana->precio.'|'.'1' }} id="vent_persianas" autocomplete="off" required> {{ $vpersiana->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpien persianas venecianas o mallorquinas en caso de tenerlas? Si es
                                        así, ¿cuántas quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($ventpersvenc as $vpersianavenc)
                                        <label for="vent_persianas_venec" class="btn btn-success  m-3">
                                            <input type="radio" name="vent_persianas_venec"  value= {{ $vpersianavenc->id.'|'.$vpersianavenc->name.'|'.$vpersianavenc->precio.'|'.'1'}} id="vent_persianas_venec" autocomplete="off" required> {{ $vpersianavenc->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">La limpieza de vidrios y cristales de las ventanas normales (con una sola línea de cristales
                                        sin contraventanas ni dobles ventanas) ya está incluida en la limpieza base, ¿quiere
                                        además que se limpien los marcos y raíles de estas ventanas? Si es así, ¿cuántas
                                        quiere limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($ventmarco as $vmarco)
                                        <label for="vent_marco_railes" class="btn btn-success  m-3">
                                            <input type="radio" name="vent_marco_railes"  value= {{ $vmarco->id.'|'.$vmarco->name.'|'.$vmarco->precio.'|'.'1' }} id="vent_marco_railes" autocomplete="off" required> {{ $vmarco->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Tiene doble ventana (una ventana delante de la otra) o contra ventanas (puertas de madera
                                        o metal que se ponen en la parte exterior de las ventanas)? Si es así, ¿cuántas quiere
                                        limpiar?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($ventcontvent as $vcontvent)
                                        <label for="vent_cont_vent" class="btn btn-success  m-3">
                                            <input type="radio" name="vent_cont_vent"  value= {{ $vcontvent->id.'|'.$vcontvent->name.'|'.$vcontvent->precio.'|'.'1' }} id="vent_cont_vent" autocomplete="off" required > {{ $vcontvent->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpien ventanas grandes, miradores o puertas de cristal (hasta un máximo
                                        de 2.5m de altura y anchura, y cuyo acceso a la superficie exterior a limpiar no
                                        esté a más de 1.5m desde el acceso)? Si es así, ¿cuántos quiere limpiar? Si supera
                                        estas medidas deje marcado "no" y lea el "más info"
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($ventcerrpano as $vcerrpano)
                                        <label for=" vent_panoramica" class="btn btn-success  m-3">
                                            <input type="radio" name="vent_panoramica"  value= {{ $vcerrpano->id.'|'.$vcerrpano->name.'|'.$vcontvent->precio.'|'.'1' }} id="vent_panoramica" autocomplete="off" required> {{ $vcerrpano->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Regresar
                                </button>
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                Continuar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false"
                                    aria-controls="collapse5">
                        TERRAZAS, EXTERIORES Y TENDEDEROS
                        </button>
                            </h5>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se cepillen las paredes quitando el polvo, las telarañas y pelusas? Si tienen
                                        grafitti, hollín, humo, u otras manchas muy incrustadas, deje marcado "no" y lea
                                        el "más info"
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($terrcepparedes as $tparedes)
                                        <label for="terra_cepill_pared" class="btn btn-success  m-3">
                                            <input type="radio" name="terra_cepill_pared"  value= {{ $tparedes->id.'|'.$tparedes->name.'|'.$tparedes->precio.'|'.'1'}} id="terra_cepill_pared" autocomplete="off" required> {{ $tparedes->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpien los armarios por el interior y el exterior?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($terrarmextint as $tarmario)
                                        <label for="terra_arm_int_ext" class="btn btn-success  m-3">
                                            <input type="radio" name="terra_arm_int_ext"  value= {{ $tarmario->id.'|'.$tarmario->name.'|'.$tarmario->precio.'|'.'1' }} id="terra_arm_int_ext" autocomplete="off" required> {{ $tarmario->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se limpie la barandilla y/o perímetro de la terraza (ya sea de cristal, metal
                                        o azulejo)?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($terrbarandilla as $tbarandilla)
                                        <label for="terra_baranda" class="btn btn-success  m-3">
                                            <input type="radio" name="terra_baranda"  value= {{ $tbarandilla->id.'|'.$tbarandilla->name.'|'.$tbarandilla->precio.'|'.'1' }} id="terra_baranda" autocomplete="off" required > {{ $tbarandilla->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="p-1">
                                    <p class="p1">¿Quiere que se barra, aspire y friegue el suelo?
                                        <a href="#" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Coloca texto">Mas informacion
                                    </a>
                                    </p>
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        @foreach ($terrsuelo as $tsuelo)
                                        <label for="terra_suelo" class="btn btn-success  m-3">
                                            <input type="radio" name="terra_suelo"  value= {{ $tsuelo->id.'|'.$tsuelo->name.'|'.$tsuelo->precio.'|'.'1' }} id="terra_suelo" autocomplete="off" required> {{ $tsuelo->name}}
                                        </label> @endforeach
                                    </div>
                                </div>
                                <div class="p-3">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            Regresar
                            </button> {{-- <button class="btn btn-dark" type="button" data-toggle="collapse"
                                    data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            Continuar
                            </button> --}}
                            </div>
                        </div>
                    </div>
                    {{--
                    <div class="card">
                        <div class="card-header" id="heading6">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false"
                                    aria-controls="collapseThree">
                        ZONAS GENERALES
                        </button>
                            </h5>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                                3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                                labore sustainable VHS.
                            </div>
                        </div>
                    </div> --}}

                </div>
                <div class="p-4">
                    <div class="d-flex flex-row-reverse  bd-highlight">
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary btn-lg">Siguiente
                        </button>
                        </div>
                    </div>
                </div>
            </div>

</form>
@endsection

<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>


<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Usuarios</span></a></li>
        <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
        <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permisos</span></a></li>
        <li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Servicios</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('servicio') }}"><i class="fa fa-cubes"></i> <span>Servicios</span></a></li>
        <li><a href="{{ backpack_url('subservicio') }}"><i class="fa fa-cube"></i> <span>Sub Servicios</span></a></li>
        <li><a href="{{ backpack_url('provincia') }}"><i class="fa fa-map"></i> <span>Provincias</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Vivienda </span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('viviendatipo') }}"><i class="fa fa-home"></i> <span>Vivienda Tipo</span></a></li>
        <li><a href="{{ backpack_url('dimension') }}"><i class="fa fa-square"></i> <span>Dimensiones</span></a></li>
        <li><a href="{{ backpack_url('estancia') }}"><i class="fa fa-square"></i> <span>Estancias</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Servicios de Cocina</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('cocinainteriorcajonesarmario') }}"><i class="fa fa-square"></i> <span>Interior Cajones</span></a></li>
        <li><a href="{{ backpack_url('cocinainteriorcajonmueble') }}"><i class="fa fa-square"></i> <span>Interior Armarios</span></a></li>
        <li><a href="{{ backpack_url('cocinainteriorelectrambiente') }}"><i class="fa fa-square"></i> <span>Ambientes Electrodomesticos</span></a></li>
        <li><a href="{{ backpack_url('cocinainteriorhornonevera') }}"><i class="fa fa-square"></i> <span>Horno Nevera</span></a></li>
        <li><a href="{{ backpack_url('cocinajtaazulejos') }}"><i class="fa fa-square"></i> <span>Juntas Azulejos</span></a></li>
        <li><a href="{{ backpack_url('cocinajtasuelo') }}"><i class="fa fa-square"></i> <span>Juntas Suelo</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Servicios de Baño</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('banointcajarm') }}"><i class="fa fa-square"></i> <span>Interior Cajones</span></a></li>
        <li><a href="{{ backpack_url('banomampara') }}"><i class="fa fa-square"></i> <span>Mamparas</span></a></li>
        <li><a href="{{ backpack_url('banoplatoducha') }}"><i class="fa fa-square"></i> <span>Platos de ducha</span></a></li>
        <li><a href="{{ backpack_url('banojtaazulejo') }}"><i class="fa fa-square"></i> <span>Juntas Azulejo</span></a></li>
        <li><a href="{{ backpack_url('banohongos') }}"><i class="fa fa-square"></i> <span>Limpieza de Hongos</span></a></li>
        <li><a href="{{ backpack_url('banojtasuelo') }}"><i class="fa fa-square"></i> <span>Juntas Suelo</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Servicios Dormitorio, Salon</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('dorsalpasialfombras') }}"><i class="fa fa-square"></i> <span>Alfombras</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasibajomueble') }}"><i class="fa fa-square"></i> <span>Muebles bajos</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasienchufes') }}"><i class="fa fa-square"></i> <span>Penchufes/Apliques </span></a></li>
        <li><a href="{{ backpack_url('dorsalpasiintarm') }}"><i class="fa fa-square"></i> <span>Interno Armarios</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasijtasuelo') }}"><i class="fa fa-square"></i> <span>Juntas de suelo</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasilibrosadornos') }}"><i class="fa fa-square"></i> <span>Libros Adornos</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasimuebarmar') }}"><i class="fa fa-square"></i> <span>Ambiente Muebles Armarios</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasipuertas') }}"><i class="fa fa-square"></i> <span>Puertas</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasisillonescolchones') }}"><i class="fa fa-square"></i> <span>Sillones Colchones</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasisueloparquet') }}"><i class="fa fa-square"></i> <span>Suelo parquet</span></a></li>
        <li><a href="{{ backpack_url('dorsalpasivitrina') }}"><i class="fa fa-square"></i> <span>Vitrinas</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Servicios Ventanas</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('ventcerrintpers') }}"><i class="fa fa-square"></i> <span>Persianas</span></a></li>
        <li><a href="{{ backpack_url('ventcerrpersvenec') }}"><i class="fa fa-square"></i> <span>Persianas Venecianas</span></a></li>
        <li><a href="{{ backpack_url('ventcerrventmarco') }}"><i class="fa fa-square"></i> <span>Marcos</span></a></li>
        <li><a href="{{ backpack_url('ventcerrcontvent') }}"><i class="fa fa-square"></i> <span>Contra Ventanas</span></a></li>
        <li><a href="{{ backpack_url('ventcerrventpan') }}"><i class="fa fa-square"></i> <span>Ventanas grandes</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Servicios Terrazas Ext.</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('terrextcepparedes') }}"><i class="fa fa-square"></i> <span>Paredes</span></a></li>
        <li><a href="{{ backpack_url('terrextarmextint') }}"><i class="fa fa-square"></i> <span>Armarios</span></a></li>
        <li><a href="{{ backpack_url('terrextbarandilla') }}"><i class="fa fa-square"></i> <span>Barandilla</span></a></li>
        <li><a href="{{ backpack_url('terrextsuelo') }}"><i class="fa fa-square"></i> <span>Suelo</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Presupuestos</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('presupuesto') }}"><i class="fa fa-square"></i> <span>Presupuestos</span></a></li>

    </ul>
</li>

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;


class OpcionesTerraza extends Model
{
    protected $table = 'opciones_terrazas';
    protected $primaryKey = 'id';
    protected $fillable = [

        'terra_cepill_pared_id',
        'terra_cepill_pared_name',
        'terra_cepill_pared_precio',
        'terra_cepill_pared_multiplica',
        'terra_arm_int_ext_id',
        'terra_arm_int_ext_name',
        'terra_arm_int_ext_precio',
        'terra_arm_int_ext_multiplica',
        'terra_baranda_id',
        'terra_baranda_name',
        'terra_baranda_precio',
        'terra_baranda_multiplica',
        'terra_suelo_id',
        'terra_suelo_name',
        'terra_suelo_precio',
        'terra_suelo_multiplica',
        'presupuesto_id',
        'presup_numero',
        'terra_secc_presup_total',

    ];



     /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function opcionesterrazas()
    {
        return $this->belongsTo(Presupuesto::class);
    }

}

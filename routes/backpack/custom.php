<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace' => 'App\Http\Controllers\Admin',
], function () {
    CRUD::resource('servicio', 'ServicioCrudController');
    CRUD::resource('subservicio', 'SubservicioCrudController');
    CRUD::resource('provincia', 'ProvinciaCrudController');

    CRUD::resource('viviendatipo', 'ViviendatipoCrudController');
    CRUD::resource('dimension', 'DimensionCrudController');
    CRUD::resource('estancia', 'EstanciaCrudController');

    CRUD::resource('cocinainteriorcajonesarmario', 'CocinaInteriorCajonesArmarioCrudController');
    CRUD::resource('cocinainteriorcajonmueble', 'CocinaInteriorCajonMuebleCrudController');
    CRUD::resource('cocinainteriorelectrambiente', 'CocinaInteriorElectrAmbienteCrudController');
    CRUD::resource('cocinainteriorhornonevera', 'CocinaInteriorHornoNeveraCrudController');
    CRUD::resource('cocinajtaazulejos', 'CocinaJtaAzulejosCrudController');
    CRUD::resource('cocinajtasuelo', 'CocinaJtasueloCrudController');

    CRUD::resource('banointcajarm', 'BanoIntCajArmCrudController');
    CRUD::resource('banomampara', 'BanoMamparaCrudController');
    CRUD::resource('banoplatoducha', 'BanoPlatoDuchaCrudController');
    CRUD::resource('banojtaazulejo', 'BanoJtaAzulejoCrudController');
    CRUD::resource('banohongos', 'BanoHongosCrudController');
    CRUD::resource('banojtasuelo', 'BanoJtaSueloCrudController');

    CRUD::resource('dorsalpasialfombras', 'DorSalPasiAlfombrasCrudController');
    CRUD::resource('dorsalpasibajomueble', 'DorsalpasibajomuebleCrudController');
    CRUD::resource('dorsalpasienchufes', 'DorSalPasiEnchufesCrudController');
    CRUD::resource('dorsalpasiintarm', 'DorsalpasiintarmCrudController');
    CRUD::resource('dorsalpasijtasuelo', 'DorSalPasiJtasueloCrudController');
    CRUD::resource('dorsalpasilibrosadornos', 'DorsalpasilibrosadornosCrudController');
    CRUD::resource('dorsalpasimuebarmar', 'DorSalPasiMuebArmarCrudController');
    CRUD::resource('dorsalpasipuertas', 'DorsalpasipuertasCrudController');
    CRUD::resource('dorsalpasisillonescolchones', 'DorSalPasiSillonesColchonesCrudController');
    CRUD::resource('dorsalpasisueloparquet', 'DorsalpasisueloparquetCrudController');
    CRUD::resource('dorsalpasivitrina', 'DorsalpasivitrinaCrudController');

    CRUD::resource('ventcerrcontvent', 'VentcerrcontventCrudController');
    CRUD::resource('ventcerrintpers', 'VentcerrintpersCrudController');
    CRUD::resource('ventcerrpersvenec', 'VentcerrpersvenecCrudController');
    CRUD::resource('ventcerrventmarco', 'VentcerrventmarcoCrudController');
    CRUD::resource('ventcerrventpan', 'VentcerrventpanCrudController');

    CRUD::resource('terrextarmextint', 'TerrextarmextintCrudController');
    CRUD::resource('terrextbarandilla', 'TerrextbarandillaCrudController');
    CRUD::resource('terrextcepparedes', 'TerrextcepparedesCrudController');
    CRUD::resource('terrextsuelo', 'TerrextsueloCrudController');
    CRUD::resource('presupuesto', 'PresupuestoCrudController');




});

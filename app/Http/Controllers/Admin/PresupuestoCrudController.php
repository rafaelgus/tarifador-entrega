<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PresupuestoRequest as StoreRequest;
use App\Http\Requests\PresupuestoRequest as UpdateRequest;

/**
 * Class PresupuestoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PresupuestoCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\PresupuestoAdmin');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/presupuesto');
        $this->crud->setEntityNameStrings('presupuesto', 'presupuestos');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'numero_presup',
            'label' => "Presupuesto",
            'type' => 'text'
        ]);
        $this->crud->addColumn([
            'name' => 'servicio',
            'label' => 'Servicio',
            'type' => 'text',

        ]);
        $this->crud->addColumn([
            'name' => 'subservicio',
            'label' => 'SubServicio',
            'type' => 'text',

        ]);
        $this->crud->addColumn([
            'name' => 'provincia',
            'label' => 'Provincia',
            'type' => 'text',

        ]);
        $this->crud->addColumn([
            'name' => 'total_presupuesto',
            'label' => 'Total ',
            'type' => 'number',

        ]);


        // add asterisk for fields that are required in PresupuestoRequest
        //$this->crud->setRequiredFields(StoreRequest::class, 'create');
        //$this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->allowAccess('show');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('create');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Terrextarmextint;
use App\Models\Terrextbarandilla;
use App\Models\Terrextcepparedes;
use App\Models\Terrextsuelo;


class TerrazaController extends Controller
{
    public function terrextarmextint()
    {
        return Terrextarmextint::get(['id', 'name', 'precio']);
    }
    public function terrextbarandilla()
    {
        return Terrextbarandilla::get(['id', 'name', 'precio']);
    }
    public function terrextcepparedes()
    {
        return Terrextcepparedes::get(['id', 'name', 'precio']);
    }
    public function terrextsuelo()
    {
        return Terrextsuelo::get(['id', 'name', 'precio']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Presupuesto;
use App\Models\opcionesPresupuesto;
use App\Models\OpcionesBanoAseo;
use App\Models\OpcionesCocina;
use App\Models\OpcionesDormitorio;
use App\Models\OpcionesTerraza;
use App\Models\OpcionesVentana;
use App\Models\PresupuestoFecha;
use App\Models\PresupuestoSolicitante;
use App\Models\PresupuestoVivienda;
use Illuminate\Http\Request;

class PresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /* $this->validate($request, [
            'servicios' => 'required',
            'subservicios' => 'required',
            'region' => 'required',

        ]); */

        $servicios = explode('|', $request['servicio']);
        $subservicios = explode('|', $request['subservicio']);
        $provincia = explode('|', $request['region']);


        $presup = Presupuesto::create([
            'numero_presup' => md5(time()),
            'servicio_id' => $servicios[0],
            'servicio' => $servicios[1],
            'servicio_precio' => $servicios[2],
            'subservicio_id' => $subservicios[0],
            'subservicio' => $subservicios[1],
            'subservicio_precio' => $subservicios[2],
            'provincia_id' => $provincia[0],
            'provincia' => $provincia[1],
            'provincia_precio' => $provincia[2],
            'secc_ini_presup_total' => $servicios[2] + $subservicios[2] + $provincia[2],
        ]);



        $opcioncocina = OpcionesCocina::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],

        ]);

        $opcionbano = OpcionesBanoAseo::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);
        $opciondormitorio = OpcionesDormitorio::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);
        $opcionterraza = OpcionesTerraza::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);
        $opcionventana = OpcionesVentana::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);
        $presupfecha = PresupuestoFecha::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);
        $presupvivienda = PresupuestoVivienda::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);
        $presupsolicitante = PresupuestoSolicitante::create([
            'presupuesto_id' => $presup->id,
            'presup_numero' => $presup['numero_presup'],
        ]);

        //dd([$presup, $opcioncocina]);



        return redirect()->route('opcionpresupuesto.index', ['id' => $presup->id]);

        /* return redirect()->route('profile', [$user]); */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function show(Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function edit(Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presupuesto $presupuesto)
    {
        //
    }
}

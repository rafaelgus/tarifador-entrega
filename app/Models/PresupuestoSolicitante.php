<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;

class PresupuestoSolicitante extends Model
{
    protected $table = 'presupuesto_solicitantes';

    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre',
        'email',
        'telefono',
        'direccion',
        'presupuesto_id',
        'presup_numero',
    ];

     /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function presupuestosolicitante()
    {
        return $this->belongsTo(Presupuesto::class);
    }
}

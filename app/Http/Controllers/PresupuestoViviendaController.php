<?php

namespace App\Http\Controllers;

use App\Models\PresupuestoVivienda;
use Illuminate\Http\Request;

class PresupuestoViviendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PresupuestoVivienda  $presupuestoVivienda
     * @return \Illuminate\Http\Response
     */
    public function show(PresupuestoVivienda $presupuestoVivienda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PresupuestoVivienda  $presupuestoVivienda
     * @return \Illuminate\Http\Response
     */
    public function edit(PresupuestoVivienda $presupuestoVivienda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PresupuestoVivienda  $presupuestoVivienda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PresupuestoVivienda $presupuestoVivienda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PresupuestoVivienda  $presupuestoVivienda
     * @return \Illuminate\Http\Response
     */
    public function destroy(PresupuestoVivienda $presupuestoVivienda)
    {
        //
    }
}

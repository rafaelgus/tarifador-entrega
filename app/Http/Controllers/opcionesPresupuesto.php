<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Presupuesto;
use App\Models\Viviendatipo;
use App\Models\Dimension;
use App\Models\Estancia;

use App\Models\CocinaInteriorCajonesArmario;
use App\Models\CocinaInteriorCajonMueble;
use App\Models\CocinaInteriorElectrAmbiente;
use App\Models\CocinaInteriorHornoNevera;
use App\Models\CocinaJtaAzulejos;
use App\Models\CocinaJtasuelo;

use App\Models\BanoHongos;
use App\Models\BanoIntCajArm;
use App\Models\BanoJtaAzulejo;
use App\Models\BanoJtaSuelo;
use App\Models\BanoMampara;
use App\Models\BanoPlatoDucha;

use App\Models\DorSalPasiAlfombras;
use App\Models\Dorsalpasibajomueble;
use App\Models\DorSalPasiEnchufes;
use App\Models\Dorsalpasiintarm;
use App\Models\DorSalPasiJtasuelo;
use App\Models\Dorsalpasilibrosadornos;
use App\Models\DorSalPasiMuebArmar;
use App\Models\Dorsalpasipuertas;
use App\Models\DorSalPasiSillonesColchones;
use App\Models\Dorsalpasisueloparquet;
use App\Models\Dorsalpasivitrina;

use App\Models\Ventcerrcontvent;
use App\Models\Ventcerrintpers;
use App\Models\Ventcerrpersvenec;
use App\Models\Ventcerrventmarco;
use App\Models\Ventcerrventpan;

use App\Models\Terrextarmextint;
use App\Models\Terrextbarandilla;
use App\Models\Terrextcepparedes;
use App\Models\Terrextsuelo;
use App\Models\PresupuestoVivienda;

use App\Models\OpcionesBanoAseo;
use App\Models\OpcionesCocina;
use App\Models\OpcionesDormitorio;
use App\Models\OpcionesTerraza;
use App\Models\OpcionesVentana;




class opcionesPresupuesto extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $presup = Presupuesto::find($id);

        $viviendatipo = Viviendatipo::get(['id', 'tipo', 'precio']);
        $dimension = Dimension::get(['id', 'dimension', 'precio']);
        $estancias = Estancia::get(['id', 'cantidad', 'precio']);


        $armariococ = CocinaInteriorCajonesArmario::get(['id', 'name', 'precio']);
        $cajoncoc = CocinaInteriorCajonMueble::get(['id', 'name', 'precio']);
        $electrococ = CocinaInteriorElectrAmbiente::get(['id', 'name', 'precio']);
        $hornococ = CocinaInteriorHornoNevera::get(['id', 'name', 'precio']);
        $azulejococ = CocinaJtaAzulejos::get(['id', 'name', 'precio']);
        $jtacoc = CocinaJtaAzulejos::get(['id', 'name', 'precio']);

        $banohongo = BanoHongos::get(['id', 'name', 'precio']);
        $banocaj = BanoIntCajArm::get(['id', 'name', 'precio']);
        $banoazulejo = BanoJtaAzulejo::get(['id', 'name', 'precio']);
        $banosuelo = BanoJtaSuelo::get(['id', 'name', 'precio']);
        $banomampara = BanoMampara::get(['id', 'name', 'precio']);
        $banoducha = BanoPlatoDucha::get(['id', 'name', 'precio']);



        $dspalfombras = DorSalPasiAlfombras::get(['id', 'name', 'precio']);
        $dspmuebles = Dorsalpasibajomueble::get(['id', 'name', 'precio']);
        $dspenchufes = DorSalPasiEnchufes::get(['id', 'name', 'precio']);
        $dspinteriorarmarios = Dorsalpasiintarm::get(['id', 'name', 'precio']);
        $dspjtasuelo = DorSalPasiJtasuelo::get(['id', 'name', 'precio']);
        $dsplibros = Dorsalpasilibrosadornos::get(['id', 'name', 'precio']);
        $dspmuebarmar = DorSalPasiMuebArmar::get(['id', 'name', 'precio']);
        $dsppuertas = Dorsalpasipuertas::get(['id', 'name', 'precio']);
        $dspcolchones = DorSalPasiSillonesColchones::get(['id', 'name', 'precio']);
        $dspparquet = Dorsalpasisueloparquet::get(['id', 'name', 'precio']);
        $dspvitrina = Dorsalpasivitrina::get(['id', 'name', 'precio']);


        $ventcontvent = Ventcerrcontvent::get(['id', 'name', 'precio']);
        $ventpers = Ventcerrintpers::get(['id', 'name', 'precio']);
        $ventpersvenc = Ventcerrpersvenec::get(['id', 'name', 'precio']);
        $ventmarco = Ventcerrventmarco::get(['id', 'name', 'precio']);
        $ventcerrpano = Ventcerrventpan::get(['id', 'name', 'precio']);


        $terrarmextint = Terrextarmextint::get(['id', 'name', 'precio']);
        $terrbarandilla = Terrextbarandilla::get(['id', 'name', 'precio']);
        $terrcepparedes = Terrextcepparedes::get(['id', 'name', 'precio']);
        $terrsuelo = Terrextsuelo::get(['id', 'name', 'precio']);






        return view('opcionespresupuesto', ['viviendatipo' => $viviendatipo, 'dimension' => $dimension, 'estancias' => $estancias, 'armariococ' => $armariococ, 'cajoncoc' => $cajoncoc, 'electrococ' => $electrococ, 'hornococ' => $hornococ, 'azulejococ' => $azulejococ, 'jtacoc' => $jtacoc, 'banohongo' => $banohongo, 'banocaj' => $banocaj, 'banoazulejo' => $banoazulejo, 'banosuelo' => $banosuelo, 'banomampara' => $banomampara, 'banoducha' => $banoducha, 'dspalfombras' => $dspalfombras, 'dspmuebles' => $dspmuebles, 'dspenchufes' => $dspenchufes, 'dspinteriorarmarios' => $dspinteriorarmarios, 'dspjtasuelo' => $dspjtasuelo, 'dsplibros' => $dsplibros, 'dspmuebarmar' => $dspmuebarmar, 'dsppuertas' => $dsppuertas, 'dspcolchones' => $dspcolchones, 'dspparquet' => $dspparquet, 'dspvitrina' => $dspvitrina, 'ventcontvent' => $ventcontvent, 'ventpers' => $ventpers, 'ventpersvenc' => $ventpersvenc, 'ventmarco' => $ventmarco, 'ventcerrpano' => $ventcerrpano, 'terrarmextint' => $terrarmextint, 'terrbarandilla' => $terrbarandilla, 'terrcepparedes' => $terrcepparedes, 'terrsuelo' => $terrsuelo, 'presup' => $presup]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $presup = Presupuesto::find($id);
        $viviendaValues = explode('|', $request['vivienda']);
        $dimensionesValues = explode('|', $request['dimensiones']);
        $estanciasValues = explode('|', $request['estancias']);

        $cocinaArmValues = explode('|', $request['coc_arm_int']);
        $cocinaMueblValues = explode('|', $request['coc_muebl_int']);
        $cocinaHornoValues = explode('|', $request['coc_horno_int']);
        $hornoName = CocinaInteriorHornoNevera::where('id', $cocinaHornoValues)->get();
        $cocinaElectValues = explode('|', $request['coc_elect_int']);
        $cocinaAzujtaValues = explode('|', $request['coc_azujta']);
        $cocinaSueljtaValues = explode('|', $request['coc_sueljta']);

        $banIntArmValues = explode('|', $request['ban_int_arm']);
        $banMampValues = explode('|', $request['ban_mamp']);
        $banSanitValues = explode('|', $request['ban_sanit']);
        $banAzuljtaValues = explode('|', $request['ban_azuljta_pared']);
        $banHongostaValues = explode('|', $request['ban_hongos']);
        $banSueljtataValues = explode('|', $request['ban_sueljta']);

        $dormEnchInt = explode('|', $request['dorm_ench_int']);
        $dormAspVapColch = explode('|', $request['dorm_asp_vap_colch']);
        $dormAspVapAlfomb = explode('|', $request['dorm_asp_vap_alfomb']);
        $dormFrePuertas = explode('|', $request['dorm_fre_puertas']);
        $dormArmRopInt = explode('|', $request['dorm_arm_rop_int']);
        $dormVitrMur = explode('|', $request['dorm_vitr_mur']);
        $dormSuelParq = explode('|', $request['dorm_suel_parq']);
        $dormMuebBajo = explode('|', $request['dorm_mueb_bajo']);
        $dormLibroAdor = explode('|', $request['dorm_libro_ador']);
        $dormMuebArma = explode('|', $request['dorm_mueb_arma']);
        $dormSueljta = explode('|', $request['dorm_sueljta']);

        $ventPersianas = explode('|', $request['vent_persianas']);
        $ventPersianasVenec = explode('|', $request['vent_persianas_venec']);
        $ventMarcoRailes = explode('|', $request['vent_marco_railes']);
        $ventContVent = explode('|', $request['vent_cont_vent']);
        $ventPanoramica = explode('|', $request['vent_panoramica']);

        $terraCepillPared = explode('|', $request['terra_cepill_pared']);
        $terraArmIntExt = explode('|', $request['terra_arm_int_ext']);
        $terraBaranda = explode('|', $request['terra_baranda']);
        $terraSuelo = explode('|', $request['terra_suelo']);




        $vivienda = PresupuestoVivienda::where('presupuesto_id', $id)->update([
            'tipo_vivienda_id' => $viviendaValues[0],
            'tipo_vivienda_name' => $viviendaValues[1],
            'tipo_vivienda_precio' => $viviendaValues[2],
            'dimen_vivienda_id' => $dimensionesValues[0],
            'dimen_vivienda_name' => $dimensionesValues[1],
            'dimen_vivienda_precio' => $dimensionesValues[2],
            'estanc_vivienda_id' => $estanciasValues[0],
            'estanc_vivienda_name' => $estanciasValues[1],
            'estanc_vivienda_precio' => $estanciasValues[2],
            'vivi_secc_presup_total' => $viviendaValues[2] + $dimensionesValues[2] + $estanciasValues[2],
        ]);

        $cocina = OpcionesCocina::where('presupuesto_id', $id)->update([
            'coc_arm_int_id' => $cocinaArmValues[0],
            'coc_arm_int_name' => $cocinaArmValues[1],
            'coc_arm_int_precio' => $cocinaArmValues[2],
            'coc_arm_int_multiplica' => $cocinaArmValues[3],
            'coc_arm_int_total' => $cocinaArmValues[2] * $cocinaArmValues[3],
            'coc_muebl_int_id' => $cocinaMueblValues[0],
            'coc_muebl_int_name' => $cocinaMueblValues[1],
            'coc_muebl_int_precio' => $cocinaMueblValues[2],
            'coc_muebl_int_multiplica' => $cocinaMueblValues[3],
            'coc_mueble_int_total' => $cocinaMueblValues[2] * $cocinaMueblValues[3],
            'coc_horno_int_id' => $cocinaHornoValues[0],
            'coc_horno_int_name' => $hornoName[0]->name,
            'coc_horno_int_precio' => $cocinaHornoValues[1],
            'coc_horno_int_multiplica' => $cocinaHornoValues[2],
            'coc_horno_int_total' => $cocinaHornoValues[1] * $cocinaHornoValues[2],
            'coc_elect_int_id' => $cocinaElectValues[0],
            'coc_elect_int_name' => $cocinaElectValues[1],
            'coc_elect_int_precio' => $cocinaElectValues[2],
            'coc_elect_int_multiplica' => $cocinaElectValues[3],
            'coc_elect_int_total' => $cocinaElectValues[2] * $cocinaElectValues[3],
            'coc_azujta_id' => $cocinaAzujtaValues[0],
            'coc_azujta_name' => $cocinaAzujtaValues[1],
            'coc_azujta_precio' => $cocinaAzujtaValues[2],
            'coc_azujta_multiplica' => $cocinaAzujtaValues[3],
            'coc_azujta_total' => $cocinaAzujtaValues[2] * $cocinaAzujtaValues[3],
            'coc_sueljta_id' => $cocinaSueljtaValues[0],
            'coc_sueljta_name' => $cocinaSueljtaValues[1],
            'coc_sueljta_precio' => $cocinaSueljtaValues[2],
            'coc_sueljta_multiplica' => $cocinaSueljtaValues[3],
            'coc_sueljta_total' => $cocinaSueljtaValues[2] * $cocinaSueljtaValues[3],
            'coc_secc_presup_total' => $cocinaArmValues[2] * $cocinaArmValues[3] + $cocinaMueblValues[2] * $cocinaMueblValues[3] + $cocinaHornoValues[1] * $cocinaHornoValues[2] + $cocinaElectValues[2] * $cocinaElectValues[3] + $cocinaAzujtaValues[2] * $cocinaAzujtaValues[3] + $cocinaSueljtaValues[2] * $cocinaSueljtaValues[3],



        ]);

        $aseo = OpcionesBanoAseo::where('presupuesto_id', $id)->update([
            'ban_int_arm_id' => $banIntArmValues[0],
            'ban_int_arm_name' => $banIntArmValues[1],
            'ban_int_arm_precio' => $banIntArmValues[2],
            'ban_int_arm_multiplica' => $banIntArmValues[3],
            'ban_int_arm_total' => $banIntArmValues[2] * $banIntArmValues[3],
            'ban_mamp_id' => $banMampValues[0],
            'ban_mamp_name' => $banMampValues[1],
            'ban_mamp_precio' => $banMampValues[2],
            'ban_mamp_multiplica' => $banMampValues[3],
            'ban_mamp_total' => $banMampValues[2] * $banMampValues[3],
            'ban_sanit_id' => $banSanitValues[0],
            'ban_sanit_name' => $banSanitValues[1],
            'ban_sanit_precio' => $banSanitValues[2],
            'ban_sanit_multiplica' => $banSanitValues[3],
            'ban_sanit_total' => $banSanitValues[2] * $banSanitValues[3],
            'ban_azuljta_pared_id' => $banAzuljtaValues[0],
            'ban_azuljta_pared_name' => $banAzuljtaValues[1],
            'ban_azuljta_pared_precio' => $banAzuljtaValues[2],
            'ban_azuljta_pared_multiplica' => $banAzuljtaValues[3],
            'ban_azuljta_pared_total' => $banAzuljtaValues[2] * $banAzuljtaValues[3],
            'ban_hongos_id' => $banHongostaValues[0],
            'ban_hongos_name' => $banHongostaValues[1],
            'ban_hongos_precio' => $banHongostaValues[2],
            'ban_hongos_multiplica' => $banHongostaValues[3],
            'ban_hongos_total' => $banHongostaValues[2] * $banHongostaValues[3],
            'ban_sueljta_id' => $banSueljtataValues[0],
            'ban_sueljta_name' => $banSueljtataValues[1],
            'ban_sueljta_precio' => $banSueljtataValues[2],
            'ban_sueljta_multiplica' => $banSueljtataValues[3],
            'ban_sueljta_total' => $banSueljtataValues[2] * $banSueljtataValues[3],

            'ban_secc_presup_total' => $banIntArmValues[2] * $banIntArmValues[3] + $banMampValues[2] * $banMampValues[3] + $banSanitValues[2] * $banSanitValues[3] + $banAzuljtaValues[2] * $banAzuljtaValues[3] + $banHongostaValues[2] * $banHongostaValues[3] + $banSueljtataValues[2] * $banSueljtataValues[3],


        ]);

        $dormitorio = OpcionesDormitorio::where('presupuesto_id', $id)->update([
            'dorm_ench_int_id' => $dormEnchInt[0],
            'dorm_ench_int_name' => $dormEnchInt[1],
            'dorm_ench_int_precio' => $dormEnchInt[2],
            'dorm_ench_int_multiplica' => $dormEnchInt[3],
            'dorm_ench_int_total' => $dormEnchInt[2] * $dormEnchInt[3],

            'dorm_asp_vap_colch_id' => $dormAspVapColch[0],
            'dorm_asp_vap_colch_name' => $dormAspVapColch[1],
            'dorm_asp_vap_colch_precio' => $dormAspVapColch[2],
            'dorm_asp_vap_colch_multiplica' => $dormAspVapColch[3],
            'dorm_asp_vap_colch_total' => $dormAspVapColch[2] * $dormAspVapColch[3],

            'dorm_asp_vap_alfomb_id' => $dormAspVapAlfomb[0],
            'dorm_asp_vap_alfomb_name' => $dormAspVapAlfomb[1],
            'dorm_asp_vap_alfomb_precio' => $dormAspVapAlfomb[2],
            'dorm_asp_vap_alfomb_multiplica' => $dormAspVapAlfomb[3],
            'dorm_asp_vap_alfomb_total' => $dormAspVapAlfomb[2] * $dormAspVapAlfomb[3],

            'dorm_fre_puertas_id' => $dormFrePuertas[0],
            'dorm_fre_puertas_name' => $dormFrePuertas[1],
            'dorm_fre_puertas_precio' => $dormFrePuertas[2],
            'dorm_fre_puertas_multiplica' => $dormFrePuertas[3],
            'dorm_fre_puertas_total' => $dormFrePuertas[2] * $dormFrePuertas[3],

            'dorm_arm_rop_int_id' => $dormArmRopInt[0],
            'dorm_arm_rop_int_name' => $dormArmRopInt[1],
            'dorm_arm_rop_int_precio' => $dormArmRopInt[2],
            'dorm_arm_rop_int_multiplica' => $dormArmRopInt[3],
            'dorm_arm_rop_int_total' => $dormArmRopInt[2] * $dormArmRopInt[3],

            'dorm_vitr_mur_id' => $dormVitrMur[0],
            'dorm_vitr_mur_name' => $dormVitrMur[1],
            'dorm_vitr_mur_precio' => $dormVitrMur[2],
            'dorm_vitr_mur_multiplica' => $dormVitrMur[3],
            'dorm_vitr_mur_total' => $dormVitrMur[2] * $dormVitrMur[3],

            'dorm_suel_parq_id' => $dormSuelParq[0],
            'dorm_suel_parq_name' => $dormSuelParq[1],
            'dorm_suel_parq_precio' => $dormSuelParq[2],
            'dorm_suel_parq_multiplica' => $dormSuelParq[3],
            'dorm_suel_parq_total' => $dormSuelParq[2] * $dormSuelParq[3],

            'dorm_mueb_bajo_id' => $dormMuebBajo[0],
            'dorm_mueb_bajo_name' => $dormMuebBajo[1],
            'dorm_mueb_bajo_precio' => $dormMuebBajo[2],
            'dorm_mueb_bajo_multiplica' => $dormMuebBajo[3],
            'dorm_mueb_bajo_total' => $dormMuebBajo[2] * $dormMuebBajo[3],

            'dorm_libro_ador_id' => $dormLibroAdor[0],
            'dorm_libro_ador_name' => $dormLibroAdor[1],
            'dorm_libro_ador_precio' => $dormLibroAdor[2],
            'dorm_libro_ador_multiplica' => $dormLibroAdor[3],
            'dorm_libro_ador_total' => $dormLibroAdor[2] * $dormLibroAdor[3],

            'dorm_mueb_arma_id' => $dormMuebArma[0],
            'dorm_mueb_arma_name' => $dormMuebArma[1],
            'dorm_mueb_arma_precio' => $dormMuebArma[2],
            'dorm_mueb_arma_multiplica' => $dormMuebArma[3],
            'dorm_mueb_arma_total' => $dormMuebArma[2] * $dormMuebArma[3],

            'dorm_sueljta_id' => $dormSueljta[0],
            'dorm_sueljta_name' => $dormSueljta[1],
            'dorm_sueljta_precio' => $dormSueljta[2],
            'dorm_sueljta_multiplica' => $dormSueljta[3],
            'dorm_sueljta_total' => $dormSueljta[2] * $dormSueljta[3],

            'dorm_secc_presup_total' => $dormEnchInt[2] * $dormEnchInt[3] + $dormAspVapColch[2] * $dormAspVapColch[3] + $dormAspVapAlfomb[2] * $dormAspVapAlfomb[3] + $dormFrePuertas[2] * $dormFrePuertas[3] + $dormArmRopInt[2] * $dormArmRopInt[3] + $dormVitrMur[2] * $dormVitrMur[3] + $dormSuelParq[2] * $dormSuelParq[3] + $dormMuebBajo[2] * $dormMuebBajo[3] + $dormLibroAdor[2] * $dormLibroAdor[3] + $dormMuebArma[2] * $dormMuebArma[3] + $dormSueljta[2] * $dormSueljta[3],

        ]);

        $ventana = OpcionesVentana::where('presupuesto_id', $id)->update([
            'vent_persianas_id' => $ventPersianas[0],
            'vent_persianas_name' => $ventPersianas[1],
            'vent_persianas_precio' => $ventPersianas[2],
            'vent_persianas_multiplica' => $ventPersianas[3],
            'vent_persianas_total' => $ventPersianas[2] * $ventPersianas[3],

            'vent_persianas_venec_id' => $ventPersianasVenec[0],
            'vent_persianas_venec_name' => $ventPersianasVenec[1],
            'vent_persianas_venec_precio' => $ventPersianasVenec[2],
            'vent_persianas_venec_multiplica' => $ventPersianasVenec[3],
            'vent_persianas_venec_total' => $ventPersianasVenec[2] * $ventPersianasVenec[3],

            'vent_marco_railes_id' => $ventMarcoRailes[0],
            'vent_marco_railes_name' => $ventMarcoRailes[1],
            'vent_marco_railes_precio' => $ventMarcoRailes[2],
            'vent_marco_railes_multiplica' => $ventMarcoRailes[3],
            'vent_marco_railes_total' => $ventMarcoRailes[2] * $ventMarcoRailes[3],

            'vent_cont_vent_id' => $ventContVent[0],
            'vent_cont_vent_name' => $ventContVent[1],
            'vent_cont_vent_precio' => $ventContVent[2],
            'vent_cont_vent_multiplica' => $ventContVent[3],
            'vent_cont_vent_total' => $ventContVent[2] * $ventContVent[3],

            'vent_panoramica_id' => $ventPanoramica[0],
            'vent_panoramica_name' => $ventPanoramica[1],
            'vent_panoramica_precio' => $ventPanoramica[2],
            'vent_panoramica_multiplica' => $ventPanoramica[3],
            'vent_panoramica_total' => $ventPanoramica[2] * $ventContVent[3],

            'vent_secc_presup_total' => $ventPersianas[2] * $ventPersianas[3] + $ventPersianasVenec[2] * $ventPersianasVenec[3] + $ventMarcoRailes[2] * $ventMarcoRailes[3] + $ventContVent[2] * $ventContVent[3] + $ventPanoramica[2] * $ventContVent[3],



        ]);

        $terraza = OpcionesTerraza::where('presupuesto_id', $id)->update([

            'terra_cepill_pared_id' => $terraCepillPared[0],
            'terra_cepill_pared_name' => $terraCepillPared[1],
            'terra_cepill_pared_precio' => $terraCepillPared[2],
            'terra_cepill_pared_multiplica' => $terraCepillPared[3],
            'terra_cepill_pared_total' => $terraCepillPared[2] * $terraCepillPared[3],

            'terra_arm_int_ext_id' => $terraArmIntExt[0],
            'terra_arm_int_ext_name' => $terraArmIntExt[1],
            'terra_arm_int_ext_precio' => $terraArmIntExt[2],
            'terra_arm_int_ext_multiplica' => $terraArmIntExt[3],
            'terra_arm_int_ext_total' => $terraArmIntExt[2] * $terraArmIntExt[3],

            'terra_baranda_id' => $terraBaranda[0],
            'terra_baranda_name' => $terraBaranda[1],
            'terra_baranda_precio' => $terraBaranda[2],
            'terra_baranda_multiplica' => $terraBaranda[3],
            'terra_baranda_total' => $terraBaranda[2] * $terraBaranda[3],

            'terra_suelo_id' => $terraSuelo[0],
            'terra_suelo_name' => $terraSuelo[1],
            'terra_suelo_precio' => $terraSuelo[2],
            'terra_suelo_multiplica' => $terraSuelo[3],
            'terra_suelo_total' => $terraSuelo[2] * $terraSuelo[3],


            'terra_secc_presup_total' => $terraCepillPared[2] * $terraCepillPared[3] + $terraArmIntExt[2] * $terraArmIntExt[3] + $terraBaranda[2] * $terraBaranda[3] + $terraSuelo[2] * $terraSuelo[3],



        ]);

        $tVivienda = PresupuestoVivienda::where('presupuesto_id', $id)->firstOrFail();
        $tCocina = OpcionesCocina::where('presupuesto_id', $id)->firstOrFail();
        $tAseo = OpcionesBanoAseo::where('presupuesto_id', $id)->firstOrFail();
        $tDormitorio = OpcionesDormitorio::where('presupuesto_id', $id)->firstOrFail();
        $tVentana = OpcionesVentana::where('presupuesto_id', $id)->firstOrFail();
        $tTerraza = OpcionesTerraza::where('presupuesto_id', $id)->firstOrFail();

        $presupuestoTotal = Presupuesto::where('id', $id)->update([

            'total_presupuesto' => $presup['secc_ini_presup_total'] + $tVivienda->vivi_secc_presup_total + $tCocina->coc_secc_presup_total + $tAseo->ban_secc_presup_total + $tDormitorio->dorm_secc_presup_total + $tVentana->vent_secc_presup_total + $tTerraza->terra_secc_presup_total,


        ]);
        


        /*  dd([
            $vivienda,
            $cocina,
            $aseo,
            $dormitorio,
            $ventana,
            $terraza,
            ]); */

        return redirect()->route('opcionfecha.index', ['id' => $id]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesDormitoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_dormitorios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dorm_ench_int_id');
            $table->string('dorm_ench_int_name');
            $table->decimal('dorm_ench_int_precio', 12, 2);
            $table->decimal('dorm_ench_int_multiplica', 4, 2);
            $table->integer('dorm_asp_vap_colch_id');
            $table->string('dorm_asp_vap_colch_name');
            $table->decimal('dorm_asp_vap_colch_precio', 12, 2);
            $table->decimal('dorm_asp_vap_colch_multiplica', 4, 2);
            $table->integer('dorm_asp_vap_alfomb_id');
            $table->string('dorm_asp_vap_alfomb_name');
            $table->decimal('dorm_asp_vap_alfomb_precio', 12, 2);
            $table->decimal('dorm_asp_vap_alfomb_multiplica', 4, 2);
            $table->integer('dorm_fre_puertas_id');
            $table->string('dorm_fre_puertas_name');
            $table->decimal('dorm_fre_puertas_precio', 12, 2);
            $table->decimal('dorm_fre_puertas_multiplica', 4, 2);
            $table->integer('dorm_arm_rop_int_id');
            $table->string('dorm_arm_rop_int_name');
            $table->decimal('dorm_arm_rop_int_precio', 12, 2);
            $table->decimal('dorm_arm_rop_int_multiplica', 4, 2);
            $table->integer('dorm_vitr_mur_id');
            $table->string('dorm_vitr_mur_name');
            $table->decimal('dorm_vitr_mur_precio', 12, 2);
            $table->decimal('dorm_vitr_mur_multiplica', 4, 2);
            $table->integer('dorm_suel_parq_id');
            $table->string('dorm_suel_parq_name');
            $table->decimal('dorm_suel_parq_precio', 12, 2);
            $table->decimal('dorm_suel_parq_multiplica', 4, 2);
            $table->integer('dorm_mueb_bajo_id');
            $table->string('dorm_mueb_bajo_name');
            $table->decimal('dorm_mueb_bajo_precio', 12, 2);
            $table->decimal('dorm_mueb_bajo_multiplica', 4, 2);
            $table->integer('dorm_mueb_arma_id');
            $table->string('dorm_mueb_arma_name');
            $table->decimal('dorm_mueb_arma_precio', 12, 2);
            $table->decimal('dorm_mueb_arma_multiplica', 4, 2);
            $table->integer('dorm_sueljta_id');
            $table->string('dorm_sueljta_name');
            $table->decimal('dorm_sueljta_precio', 12, 2);
            $table->decimal('dorm_sueljta_multiplica', 4, 2);
            $table->integer('presupuesto_id');
            $table->string('presup_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_dormitorios');
    }
}

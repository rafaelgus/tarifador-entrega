<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Ventcerrcontvent;
use App\Models\Ventcerrintpers;
use App\Models\Ventcerrpersvenec;
use App\Models\Ventcerrventmarco;
use App\Models\Ventcerrventpan;


class VentanasController extends Controller

{
    public function ventcerrcontvent()
    {
        return Ventcerrcontvent::get(['id', 'name', 'precio']);
    }
    public function ventcerrintpers()
    {
        return Ventcerrintpers::get(['id', 'name', 'precio']);
    }
    public function ventcerrpersvenec()
    {
        return Ventcerrpersvenec::get(['id', 'name', 'precio']);
    }
    public function ventcerrventmarco()
    {
        return Ventcerrventmarco::get(['id', 'name', 'precio']);
    }
    public function ventcerrventpan()
    {
        return Ventcerrventpan::get(['id', 'name', 'precio']);
    }
}

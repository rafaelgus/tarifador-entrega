<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesCocinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_cocinas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coc_arm_int_id');
            $table->string('coc_arm_int_name');
            $table->decimal('coc_arm_int_precio', 12, 2);
            $table->decimal('coc_arm_int_multiplica', 4, 2);
            $table->integer('coc_muebl_int_id');
            $table->string('coc_muebl_int_name');
            $table->decimal('coc_muebl_int_precio', 12, 2);
            $table->decimal('coc_muebl_int_multiplica', 4, 2);
            $table->integer('coc_horno_int_id');
            $table->string('coc_horno_int_name');
            $table->decimal('coc_horno_int_precio', 12, 2);
            $table->decimal('coc_horno_int_multiplica', 4, 2);
            $table->integer('coc_elect_int_id');
            $table->string('coc_elect_int_name');
            $table->decimal('coc_elect_int_precio', 12, 2);
            $table->decimal('coc_elect_int_multiplica', 4, 2);
            $table->integer('coc_azujta_id');
            $table->string('coc_azujta_name');
            $table->decimal('coc_azujta_precio', 12, 2);
            $table->decimal('coc_azujta_multiplica', 4, 2);
            $table->integer('coc_sueljta_id');
            $table->string('coc_sueljta_name');
            $table->decimal('coc_sueljta_precio', 12, 2);
            $table->decimal('coc_sueljta_multiplica', 4, 2);
            $table->integer('presupuesto_id');
            $table->string('presup_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_cocinas');
    }
}

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Conozca su presupuesto de limpieza personalizado</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/favicon.png" />
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/style.css"> <!-- Resource style -->
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/modernizr.js"></script> <!-- Modernizr -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">

	<!-- tooltipster -->
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/jquery.tooltip.css">

	<!-- Confirmatiosn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/slider-navbar-right.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/css.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5048875-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-5048875-1');
	</script>

	<!-- Global site tag (gtag.js) - Google Ads: 991546510 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-991546510"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-991546510');
	</script>

	<script>
	    window.InfoUserLE = {"email":"nuevou@example.com","route":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com\/tracking\/event\/ac","route_home":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com","list_id_ac":"118"};
	</script>

	<script type="application/ld+json">
	    {
	        "@context": "http://schema.org/",
	        "@type": "Organization",
	        "name": "Limpiezas Express",
	        "url": "http://www.limpiezasexpress.com/",
	        "logo": "http://www.limpiezasexpress.com/uploads/1/0/2/8/1028263/1457599333.png",
	        "sameAs" : [
	        "https://www.facebook.com/LimpiezasExpress/",
	        "https://twitter.com/limpiezaexpress",
	        "https://www.youtube.com/user/LimpiezasExpress",
	        "https://plus.google.com/+Limpiezasexpress"
	        ]
	    }
	</script>

	    <style>
	    	#cover{
	    		background: url(https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/loading-gif.gif) no-repeat scroll center center #fff;
	    		position: fixed;
	    		height: 100%;
	    		width: 100%;
	    		overflow: hidden;
	    		z-index: 999;
	    		background-size: 140px;
	    	}
	    </style>
		<meta name="description" content=" Consulte el precio de la limpieza de su hogar. Limpiezas a fondo de casas y pisos en toda España. Servicio profesional y todo incluido.">
	<meta name="keywords" content="limpieza de pisos a domicilio, limpieza de casas precio, empresas de limpieza domicilios particulares, empresas limpieza a domicilio, cuanto cuesta limpieza de casas">

	<script>
	    window.InfoUserDetailsLE = {"name":"Prueba 5","email":"nuevou@example.com","phone":"+58412204444","route":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com\/tracking\/event\/ac","home":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com"};
	</script>

	<!-- Event snippet for Conversión Lead Presupuestador conversion page -->
	<script>
	  gtag('event', 'conversion', {'send_to': 'AW-991546510/4gMCCJinnocBEI6Z59gD'});
	</script>

	<script type="application/ld+json">
	{
	  "@context" : "http://schema.org",
	  "@type" : "Product",
	  "name" : "Limpiezas a fondo profesionales",
	  "description" : "Limpiezas fin de obra, alquileres, casas habitadas, etc. Operarios, maquinaria y seguro de responsabilidad civil incluidos",
	  "url" : "https://presupuesto-limpieza-online.limpiezasexpress.com/",
	  "brand" : {
	    "@type" : "Brand",
	    "name" : "Limpiezas Express",
	    "logo" : "https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png"
	  },
	  "offers" : {
	    "@type" : "Offer",
	    "price" : "Desde 180€"
	  },
	  "aggregateRating" : {
	    "@type" : "AggregateRating",
	    "ratingValue" : "4,8",
	    "bestRating" : "5",
	    "worstRating" : "1",
	    "ratingCount" : "106"
	  }
	}
	</script>


</head>
<body>
	<div id="cover"></div>
	<nav class="navbar navbar-default" id="navbar">
	  <div class="container-fluid">
	    <div class="row">
	    	<div class="col-lg-12">
	    		<div class="navbar-header">
	    			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	    				<!-- <span class="sr-only">Toggle navigation</span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span> -->
	    				<i class="fa fa-phone" aria-hidden="true"></i>
	    			</button>
	    			<a href="https://presupuesto-limpieza-online.limpiezasexpress.com"><img src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png" alt="Logo Limpiezas Express" id="logo" class="img-responsive"></a>
	    			<!-- <a class="navbar-brand" href="" id="">Kode<strong>Working</strong></a> -->
	    		</div>

	    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    			<ul class="nav navbar-nav navbar-right">

	    					    				<li id="navbar-number"><span>Si tiene dudas sobre la contratación llame al: <a href="tel:+34910325262"><u>91 0325262</u></a></span></li>
	    					    				<!-- <li id="navbar-slider-call">

							<a id="menu-toggle" href="#" class="">¿Prefieres que te llamemos?</a>
								<div id="sidebar-wrapper">

									<a id="menu-close" href="#" class="pull-right toggle"><i class="glyphicon glyphicon-remove"></i></a>


									<h1>Slider navbar right</h1>
								</div>
	    				</li> -->

	    			</ul>
	    		</div>
	    	</div>
	    </div>
	  </div>
	</nav>
	<section id="task_flyout">
	<nav>
		<ol class="cd-multi-steps text-bottom count">
			<li class="visited"><a href="javascript:;">Definir servicio</a></li>
			<li class="visited"><em>Elegir fecha</em></li>
			<li class="current"><em>Ver presupuesto</em></li>
			<li><em>Realizar reserva</em></li>
		</ol>
	</nav>
</section>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-lg-10 col-lg-offset-1">

			<div class="content-budget mt-50">

				<div class="container-fluid">
					<div class="row mt-20">
						<div class="col-xs-12">
							<div class="alert alert-danger">
								<p><i class="fa fa-clock-o"></i> Alta demanda prevista para las fechas solicitadas. Quedan pocos equipos disponibles.</p>
							</div>
						</div>
					</div>

					<div class="row mt-20">
						<div class="col-xs-12 col-sm-12 col-md-12">

							<div class="panel panel-default">
								<div class="panel-body">

									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<p><strong>Tipo de servicio: Limpieza  casa amueblada despu&eacute;s de obra</strong></p>
											<p><strong>Tipo de vivienda: Casa/Chalet</strong></p>
											<p><strong>Tamaño de la vivienda: 180 m<sup>2</sup></strong></p>

											<p><strong>Número de estancias de la vivienda: 4</strong></p>

											<p><strong>Fecha del servicio: Sin fecha definida</strong></p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<p><strong>Cliente: </strong>Prueba 5</p>
											<p><strong>Email: </strong>nuevou@example.com</p>
											<p><strong>Télefono: </strong>+58412204444</p>
											<p><strong>Dirección: </strong>alicante</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<p><strong>En la limpieza base están incluidos los siguientes servicios:</strong></p>
							<p>• Desplazamientos, útiles, y productos.</p>
							<p>• Atención personalizada.</p>
							<p>• Seguro de Responsabilidad Civil.</p>
							<p>• Un equipo de varios operarios que trabajan de forma simultánea.</p>
							<p>• Trabajos realizados en solo una jornada.</p>
							<br>
							<p><strong>Además, está incluida la limpieza de los siguientes elementos cuando los haya en la vivienda:</strong></p>
							<p>• Barrido y/o aspirado y el fregado del suelo de la vivienda.</p>
							<p>• Quitar el polvo, telarañas, pelusas etc. de las paredes del inmueble.</p>
							<p>• Limpieza del polvo exterior de los muebles de las habitaciones (incluyendo las puertas de los armarios), salón, pasillos y baños sin retirar los objetos.</p>
							<p>• Fregado de azulejos de cocina y baños.</p>
							<p>• La limpieza del vidrio y cristales de los espejos y ventanas normales (con una sola línea de cristales sin contraventanas ni dobles ventanas)</p>
							<p>• En la cocina está incluida la limpieza exterior de: encimera y fregadero, armarios, cajones, electrodomésticos y otro mobiliario como sillas y mesas cuando lo haya, la vitrocerámica y los quemadores de gas siempre que estos no estén muy sucios.</p>
							<br>
							<p>Adicionalmente a lo anterior, usted ha seleccionado los siguientes servicios que puede ver clicando en cada estancia (un tick verde significa seleccionado, un aspa roja es no seleccionado). Si quiere modificar alguno de los elementos que ha elegido, puede hacerlo clicando en el botón de abajo de la página donde dice “Modificar presupuesto”.</p>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">

							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="font-size: 18px;">


			      				<div class="panel panel-default mt-50" id="cocina">
									<div class="panel-heading heading-title-zone" role="tab" id="heading1">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst1"  aria-controls="collapseFirst1"><h3 class="heading-text-zone"><strong>COCINA</strong> <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3></a>
										</h4>
									</div>

									<div id="collapseFirst1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
			      						<div class="panel-body">



			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza de las juntas del suelo o unión de las plaquetas: <strong>Sí</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de las juntas de la pared o unión de las plaquetas: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Retirada de electrodomésticos y embellecedores y limpieza por detrás: <strong>Sí</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza interior de electrodomésticos y si están poco o muy sucios: <strong>Sí, quiero limpiarlos y están poco sucios</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza de la estructura de los cajones y del interior del mueble que los acoge: <strong>Sí</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza interior de armarios y si están llenos o vacíos: <strong>Sí, quiero limpiarlos y están vacíos</strong>			      								</p>
			      										      						</div>
			      					</div>
			      				</div>


			      				<div class="panel panel-default mt-50" id="bano">
									<div class="panel-heading heading-title-zone" role="tab" id="heading2">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst2"  aria-controls="collapseFirst2"><h3 class="heading-text-zone"><strong>BAÑOS Y ASEOS</strong> <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3></a>
										</h4>
									</div>

									<div id="collapseFirst2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
			      						<div class="panel-body">


			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de las juntas del suelo o unión de las plaquetas del baño: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de zonas ennegrecidas en sanitarios por hongos o moho: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza de las juntas de la pared o unión de las plaquetas del baño: <strong>Sí</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de sanitarios, plato de ducha y/o bañera: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza de la mampara en caso de que tuviera: <strong>Sí</strong>			      								</p>
			      										      								<p><span><i class="fa fa-check" aria-hidden="true"></i></span>

			      									Limpieza interior de armarios del baño y/o aseo y cuántos son: <strong>Sí (1)</strong>			      								</p>
			      										      						</div>
			      					</div>
			      				</div>


			      				<div class="panel panel-default mt-50" id="salon">
									<div class="panel-heading heading-title-zone" role="tab" id="heading3">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst3"  aria-controls="collapseFirst3"><h3 class="heading-text-zone"><strong>DORMITORIOS, SALÓN Y PASILLOS</strong> <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3></a>
										</h4>
									</div>

									<div id="collapseFirst3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
			      						<div class="panel-body">

			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de las juntas del suelo en el caso de suelos de gres, mármol o terrazo: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Retirada de muebles, armarios y limpieza de la parte posterior del mueble, paredes y suelo donde se encuentren: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de libros, adornos, aparatos electrónicos, marcos de foto y cuántos son: <strong>No quiero limpiarlos</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza del interior de muebles bajos como aparadores, cómodas o cajoneras y cuántos son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Aplicación de cera líquida en el suelo de parquet: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									 Limpieza del interior de la vitrina del salón y/o mueble mural y cuántos son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza del interior de armarios roperos, armarios empotrados y cuántos son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Fregado de las puertas de la vivienda y cuántas son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Aspirado y vaporización de alfombras y cuántas son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Aspirado y vaporización de sofás, sillones y colchones: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de enchufes, interruptores, radiadores, rodapiés, salidas de aire acondicionado y apliques/lámparas: <strong>No</strong>			      								</p>
			      										      						</div>
			      					</div>
			      				</div>

			      				<div class="panel panel-default mt-50" id="ventanas">
									<div class="panel-heading heading-title-zone" role="tab" id="heading4">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst4"  aria-controls="collapseFirst4"><h3 class="heading-text-zone"><strong>VENTANAS Y CERRAMIENTOS EXTERIORES</strong> <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3></a>
										</h4>
									</div>

									<div id="collapseFirst4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
			      						<div class="panel-body">

			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de ventanas grandes, miradores o puertas de cristal y cuántas son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de doble o contra ventanas y cuántas son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de marcos y raíles de ventanas normales y cuántos son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de persianas venecianas o mallorquinas y cuántas son: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza interior de las persianas y cuántas son: <strong>No</strong>			      								</p>
			      										      						</div>
			      					</div>
			      				</div>

			      				<div class="panel panel-default mt-50" id="terrazas">
									<div class="panel-heading heading-title-zone" role="tab" id="heading5">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst5"  aria-controls="collapseFirst5"><h3 class="heading-text-zone"><strong>TERRAZAS, EXTERIORES Y TENDEDEROS</strong> <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3></a>
										</h4>
									</div>

									<div id="collapseFirst5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
			      						<div class="panel-body">

			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Barrido, aspirado y fregado del suelo de la terraza: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de barandilla y/o perímetro de la terraza: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Limpieza de armarios de la terraza por dentro: <strong>No</strong>			      								</p>
			      										      								<p><span><i class="fa fa-close" aria-hidden="true"></i></span>

			      									Cepillado de paredes quitando polvo, telarañas y pelusas: <strong>No</strong>			      								</p>
			      										      						</div>
			      					</div>
			      				</div>

			      				<div class="panel panel-default mt-50" id="zoneGeneral">
									<div class="panel-heading heading-title-zone" role="tab" id="heading6">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst6"  aria-controls="collapseFirst6"><h3 class="heading-text-zone"><strong>ZONAS GENERALES</strong> <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3></a>
										</h4>
									</div>

									<div id="collapseFirst6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
			      						<div class="panel-body">

			      										      								<p>

			      									Estado general de la vivienda: <strong>Poco sucia</strong>			      								</p>
			      										      								<p>

			      									Fumadores dentro de la vivienda: <strong>No</strong>			      								</p>
			      										      								<p>

			      									Mascotas en la vivienda: <strong>No</strong>			      								</p>
			      										      								<p>

			      									Restos de yeso, pintura o lechada en suelo, rodapiés, ventanas, radiadores, puertas, etc: <strong>Nada</strong>			      								</p>
			      										      						</div>
			      					</div>
			      				</div>

							</div>
						</div>
					</div>

					<div class="row mt-50">
						<div class="col-xs-12">

							<div class="content-price-detail">

								<div class="row">
									<div class="col-xs-12 col-sm-8">
										<p>
											<strong>Atención, los precios varían en función de la demanda y de la fecha elegida. Si reserva ahora su presupuesto es de:</strong>
										</p>
									</div>
									<div class="col-xs-12 col-sm-4">
										<h3 class="pull-right"  style="font-size: 36px;">468.74€</h3>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="row mt-50">

						<div class="col-xs-12 col-sm-4">

							<p class="text-center"><strong><a href="javascript:;" onclick="window.history.go(-2); return false;">Deseo cambiar la fecha</a></strong></p>

							<a href="javascript:;" id="reformeBudgetDesk" class="a-button hidden-xs"><button class="btn btn-danger btn-lg btn-le-danger center-block mt-20" style="width: 100%;">Modificar <br> presupuesto</button></a>


						</div>

						<div class="col-xs-12 col-sm-8">

							<a href="https://presupuesto-limpieza-online.limpiezasexpress.com/payment" class="a-button" id="budgetSucess"><button class="btn btn-lg btn-le-blue center-block mt-20" style="width: 100%;color:#FFF;margin-top: 40px;">Reserve ahora con 40€ y <br>cancele gratis</button></a>


						</div>


					</div>

					<div class="row">

						<div class="col-xs-12">

							<a href="javascript:;" id="reformeBudgeMobile" class="a-button visible-xs reformeBudget"><button class="btn btn-danger btn-lg btn-le-danger center-block mt-20">Modificar <br> presupuesto</button></a>


						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-offset-4 col-sm-8">

							<div class="form-group">

								<label for="service_finances" style="font-weight: normal;"><input type="checkbox" name="service_finances" id="service_finances" onclick="serviceFinances()"> Quiero información para financiar mi servicio para lo que acepto que se comuniquen mis datos a empresas financieras.</label>
							</div>

							<!-- <p class="mt-20">
								<strong><a data-toggle="collapse" href="#phone-contact" id="phone">¿Tiene alguna duda? Llámenos</a></strong>
							</p>

							<div class="collapse" id="phone-contact">
								<p class="mt-20">Tel. 24 horas toda España: <strong>910325262</strong></p>
							</div> -->
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>



	<script type="text/javascript">
		// Set to false if opt-in required
		var trackByDefault = true;

		function acEnableTracking() {
			var expiration = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
			document.cookie = "ac_enable_tracking=1; expires= " + expiration + "; path=/";
			acTrackVisit();
		}

		function acTrackVisit() {
			var trackcmp_email = '';
			var trackcmp = document.createElement("script");
			trackcmp.async = true;
			trackcmp.type = 'text/javascript';
			trackcmp.src = '//trackcmp.net/visit?actid=65774532&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
			var trackcmp_s = document.getElementsByTagName("script");
			if (trackcmp_s.length) {
				trackcmp_s[0].parentNode.appendChild(trackcmp);
			} else {
				var trackcmp_h = document.getElementsByTagName("head");
				trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
			}
		}

		if (trackByDefault || /(^|; )ac_enable_tracking=([^;]+)/.test(document.cookie)) {
			acEnableTracking();
		}
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/slider.navbar.right.js"></script>
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/tooltips.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/jquery.tooltip.js"></script>
	<script>

		$(window).on('load', function () {
		    $('#cover').fadeOut(1000);

		});

		$('.hastip').powerTip({
			smartPlacement: true
		});

		$(document).ready(function() {

		  $('form').keypress(function(e){
		    if(e == 13){
		      return false;
		    }
		  });

		  $('input').keypress(function(e){
		      if(e.which == 13){
		        return false;
		      }
		    });

			/*window.onbeforeunload = function (e) {

				var e = e || window.event;

				if (e) {

					$.ajax({
			  			type: 'get',
			  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
			  			async: false
			  		});
					console.log('Abandono ejecutado');
				}

			}*/

		  window.setTimeout(function(){

		  	if(InfoUserLE.email && InfoUserLE.list_id_ac == "118"){
		  		$.ajax({
		  			type: 'get',
		  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'customerInteraction',
		  			async: false
		  		});
		  	}else if(InfoUserLE.email && InfoUserLE.list_id_ac == "119"){

		  		$.ajax({
		  			type: 'get',
		  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'customerInteractionEmpties',
		  			async: false
		  		});
		  	}


		  }, 900000);
		});
	</script>

	<script
		src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js">
	</script>

	<script>
		$.validate({
			form : '#forms',
		   	lang: 'es',
		 });
	</script>

	<script type="text/javascript">
		var validNavigationClose = 0;

		function endSession()
		{
			console.log('CLOSED');

			$.ajax({
	  			type: 'get',
	  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
	  			async: false
	  		});
		}
		function bindDOMEvents() {
			console.log('INIT '+ validNavigationClose);
			/*

			* unload works on both closing tab and on refreshing tab.

			*/

			$(window).on('beforeunload', function(){
				if (validNavigationClose == 0)
				{
				  endSession();
				}
			})

			/*$(window).on('unload',function()
			{
			   if (validNavigationClose==0)
			   {
			     endSession();
			   }
			});*/

			// Attach the event keypress to exclude the F5 refresh
			$(document).keydown(function(e)
			{
			   var key=e.which || e.keyCode;
			   if (key == 116)
			   {
			     validNavigationClose = 1;
			   }
			});

			// Attach the event click for all links in the page
			$("a").bind("click", function()
			{
			   validNavigationClose = 1;
			});

			 // Attach the event submit for all forms in the page
			 $("form").bind("submit", function()
			{
			   validNavigationClose = 1;
			});

			 // Attach the event click for all inputs in the page
			 $("input[type=submit]").bind("click", function()
			{
			   validNavigationClose = 1;
			});

		}

		// Wire up the events as soon as the DOM tree is ready
		$(document).ready(function()
		{
		   bindDOMEvents();
		});
	</script>

	<script>
	/*getToolTips('2');*/

	validNavigation = false;
	clikcServiceFinances = true;

	function reformeBudget(){

		validNavigation = true;
		$.get(InfoUserDetailsLE.route+'/'+InfoUserDetailsLE.email+'/'+'reformeBudget', function(response){

			history.go(-3);
		})

	}

	var desk = document.getElementById ("reformeBudgetDesk").addEventListener ("click", reformeBudget, false);
	var mobile = document.getElementById ("reformeBudgeMobile").addEventListener ("click", reformeBudget, false);


	function wireUpEvents() {
		$(window).on('beforeunload',function() {
			if (!validNavigation) {
				$.ajax({
					type: 'get',
					url: InfoUserDetailsLE.route+'/'+InfoUserDetailsLE.email+'/'+'outPrice',
					async: false
				});
			}
		});


		$('document').bind('keypress', function (e) {
		        if (e.keyCode == 116) {
		            validNavigation = true;
		        }
		    });
	}

	$(document).ready(function(){

		$('#phone').on('click', function(){
			$('html, body').animate({scrollTop:$(document).height()}, 'slow');
		});

		$('#accordion').on('shown.bs.collapse', '.panel-default', function (e) {
		    $('html,body').animate({
		        scrollTop: $(this).offset().top
		    }, 500);
		});

		$(document).on('click','.navbar-collapse.in',function(e) {
		    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
		        $(this).collapse('hide');
		    }
		});

		$('#budgetSucess').on('click', function(){

			validNavigation = true;
			wireUpEvents();
		});
		wireUpEvents();

	})

	function serviceFinances(){
		if(clikcServiceFinances){
			$.ajax({
				type: 'get',
				url: InfoUserDetailsLE.route+'/'+InfoUserDetailsLE.email+'/'+'serviceFinances',
				async: false
			});
			clikcServiceFinances = false;
		}
	}
	</script>

</body>
</html>

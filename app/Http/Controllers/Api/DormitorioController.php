<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\DorSalPasiAlfombras;
use App\Models\Dorsalpasibajomueble;
use App\Models\DorSalPasiEnchufes;
use App\Models\Dorsalpasiintarm;
use App\Models\DorSalPasiJtasuelo;
use App\Models\Dorsalpasilibrosadornos;
use App\Models\DorSalPasiMuebArmar;
use App\Models\Dorsalpasipuertas;
use App\Models\DorSalPasiSillonesColchones;
use App\Models\Dorsalpasisueloparquet;
use App\Models\Dorsalpasivitrina;

class DormitorioController extends Controller
{
    public function dorsalpasialfombras()
    {
        return DorSalPasiAlfombras::get(['id', 'name', 'precio']);
    }
    public function dorsalpasibajomueble()
    {
        return Dorsalpasibajomueble::get(['id', 'name', 'precio']);
    }
    public function dorsalpasienchufes()
    {
        return DorSalPasiEnchufes::get(['id', 'name', 'precio']);
    }
    public function dorsalpasiintarm()
    {
        return Dorsalpasiintarm::get(['id', 'name', 'precio']);
    }
    public function dorsalpasijtasuelo()
    {
        return DorSalPasiJtasuelo::get(['id', 'name', 'precio']);
    }
    public function dorsalpasilibrosadornos()
    {
        return Dorsalpasilibrosadornos::get(['id', 'name', 'precio']);
    }
    public function dorsalpasimuebarmar()
    {
        return DorSalPasiMuebArmar::get(['id', 'name', 'precio']);
    }
    public function dorsalpasipuertas()
    {
        return Dorsalpasipuertas::get(['id', 'name', 'precio']);
    }
    public function dorsalpasisillonescolchones()
    {
        return DorSalPasiSillonesColchones::get(['id', 'name', 'precio']);
    }
    public function dorsalpasisueloparquet()
    {
        return Dorsalpasisueloparquet::get(['id', 'name', 'precio']);
    }
    public function dorsalpasivitrina()
    {
        return Dorsalpasivitrina::get(['id', 'name', 'precio']);
    }
}

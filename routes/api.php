<?php

use Illuminate\Http\Request;

Route::get('/servicios', 'Api\ServiciosController@servicios');
Route::get('/subservicios/{servicio}', 'Api\ServiciosController@subservicios');
Route::get('/provincia', 'Api\RegionController@region');

/*
/ Servicios individuales
/
 */

Route::get('/viviendatipo', 'Api\TipoViviendaController@viviendatipo');
Route::get('/dimension', 'Api\TipoViviendaController@dimension');
Route::get('/estancia', 'Api\TipoViviendaController@estancia');

/*
/ Servicios cocina
/
 */
Route::get('/cocinainteriorcajonesarmario', 'Api\CocinaController@cocinainteriorcajones');
Route::get('/cocinainteriormueble', 'Api\CocinaController@cocinainteriormueble');
Route::get('/cocinainteriorelectr', 'Api\CocinaController@cocinainteriorelectr');
Route::get('/cocinainteriorhorno', 'Api\CocinaController@cocinainteriorhorno');
Route::get('/cocinainteriorazulejo', 'Api\CocinaController@cocinainteriorazulejo');
Route::get('/cocinainteriorsuelo', 'Api\CocinaController@cocinainteriorsuelo');

/*
/ Servicios Baños
/
 */
Route::get('/banohongos', 'Api\AseoController@banohongos');
Route::get('/banointcajarm', 'Api\AseoController@banointcajarm');
Route::get('/banojtaazulejo', 'Api\AseoController@banojtaazulejo');
Route::get('/banojtasuelo', 'Api\AseoController@banojtasuelo');
Route::get('/banomampara', 'Api\AseoController@banomampara');
Route::get('/banoplatoducha', 'Api\AseoController@banoplatoducha');
/*
/ Servicios Dormitorio salon y pasillo
/
 */
Route::get('/dorsalpasialfombras', 'Api\DormitorioController@dorsalpasialfombras');
Route::get('/dorsalpasibajomueble', 'Api\DormitorioController@dorsalpasibajomueble');
Route::get('/dorsalpasienchufes', 'Api\DormitorioController@dorsalpasienchufes');
Route::get('/dorsalpasiintarm', 'Api\DormitorioController@dorsalpasiintarm');
Route::get('/dorsalpasijtasuelo', 'Api\DormitorioController@dorsalpasijtasuelo');
Route::get('/dorsalpasilibrosadornos', 'Api\DormitorioController@dorsalpasilibrosadornos');
Route::get('/dorsalpasimuebarmar', 'Api\DormitorioController@dorsalpasimuebarmar');
Route::get('/dorsalpasipuertas', 'Api\DormitorioController@dorsalpasipuertas');
Route::get('/dorsalpasisillonescolchones', 'Api\DormitorioController@dorsalpasisillonescolchones');
Route::get('/dorsalpasisueloparquet', 'Api\DormitorioController@dorsalpasisueloparquet');
Route::get('/dorsalpasivitrina', 'Api\DormitorioController@dorsalpasivitrina');
/*
/ Ventans y cerramientos exteriores
/
 */
Route::get('/ventcerrcontvent', 'Api\VentanasController@ventcerrcontvent');
Route::get('/ventcerrintpers', 'Api\VentanasController@ventcerrintpers');
Route::get('/ventcerrpersvenec', 'Api\VentanasController@ventcerrpersvenec');
Route::get('/ventcerrventmarco', 'Api\VentanasController@ventcerrventmarco');
Route::get('/ventcerrventpan', 'Api\VentanasController@ventcerrventpan');
/*
/ Terrazas, exteriores y tendederos
/
 */
Route::get('/terrextarmextint', 'Api\TerrazaController@terrextarmextint');
Route::get('/terrextbarandilla', 'Api\TerrazaController@terrextbarandilla');
Route::get('/terrextcepparedes', 'Api\TerrazaController@terrextcepparedes');
Route::get('/terrextsuelo', 'Api\TerrazaController@terrextsuelo');


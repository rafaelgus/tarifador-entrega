<?php

namespace App\Http\Controllers;

use App\Models\PresupuestoFecha;
use Illuminate\Http\Request;

use App\Models\Presupuesto;

class PresupuestoFechaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $presup = Presupuesto::find($id);

        return view('fecha', ['presup' => $presup]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PresupuestoFecha  $presupuestoFecha
     * @return \Illuminate\Http\Response
     */
    public function show(PresupuestoFecha $presupuestoFecha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PresupuestoFecha  $presupuestoFecha
     * @return \Illuminate\Http\Response
     */
    public function edit(PresupuestoFecha $presupuestoFecha)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PresupuestoFecha  $presupuestoFecha
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PresupuestoFecha $presupuestoFecha, $id)
    {
        $presup = Presupuesto::find($id);

        $fecha = PresupuestoFecha::where('presupuesto_id', $id)->update([
            'fecha' => $request->fecha,
        ]);






        return redirect()->route('solicitante.index', ['id' => $presup->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PresupuestoFecha  $presupuestoFecha
     * @return \Illuminate\Http\Response
     */
    public function destroy(PresupuestoFecha $presupuestoFecha)
    {
        //
    }
}

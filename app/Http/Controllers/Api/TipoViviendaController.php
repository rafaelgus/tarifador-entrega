<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Viviendatipo;
use App\Models\Dimension;
use App\Models\Estancia;

class TipoViviendaController extends Controller
{
    public function viviendatipo()
    {
        return Viviendatipo::get(['id', 'tipo', 'precio']);
    }

    public function dimension()
    {
        return Dimension::get(['id', 'dimension', 'precio']);
    }

    public function estancia()
    {
        return Estancia::get(['id', 'cantidad', 'precio']);
    }
}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Personalice su servicio de limpieza de inmuebles</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/favicon.png" />
    <link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/reset.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/style.css">
    <!-- Resource style -->
    <script src="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/modernizr.js"></script>
    <!-- Modernizr -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">

    <!-- tooltipster -->
    <link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/jquery.tooltip.css">

    <!-- Confirmatiosn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/slider-navbar-right.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/css.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-5048875-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-5048875-1');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 991546510 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-991546510"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-991546510');
    </script>

    <script>
        window.InfoUserLE = {
            "email": "",
            "route": "https:\/\/presupuesto-limpieza-online.limpiezasexpress.com\/tracking\/event\/ac",
            "route_home": "https:\/\/presupuesto-limpieza-online.limpiezasexpress.com",
            "list_id_ac": "118"
        };
    </script>

    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Organization",
            "name": "Limpiezas Express",
            "url": "http://www.limpiezasexpress.com/",
            "logo": "http://www.limpiezasexpress.com/uploads/1/0/2/8/1028263/1457599333.png",
            "sameAs": [
                "https://www.facebook.com/LimpiezasExpress/",
                "https://twitter.com/limpiezaexpress",
                "https://www.youtube.com/user/LimpiezasExpress",
                "https://plus.google.com/+Limpiezasexpress"
            ]
        }
    </script>

    <style>
        #cover{
	    		background: url(https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/loading-gif.gif) no-repeat scroll center center #fff;
	    		position: fixed;
	    		height: 100%;
	    		width: 100%;
	    		overflow: hidden;
	    		z-index: 999;
	    		background-size: 140px;
	    	}
	    </style>
    <meta name="description" content="Elija qué quiere limpiar entre las más de 100 zonas de su hogar. Limpiezas a fondo. Profesionales de confianza y maquinaria especializada.">
    <meta name="keywords" content="limpiezas a fondo casas, limpiar pisos, limpieza casas, empresa limpieza a domicilio, empresa limpieza pisos">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Product",
            "name": "Limpiezas a fondo profesionales",
            "description": "Limpiezas fin de obra, alquileres, casas habitadas, etc. Operarios, maquinaria y seguro de responsabilidad civil incluidos",
            "url": "https://presupuesto-limpieza-online.limpiezasexpress.com/",
            "brand": {
                "@type": "Brand",
                "name": "Limpiezas Express",
                "logo": "https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png"
            },
            "offers": {
                "@type": "Offer",
                "price": "Desde 180€"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "4,8",
                "bestRating": "5",
                "worstRating": "1",
                "ratingCount": "106"
            }
        }
    </script>


</head>

<body>
    <div id="cover"></div>
    <nav class="navbar navbar-default" id="navbar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                            aria-expanded="false">
                            <!-- <span class="sr-only">Toggle navigation</span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span> -->
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </button>
                        <a href="https://presupuesto-limpieza-online.limpiezasexpress.com"><img src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png"
                                alt="Logo Limpiezas Express" id="logo" class="img-responsive"></a>
                        <!-- <a class="navbar-brand" href="" id="">Kode<strong>Working</strong></a> -->
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">

                            <li id="navbar-number"><span>Si tiene dudas sobre la contratación llame al: <a href="tel:+34910325262"><u>91
                                            0325262</u></a></span></li>
                            <!-- <li id="navbar-slider-call">

							<a id="menu-toggle" href="#" class="">¿Prefieres que te llamemos?</a>
								<div id="sidebar-wrapper">

									<a id="menu-close" href="#" class="pull-right toggle"><i class="glyphicon glyphicon-remove"></i></a>


									<h1>Slider navbar right</h1>
								</div>
	    				</li> -->

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>


    <style>
        .dnone{
		display: none;
	}
	.dsuccess{
		display: block;
	}
	body{
		font-size: 16px;
	}
	#task_flyout {height:90px; background-color:#FFF}

	.fixed {position:fixed; top:0; left:0; z-index:2; width:100%;}

	.tick-success{
	    width: 30px;height: 30px;border: solid 2px #0bba2e;border-radius: 50%; font-size: 20px;text-align: center;padding: 2px 0px;display: none;
	}

	.tick-success:after{
		content: "\f00c";
	    font-family: FontAwesome;color:#0bba2e;
	}

	#scrollToTop
    {
         cursor:pointer;
         background-color:#0090CB;
         display:inline-block;
         height:40px;
         width:40px;
         color:#fff;
         font-size:16pt;
         text-align:center;
         text-decoration:none;
         line-height:40px;
         position: fixed;
         bottom: 0%;
         left: 0%;
    }

	@media  screen
	and (max-width: 768px) {
	}
</style>

    <div class="container-fluid">
        <!-- <div class="row">
		<div class="col-lg-12 text-center">
			<h2 class="title-head">PRESUPUESTADOR LIMPIEZAS EXPRESS</h2>

		</div>
	</div> -->
        <section id="task_flyout">
            <nav>
                <ol class="cd-multi-steps text-bottom count">
                    <li class="current"><a href="javascript:;">Definir servicio</a></li>
                    <li class=""><em>Elegir fecha</em></li>
                    <li class=""><em>Ver presupuesto</em></li>
                    <li><em>Realizar reserva</em></li>
                </ol>
            </nav>
        </section>

        <div class="row">
            <div class="col-lg-12">
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">

                <div class="content-budget">


                    <!-- <div class="row">
					<div class="col-lg-12 text-center">
						<h1>¿CÓMO LO NECESITA?</h1>
					</div>
				</div> -->
                    <form action="https://presupuesto-limpieza-online.limpiezasexpress.com/service/setup/2" method="post">

                        <input type="hidden" name="_token" value="jCG0EBvfYO3HFLmzeThVvB5b1HZukcCG2VrCIDsu">

                        <div class="row">
                            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label for="">¿Qué tipo de vivienda es? <span class="hastip" title="Indique a continuación si vive en un piso, ático, dúplex o chalet."><small
                                                    class="text-primary">más info</small></span></label>
                                        <div class="row mb-20">

                                            <div class="col-xs-12 col-sm-3">

                                                <div class="radio-button">
                                                    <input type="radio" id="tp1" value="1" name="type_home" required
                                                        data-home="0" class="type_home first-question-enable-continue">

                                                    <label for="tp1" class="radio-inline mt-10">
                                                        Piso
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="col-xs-12 col-sm-3">

                                                <div class="radio-button">
                                                    <input type="radio" id="tp2" value="2" name="type_home" required
                                                        data-home="0" class="type_home first-question-enable-continue">

                                                    <label for="tp2" class="radio-inline mt-10">
                                                        Casa/Chalet
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="col-xs-12 col-sm-3">

                                                <div class="radio-button">
                                                    <input type="radio" id="tp3" value="3" name="type_home" required
                                                        data-home="0" class="type_home first-question-enable-continue">

                                                    <label for="tp3" class="radio-inline mt-10">
                                                        D&uacute;plex
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="col-xs-12 col-sm-3">

                                                <div class="radio-button">
                                                    <input type="radio" id="tp4" value="4" name="type_home" required
                                                        data-home="0" class="type_home first-question-enable-continue">

                                                    <label for="tp4" class="radio-inline mt-10">
                                                        &Aacute;tico
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="line-bottom"></p>
                                    </div>
                                </div>

                                <div class="row mt-20">
                                    <div class="col-xs-12">
                                        <label for="" class="">¿Cuántos metros<sup>2</sup> tiene la vivienda? <span
                                                class="hastip" title="Indique los m2 útiles de su vivienda incluyendo la suma de todas las terrazas y balcones. Esta suma no debe ser superior a 20m2."><small
                                                    class="text-primary">más info</small></span></label>

                                        <input type="number" min="35" max="180" class="form-control first-question-enable-continue"
                                            name="m2_home" required id="m2_home" value="" style="width: 200px !important;">

                                        <p>Si su vivienda tiene menos de 35m2, más de 180m2 o su terraza y/o balcones
                                            suman más de 20m2 <a href="https://www.limpiezasexpress.com/presupuestos-limpieza-pisos-y-casas/?utm_source=Limpieza%20Casas&utm_medium=Limpieza%20Casas&utm_campaign=Enlace%20m2%20desde%20presupuestador"><strong>clique
                                                    aquí para un presupuesto personalizado. </strong></a></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="line-bottom"></p>
                                    </div>
                                </div>

                                <div class="row mt-20">
                                    <div class="col-xs-12">
                                        <label for="" class="">¿Cuántas estancias de todo tipo hay en la vivienda?
                                            <span class="hastip" title="Indique aquí el número de estancias de su vivienda entendiendo por estancia cada habitación, dormitorio, salón, cocina, baños, terraza, tendederos o galerías, lavadero, despensas etc."><small
                                                    class="text-primary">más info</small></span></label>

                                        <select name="stants" id="stants" class="form-control first-question-enable-continue"
                                            required style="width: 200px !important;">

                                            <option value="1">1</option>


                                            <option value="2">2</option>


                                            <option value="3">3</option>


                                            <option value="4">4</option>


                                            <option value="5">5</option>


                                            <option value="6">6</option>


                                            <option value="7">7</option>


                                            <option value="8">8</option>


                                            <option value="9">9</option>


                                            <option value="10">10</option>


                                            <option value="11">11</option>


                                            <option value="12">12</option>


                                            <option value="13">13</option>


                                            <option value="14">14</option>


                                            <option value="15">15</option>


                                            <option value="16">16</option>


                                            <option value="17">17</option>


                                            <option value="18">18</option>


                                            <option value="19">19</option>


                                            <option value="20">20</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="line-bottom"></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <!--#collapseFirst1 top1-->


                                        <a href="javascript:;" role="button" class="btn btn-success btn-lg pull-right btn-le-green mt-20"
                                            data-position-top="" data-continue="1" data-toggle="" data-parent="" href="#"
                                            aria-controls="collapseFirst1" id="firstContinue" onclick="checkFirstContinue()">CONTINUAR</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <input type="hidden" name="count_q" value="36">
                            <div class="col-xs-12 col-lg-12">

                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">




                                    <div class="panel panel-default mt-50" id="top1">
                                        <div class="panel-heading heading-title-zone boder-color-green" role="tab" id="heading1">
                                            <h4 class="panel-title">

                                                <h3 class="heading-text-zone"><strong>COCINA</strong><span id="tick-1"
                                                        class="tick-success"></span></h3>


                                            </h4>
                                        </div>

                                        <div id="collapseFirst1" class="panel-collapse collapse in" role="tabpanel"
                                            aria-labelledby="heading1">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <p></p>

















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>La limpieza exterior
                                                                        de los armarios y cajones ya est&aacute;
                                                                        incluida en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s que se limpie su interior? Y en
                                                                        el caso de querer, &iquest;estos armarios y
                                                                        cajones est&aacute;n llenos o vac&iacute;os de
                                                                        enseres?</strong> <span class="hastip" title="Se limpia el polvo y suciedad de los armarios y, en caso de estar llenos de objetos o enseres, se vac&iacute;an para poder limpiar su interior as&iacute; como los cajones, y posteriormente, colocar dichos enseres en su lugar"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question36" value="47425">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer36"
                                                                        class="answer" data-id="36" data-quantity="0"
                                                                        data-multiple="1" id="r103347" required checked>
                                                                    <label class="radio-inline mt-10" for="r103347">
                                                                        No quiero limpiarlos
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="2.00" name="answer36"
                                                                        class="answer" data-id="36" data-quantity="0"
                                                                        data-multiple="1" id="r103348" required>
                                                                    <label class="radio-inline mt-10" for="r103348">
                                                                        S&iacute;, quiero limpiarlos y est&aacute;n
                                                                        vac&iacute;os
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="5.00" name="answer36"
                                                                        class="answer" data-id="36" data-quantity="0"
                                                                        data-multiple="1" id="r103349" required>
                                                                    <label class="radio-inline mt-10" for="r103349">
                                                                        S&iacute;, quiero limpiarlos y est&aacute;n
                                                                        llenos
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>La limpieza exterior
                                                                        de los cajones del mobiliario ya est&aacute;
                                                                        incluida en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s que los cajones sean
                                                                        extra&iacute;dos de su mueble y se limpie tanto
                                                                        el interior del caj&oacute;n como el de su
                                                                        mueble?</strong> <span class="hastip" title="Limpieza del polvo y suciedad acumulada en la estructura de los cajones as&iacute; como sus ra&iacute;les, y el interior del propio caj&oacute;n.  No se desmontar&aacute;n si est&aacute;n atornillados y se limpiar&aacute;n siempre que su extracci&oacute;n sea f&aacute;cil"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question35" value="47431">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer35"
                                                                        class="answer" data-id="35" data-quantity="0"
                                                                        data-multiple="1" id="r103362" required checked>
                                                                    <label class="radio-inline mt-10" for="r103362">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer35"
                                                                        class="answer" data-id="35" data-quantity="0"
                                                                        data-multiple="1" id="r103363" required>
                                                                    <label class="radio-inline mt-10" for="r103363">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>La limpieza exterior
                                                                        de horno, campana, nevera y microondas ya
                                                                        est&aacute; incluida, &iquest;quiere que
                                                                        tambi&eacute;n se limpie su interior? Y en el
                                                                        caso de querer limpiarlos, &iquest;c&oacute;mo
                                                                        se encuentran? Poco sucio es cuando hay algunos
                                                                        restos de comida y no est&aacute;n endurecidos,
                                                                        muy sucio lleva m&aacute;s de un mes sin
                                                                        limpiarse a fondo y los restos de comida
                                                                        est&aacute;n endurecidos.</strong> <span class="hastip"
                                                                        title="Se desmontan filtros/rejillas de la campana para eliminar la grasa acumulada y reseca, igual con el horno y su estructura interior. Se limpia la nevera, baldas, puerta y sus gomas, etc. Atenci&oacute;n, cualquier electrodom&eacute;stico muy degradado, y en particular los quemadores de gas y el horno, no se garantiza en absoluto que vayan a quedar limpios e impolutos"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question34" value="47430">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer34"
                                                                        class="answer" data-id="34" data-quantity="0"
                                                                        data-multiple="0" id="r103359" required checked>
                                                                    <label class="radio-inline mt-10" for="r103359">
                                                                        No quiero limpiarlos
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="2.50" name="answer34"
                                                                        class="answer" data-id="34" data-quantity="0"
                                                                        data-multiple="0" id="r103360" required>
                                                                    <label class="radio-inline mt-10" for="r103360">
                                                                        S&iacute;, quiero limpiarlos y est&aacute;n
                                                                        poco sucios
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="5.00" name="answer34"
                                                                        class="answer" data-id="34" data-quantity="0"
                                                                        data-multiple="0" id="r103361" required>
                                                                    <label class="radio-inline mt-10" for="r103361">
                                                                        S&iacute;, quiero limpiarlos y est&aacute;n muy
                                                                        sucios
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        los electrodom&eacute;sticos y los
                                                                        embellecedores sean retirados y se limpien
                                                                        debajo de &eacute;stos y bajo los muebles,
                                                                        adem&aacute;s del suelo y paredes donde
                                                                        est&aacute;n instalados y encajados?</strong>
                                                                    <span class="hastip" title="Siempre que sea posible y no est&eacute;n atornillados, se mueven los electrodom&eacute;sticos para limpiar la parte posterior, as&iacute; como la zona en la que han estado apoyados desde su instalaci&oacute;n (suelos y paredes que suelen estar muy sucios)"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question33" value="47432">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer33"
                                                                        class="answer" data-id="33" data-quantity="0"
                                                                        data-multiple="1" id="r103364" required checked>
                                                                    <label class="radio-inline mt-10" for="r103364">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer33"
                                                                        class="answer" data-id="33" data-quantity="0"
                                                                        data-multiple="1" id="r103365" required>
                                                                    <label class="radio-inline mt-10" for="r103365">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47432"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47432">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/kso6so_Fj_k?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>












                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>El fregado de
                                                                        azulejos ya est&aacute; incluido en la limpieza
                                                                        base, &iquest;quiere adem&aacute;s que se
                                                                        limpien las juntas de los azulejos
                                                                        (uni&oacute;n de las plaquetas)?</strong> <span
                                                                        class="hastip" title="Las juntas son la uni&oacute;n de cemento o yeso de los azulejos. Podremos usar t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question32" value="47434">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer32"
                                                                        class="answer" data-id="32" data-quantity="0"
                                                                        data-multiple="1" id="r103368" required checked>
                                                                    <label class="radio-inline mt-10" for="r103368">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer32"
                                                                        class="answer" data-id="32" data-quantity="0"
                                                                        data-multiple="1" id="r103369" required>
                                                                    <label class="radio-inline mt-10" for="r103369">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47434"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47434">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/muDzXVYXVCI?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>









                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>El barrido y/o
                                                                        aspirado y el fregado del suelo ya est&aacute;
                                                                        incluido en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s que se limpien las juntas del
                                                                        suelo?</strong> <span class="hastip" title="Las juntas son la uni&oacute;n de cemento o yeso de la plaqueta o loseta. Podremos usar t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question31" value="47433">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer31"
                                                                        class="answer" data-id="31" data-quantity="0"
                                                                        data-multiple="1" id="r103366" required checked>
                                                                    <label class="radio-inline mt-10" for="r103366">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.50" name="answer31"
                                                                        class="answer" data-id="31" data-quantity="0"
                                                                        data-multiple="1" id="r103367" required>
                                                                    <label class="radio-inline mt-10" for="r103367">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>



















                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <a role="button" class="btn-continue" data-position-top="top1"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst2"
                                                            aria-controls="collapseFirst2" data-count-continue="1"><button
                                                                class="btn btn-success btn-lg pull-right btn-le-green mt-20">CONTINUAR</button></a>



                                                    </div>
                                                </div>



                                            </div>
                                        </div>


                                    </div>





























































                                    <div class="panel panel-default mt-50" id="top2">
                                        <div class="panel-heading heading-title-zone boder-color-green" role="tab" id="heading2">
                                            <h4 class="panel-title">

                                                <h3 class="heading-text-zone"><strong>BA&Ntilde;OS Y ASEOS</strong><span
                                                        id="tick-2" class="tick-success"></span></h3>


                                            </h4>
                                        </div>

                                        <div id="collapseFirst2" class="panel-collapse collapse in" role="tabpanel"
                                            aria-labelledby="heading2">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <p></p>














                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpie el interior de los armarios y
                                                                        cajones? Si es as&iacute;,
                                                                        &iquest;cu&aacute;ntos quiere que se limpien?</strong>
                                                                    <span class="hastip" title="Se sacar&aacute;n los enseres para poder acabar con la suciedad y el polvo y posteriormente, se volver&aacute;n a meter en los armarios y cajones en caso de que est&eacute;n llenos"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question30" value="47436">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer30"
                                                                        class="answer" data-id="30" data-quantity="1"
                                                                        data-multiple="0" id="r103372" required checked>
                                                                    <label class="radio-inline mt-10" for="r103372">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.41" name="answer30"
                                                                        class="answer" data-id="30" data-quantity="1"
                                                                        data-multiple="0" id="r103373" required>
                                                                    <label class="radio-inline mt-10" for="r103373">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity30" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="30">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>La limpieza de
                                                                        espejos ya est&aacute; incluida en la limpieza
                                                                        base, &iquest;quiere adem&aacute;s que se
                                                                        limpie la mampara de la ba&ntilde;era o la
                                                                        ducha, si tuviera?</strong> <span class="hastip"
                                                                        title="Limpieza de cal, grasa corporal y otros residuos con productos espec&iacute;ficos y esponja suave, en caso de que as&iacute; lo permita el material de la mampara"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question29" value="47435">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer29"
                                                                        class="answer" data-id="29" data-quantity="0"
                                                                        data-multiple="1" id="r103370" required checked>
                                                                    <label class="radio-inline mt-10" for="r103370">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.51" name="answer29"
                                                                        class="answer" data-id="29" data-quantity="0"
                                                                        data-multiple="1" id="r103371" required>
                                                                    <label class="radio-inline mt-10" for="r103371">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpien los sanitarios, plato de ducha y/o
                                                                        ba&ntilde;eras?</strong> <span class="hastip"
                                                                        title="Higienizaci&oacute;n y desinfecci&oacute;n a fondo de sanitarios, platos de ducha y ba&ntilde;eras. Si la zona y material lo permite, se utiliza en general m&aacute;quina de vaporizaci&oacute;n / Vaporeta, o alternativamente se realiza una limpieza a fondo y meticulosa de la zona"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question28" value="47427">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer28"
                                                                        class="answer" data-id="28" data-quantity="0"
                                                                        data-multiple="1" id="r103352" required checked>
                                                                    <label class="radio-inline mt-10" for="r103352">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="3.07" name="answer28"
                                                                        class="answer" data-id="28" data-quantity="0"
                                                                        data-multiple="1" id="r103353" required>
                                                                    <label class="radio-inline mt-10" for="r103353">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>El fregado de
                                                                        azulejos de las paredes ya est&aacute; incluido
                                                                        en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s que se limpien las juntas de la
                                                                        pared, en el caso que fueran de azulejos?</strong>
                                                                    <span class="hastip" title="Las juntas son la uni&oacute;n de cemento o yeso de los azulejos. Podremos usar t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question27" value="47439">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer27"
                                                                        class="answer" data-id="27" data-quantity="0"
                                                                        data-multiple="1" id="r103378" required checked>
                                                                    <label class="radio-inline mt-10" for="r103378">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.03" name="answer27"
                                                                        class="answer" data-id="27" data-quantity="0"
                                                                        data-multiple="1" id="r103379" required>
                                                                    <label class="radio-inline mt-10" for="r103379">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>












                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere
                                                                        limpiar las zonas ennegrecidas en sanitarios
                                                                        por hongos o moho en caso de que tuviera? Si
                                                                        desea realizar esta limpieza, tenga en cuenta
                                                                        que no se retirar&aacute; la silicona.</strong>
                                                                    <span class="hastip" title="Utilizaci&oacute;n de productos con efecto antif&uacute;ngico y antimoho y limpieza posterior en toda la superficie afectada. Dependiendo del material donde est&eacute;n y la profundidad a la que hayan penetrado, puede ocurrir que no sea posible eliminarlos completamente"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question26" value="47437">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer26"
                                                                        class="answer" data-id="26" data-quantity="0"
                                                                        data-multiple="1" id="r103374" required checked>
                                                                    <label class="radio-inline mt-10" for="r103374">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.41" name="answer26"
                                                                        class="answer" data-id="26" data-quantity="0"
                                                                        data-multiple="1" id="r103375" required>
                                                                    <label class="radio-inline mt-10" for="r103375">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47437"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47437">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/s17XksSBv9Y?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>









                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>El barrido y/o
                                                                        aspirado y el fregado del suelo ya est&aacute;
                                                                        incluido en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s que se limpien las juntas del
                                                                        suelo?</strong> <span class="hastip" title="Las juntas son la uni&oacute;n de cemento o yeso de la plaqueta o loseta. Podremos usar t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question25" value="47438">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer25"
                                                                        class="answer" data-id="25" data-quantity="0"
                                                                        data-multiple="1" id="r103376" required checked>
                                                                    <label class="radio-inline mt-10" for="r103376">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.26" name="answer25"
                                                                        class="answer" data-id="25" data-quantity="0"
                                                                        data-multiple="1" id="r103377" required>
                                                                    <label class="radio-inline mt-10" for="r103377">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47438"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47438">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/k6R9nxnaao4?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>






















                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <a role="button" class="btn-continue" data-position-top="top2"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst3"
                                                            aria-controls="collapseFirst3" data-count-continue="2"><button
                                                                class="btn btn-success btn-lg pull-right btn-le-green mt-20">CONTINUAR</button></a>

                                                        <a role="button" class="btn-back" data-position-top="top1"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst1"
                                                            aria-controls="collapseFirst1" data-count-continue="1"
                                                            onclick="backScrollp('1' , '1')"><button class="btn btn-danger btn-lg pull-left btn-le-danger"
                                                                style="margin: 20px 0px;">VOLVER</button></a>




                                                    </div>
                                                </div>



                                            </div>
                                        </div>


                                    </div>





























































                                    <div class="panel panel-default mt-50" id="top3">
                                        <div class="panel-heading heading-title-zone boder-color-green" role="tab" id="heading3">
                                            <h4 class="panel-title">

                                                <h3 class="heading-text-zone"><strong>DORMITORIOS, SAL&Oacute;N Y
                                                        PASILLOS</strong><span id="tick-3" class="tick-success"></span></h3>


                                            </h4>
                                        </div>

                                        <div id="collapseFirst3" class="panel-collapse collapse in" role="tabpanel"
                                            aria-labelledby="heading3">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <p></p>











                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>La limpieza de
                                                                        paredes quitando el polvo, las
                                                                        telara&ntilde;as, pelusas etc as&iacute; como
                                                                        la limpieza de espejos ya est&aacute; incluida
                                                                        en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s que se limpien los enchufes,
                                                                        interruptores, radiadores, rodapi&eacute;s,
                                                                        salidas de aire acondicionado y
                                                                        apliques/l&aacute;mparas de la vivienda?</strong>
                                                                    <span class="hastip" title="Limpieza individualizada de enchufes, interruptores, rodapi&eacute;s, etc. para evitar la proliferaci&oacute;n de &aacute;caros y otra suciedad enquistada"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question24" value="47443">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer24"
                                                                        class="answer" data-id="24" data-quantity="0"
                                                                        data-multiple="1" id="r103386" required checked>
                                                                    <label class="radio-inline mt-10" for="r103386">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="4.00" name="answer24"
                                                                        class="answer" data-id="24" data-quantity="0"
                                                                        data-multiple="1" id="r103387" required>
                                                                    <label class="radio-inline mt-10" for="r103387">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong> &iquest;Quiere que
                                                                        sus sof&aacute;s, sillones y colchones sean
                                                                        aspirados y vaporizados?</strong> <span class="hastip"
                                                                        title="Si es posible, se desmontan los sof&aacute;s para poder limpiar entre los cojines. No se retiran las fundas para su posterior lavado. Si el material y la zona lo permite, se utiliza m&aacute;quina de vaporizaci&oacute;n / Vaporeta"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question23" value="47440">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer23"
                                                                        class="answer" data-id="23" data-quantity="0"
                                                                        data-multiple="1" id="r103380" required checked>
                                                                    <label class="radio-inline mt-10" for="r103380">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="2.00" name="answer23"
                                                                        class="answer" data-id="23" data-quantity="0"
                                                                        data-multiple="1" id="r103381" required>
                                                                    <label class="radio-inline mt-10" for="r103381">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47440"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47440">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/m4c4pTZmVSQ?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong> &iquest;Quiere que
                                                                        sus alfombras sean aspiradas y vaporizadas? Si
                                                                        es as&iacute;, &iquest;cu&aacute;ntas?</strong>
                                                                    <span class="hastip" title="Aspirado de la superficie y si el material lo permite, se utiliza en general m&aacute;quina de vaporizaci&oacute;n / Vaporeta para eliminar los &aacute;caros pero no las manchas permanentes del tejido (en caso de querer, llame al tel&eacute;fono 24h: 91 0325262 para presupuesto personalizado). Si hay elementos pesados no se retirar&aacute;n y se limpiar&aacute; alrededor"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question22" value="47442">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer22"
                                                                        class="answer" data-id="22" data-quantity="1"
                                                                        data-multiple="0" id="r103384" required checked>
                                                                    <label class="radio-inline mt-10" for="r103384">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.50" name="answer22"
                                                                        class="answer" data-id="22" data-quantity="1"
                                                                        data-multiple="0" id="r103385" required>
                                                                    <label class="radio-inline mt-10" for="r103385">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity22" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="22">
                                                            </div>


                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47442"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47442">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/7HvP2Kxq50A?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong> &iquest;Quiere que
                                                                        se frieguen las puertas de la vivienda? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntas quiere
                                                                        fregar?</strong> <span class="hastip" title="Limpieza de polvo, retirada de suciedad incrustada en la puerta, en sus marcos, dintel y bisagras, pomos o tiradores. No se aplican ceras ni ning&uacute;n tipo de reparador de maderas"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question21" value="47441">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer21"
                                                                        class="answer" data-id="21" data-quantity="1"
                                                                        data-multiple="0" id="r103382" required checked>
                                                                    <label class="radio-inline mt-10" for="r103382">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.50" name="answer21"
                                                                        class="answer" data-id="21" data-quantity="1"
                                                                        data-multiple="0" id="r103383" required>
                                                                    <label class="radio-inline mt-10" for="r103383">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity21" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="21">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>












                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpie el interior de los armarios roperos
                                                                        y/o empotrados? Si es as&iacute;,
                                                                        &iquest;cu&aacute;ntos armarios quiere limpiar?</strong>
                                                                    <span class="hastip" title="Un armario ropero es aquel de dos puertas. En el caso de que el suyo tenga cuatro puertas, marque en el selector dos armarios. No se ordenan los enseres del interior del armario; se colocan como estaban. No se hace colada ni se plancha"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question20" value="47444">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer20"
                                                                        class="answer" data-id="20" data-quantity="1"
                                                                        data-multiple="0" id="r103388" required checked>
                                                                    <label class="radio-inline mt-10" for="r103388">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.50" name="answer20"
                                                                        class="answer" data-id="20" data-quantity="1"
                                                                        data-multiple="0" id="r103389" required>
                                                                    <label class="radio-inline mt-10" for="r103389">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity20" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="20">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>









                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpie el interior de la vitrina del
                                                                        sal&oacute;n y/o mueble mural? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntos muebles
                                                                        quiere limpiar?</strong> <span class="hastip"
                                                                        title="Este mueble-vitrina de sal&oacute;n es el t&iacute;pico mueble grande o mueble bar, situado en el sal&oacute;n de las viviendas, que puede llegar a medir hasta 4 metros de longitud. En caso de estar llenos de objetos o enseres, se vac&iacute;an para poder limpiar su interior, y posteriormente, colocar dichos enseres en su lugar"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question19" value="47446">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer19"
                                                                        class="answer" data-id="19" data-quantity="1"
                                                                        data-multiple="0" id="r103392" required checked>
                                                                    <label class="radio-inline mt-10" for="r103392">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="5.00" name="answer19"
                                                                        class="answer" data-id="19" data-quantity="1"
                                                                        data-multiple="0" id="r103393" required>
                                                                    <label class="radio-inline mt-10" for="r103393">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity19" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="19">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>









                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>En el caso de que el
                                                                        material del suelo fuera parquet &iquest;quiere
                                                                        que le apliquemos cera l&iacute;quida?</strong>
                                                                    <span class="hastip" title="Se barre y aspira el suelo para quitar el polvo y suciedad, y se friega para evitar que los restos se queden impregnados en el suelo. Posteriormente, se aplicar&aacute; cera l&iacute;quida"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question18" value="47450">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer18"
                                                                        class="answer" data-id="18" data-quantity="0"
                                                                        data-multiple="1" id="r103402" required checked>
                                                                    <label class="radio-inline mt-10" for="r103402">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer18"
                                                                        class="answer" data-id="18" data-quantity="0"
                                                                        data-multiple="1" id="r103403" required>
                                                                    <label class="radio-inline mt-10" for="r103403">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47450"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47450">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/MbScs_G7y6M?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>



                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpie el interior de muebles bajos como
                                                                        aparadores, c&oacute;modas o cajoneras? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntos quiere
                                                                        limpiar?</strong> <span class="hastip" title="Se limpia el polvo de los muebles y, en caso de estar llenos de objetos o enseres, se vac&iacute;an para poder limpiar su estructura interior as&iacute; como los cajones, y posteriormente, colocar dichos enseres en su lugar"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question17" value="47447">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer17"
                                                                        class="answer" data-id="17" data-quantity="1"
                                                                        data-multiple="0" id="r103394" required checked>
                                                                    <label class="radio-inline mt-10" for="r103394">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer17"
                                                                        class="answer" data-id="17" data-quantity="1"
                                                                        data-multiple="0" id="r103395" required>
                                                                    <label class="radio-inline mt-10" for="r103395">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity17" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="17">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>



                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpien libros, adornos, aparatos
                                                                        electr&oacute;nicos o marcos de fotos? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntos quiere
                                                                        limpiar?</strong> <span class="hastip" title="Se limpia individualmente cada objeto para eliminar el polvo y suciedad acumulada. No se limpian peque&ntilde;os objetos o figuritas, cds de m&uacute;sica, colecciones u otros elementos delicados ni se aspiran los libros."><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question16" value="47448">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer16"
                                                                        class="answer" data-id="16" data-quantity="0"
                                                                        data-multiple="0" id="r103396" required checked>
                                                                    <label class="radio-inline mt-10" for="r103396">
                                                                        No quiero limpiarlos
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer16"
                                                                        class="answer" data-id="16" data-quantity="0"
                                                                        data-multiple="0" id="r103397" required>
                                                                    <label class="radio-inline mt-10" for="r103397">
                                                                        0-100
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="3.00" name="answer16"
                                                                        class="answer" data-id="16" data-quantity="0"
                                                                        data-multiple="0" id="r103398" required>
                                                                    <label class="radio-inline mt-10" for="r103398">
                                                                        100-500
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="6.00" name="answer16"
                                                                        class="answer" data-id="16" data-quantity="0"
                                                                        data-multiple="0" id="r103399" required>
                                                                    <label class="radio-inline mt-10" for="r103399">
                                                                        500-1000
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47448"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47448">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/Gve77k0bAjA?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>



                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se retiren los muebles y/o armarios y se limpie
                                                                        la parte posterior de &eacute;stos as&iacute;
                                                                        como las paredes y suelo donde se encuentran?</strong>
                                                                    <span class="hastip" title="Siempre que sea posible, se retiran los muebles y se limpia el mueble por detr&aacute;s, la pared y el suelo. En el caso de que estos muebles sean grandes o pesados tienen que estar vac&iacute;os para poder moverlos"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question15" value="47445">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer15"
                                                                        class="answer" data-id="15" data-quantity="0"
                                                                        data-multiple="1" id="r103390" required checked>
                                                                    <label class="radio-inline mt-10" for="r103390">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="2.00" name="answer15"
                                                                        class="answer" data-id="15" data-quantity="0"
                                                                        data-multiple="1" id="r103391" required>
                                                                    <label class="radio-inline mt-10" for="r103391">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>



                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>El barrido y/o
                                                                        aspirado y el fregado del suelo ya est&aacute;
                                                                        incluido en la limpieza base, &iquest;quiere
                                                                        adem&aacute;s limpiar las juntas del suelo en
                                                                        caso de que &eacute;ste fuera de gres,
                                                                        m&aacute;rmol o terrazo?</strong> <span class="hastip"
                                                                        title="Las juntas son la uni&oacute;n de cemento o yeso de la plaqueta o loseta. Podremos usar t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question14" value="47449">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer14"
                                                                        class="answer" data-id="14" data-quantity="0"
                                                                        data-multiple="1" id="r103400" required checked>
                                                                    <label class="radio-inline mt-10" for="r103400">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="10.00" name="answer14"
                                                                        class="answer" data-id="14" data-quantity="0"
                                                                        data-multiple="1" id="r103401" required>
                                                                    <label class="radio-inline mt-10" for="r103401">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>




                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <a role="button" class="btn-continue" data-position-top="top3"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst4"
                                                            aria-controls="collapseFirst4" data-count-continue="3"><button
                                                                class="btn btn-success btn-lg pull-right btn-le-green mt-20">CONTINUAR</button></a>

                                                        <a role="button" class="btn-back" data-position-top="top2"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst2"
                                                            aria-controls="collapseFirst2" data-count-continue="2"
                                                            onclick="backScrollp('2' , '2')"><button class="btn btn-danger btn-lg pull-left btn-le-danger"
                                                                style="margin: 20px 0px;">VOLVER</button></a>




                                                    </div>
                                                </div>



                                            </div>
                                        </div>


                                    </div>





























































                                    <div class="panel panel-default mt-50" id="top4">
                                        <div class="panel-heading heading-title-zone boder-color-green" role="tab" id="heading4">
                                            <h4 class="panel-title">

                                                <h3 class="heading-text-zone"><strong>VENTANAS Y CERRAMIENTOS
                                                        EXTERIORES</strong><span id="tick-4" class="tick-success"></span></h3>


                                            </h4>
                                        </div>

                                        <div id="collapseFirst4" class="panel-collapse collapse in" role="tabpanel"
                                            aria-labelledby="heading4">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <p></p>








                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpie el interior de las persianas ? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntas quiere
                                                                        limpiar? </strong> <span class="hastip" title="Limpieza de la persiana por la zona interior de la vivienda y dependiendo del material de la persiana, podremos usar t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question13" value="47451">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer13"
                                                                        class="answer" data-id="13" data-quantity="1"
                                                                        data-multiple="0" id="r103404" required checked>
                                                                    <label class="radio-inline mt-10" for="r103404">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.50" name="answer13"
                                                                        class="answer" data-id="13" data-quantity="1"
                                                                        data-multiple="0" id="r103405" required>
                                                                    <label class="radio-inline mt-10" for="r103405">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity13" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="13">
                                                            </div>


                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47451"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47451">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/iWzST_rZg6U?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpien persianas venecianas o mallorquinas
                                                                        en caso de tenerlas? Si es as&iacute;,
                                                                        &iquest;cu&aacute;ntas quiere limpiar?</strong>
                                                                    <span class="hastip" title="Entendemos por persianas venecianas o mallorquinas aquellas que est&aacute;n formadas por l&aacute;minas individuales, las cu&aacute;les se pueden regular para permitir el paso de la luz. No se desmontar&aacute;n para realizar la limpieza"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question12" value="47455">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer12"
                                                                        class="answer" data-id="12" data-quantity="1"
                                                                        data-multiple="0" id="r103412" required checked>
                                                                    <label class="radio-inline mt-10" for="r103412">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer12"
                                                                        class="answer" data-id="12" data-quantity="1"
                                                                        data-multiple="0" id="r103413" required>
                                                                    <label class="radio-inline mt-10" for="r103413">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity12" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="12">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>La limpieza de
                                                                        vidrios y cristales de las ventanas normales
                                                                        (con una sola l&iacute;nea de cristales sin
                                                                        contraventanas ni dobles ventanas) ya
                                                                        est&aacute; incluida en la limpieza base,
                                                                        &iquest;quiere adem&aacute;s que se limpien los
                                                                        marcos y ra&iacute;les de estas ventanas? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntas quiere
                                                                        limpiar?</strong> <span class="hastip" title="Eliminaci&oacute;n de la suciedad de los carriles donde se asientan las ventanas, as&iacute; como los marcos de los cristales"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question11" value="47452">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer11"
                                                                        class="answer" data-id="11" data-quantity="1"
                                                                        data-multiple="0" id="r103406" required checked>
                                                                    <label class="radio-inline mt-10" for="r103406">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.50" name="answer11"
                                                                        class="answer" data-id="11" data-quantity="1"
                                                                        data-multiple="0" id="r103407" required>
                                                                    <label class="radio-inline mt-10" for="r103407">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity11" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="11">
                                                            </div>


                                                        </div>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <a data-toggle="collapse" href="#collapse47452"
                                                                    aria-expanded="false" aria-controls="collapse"
                                                                    class="view-link-media">Ver cómo se hace</a>
                                                                <div class="collapse" id="collapse47452">
                                                                    <div class="row mt-20">
                                                                        <div class="col-xs-12">
                                                                            <div class="video-responsive">
                                                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/LN74PSA44RQ?rel=0"
                                                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                                                    allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Tiene doble
                                                                        ventana (una ventana delante de la otra) o
                                                                        contra ventanas (puertas de madera o metal que
                                                                        se ponen en la parte exterior de las ventanas)?
                                                                        Si es as&iacute;, &iquest;cu&aacute;ntas quiere
                                                                        limpiar?</strong> <span class="hastip" title="Limpieza de cristales de dobles ventanas as&iacute; como sus bordes, marcos, carriles donde se asientan las ventanas y recovecos. En las contraventanas la limpieza se realizar&aacute; siempre que se pueda acceder a la parte exterior"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question10" value="47454">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer10"
                                                                        class="answer" data-id="10" data-quantity="1"
                                                                        data-multiple="0" id="r103410" required checked>
                                                                    <label class="radio-inline mt-10" for="r103410">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer10"
                                                                        class="answer" data-id="10" data-quantity="1"
                                                                        data-multiple="0" id="r103411" required>
                                                                    <label class="radio-inline mt-10" for="r103411">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity10" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="10">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>












                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpien ventanas grandes, miradores o
                                                                        puertas de cristal (hasta un m&aacute;ximo de
                                                                        2.5m de altura y anchura, y cuyo acceso a la
                                                                        superficie exterior a limpiar no est&eacute; a
                                                                        m&aacute;s de 1.5m desde el acceso)? Si es
                                                                        as&iacute;, &iquest;cu&aacute;ntos quiere
                                                                        limpiar? Si supera estas medidas deje marcado
                                                                        &quot;no&quot; y lea el &quot;m&aacute;s
                                                                        info&quot;</strong> <span class="hastip" title="Para que le demos un presupuesto adicional concreto por los ventanales o miradores que superen esas medidas, nos deber&aacute; mandar fotograf&iacute;as respondiendo al presupuesto que le mandaremos por email una vez que haya completado esta calculadora online"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question9" value="47453">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer9"
                                                                        class="answer" data-id="9" data-quantity="1"
                                                                        data-multiple="0" id="r103408" required checked>
                                                                    <label class="radio-inline mt-10" for="r103408">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="3.00" name="answer9"
                                                                        class="answer" data-id="9" data-quantity="1"
                                                                        data-multiple="0" id="r103409" required>
                                                                    <label class="radio-inline mt-10" for="r103409">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>




                                                            <div class="col-xs-12 col-sm-3">
                                                                <input type="number" class="form-control quantity dnone mt-10"
                                                                    name="quantity9" min="1" placeholder="" value="1"
                                                                    required data-show-quantity="9">
                                                            </div>


                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>





































                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <a role="button" class="btn-continue" data-position-top="top4"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst5"
                                                            aria-controls="collapseFirst5" data-count-continue="4"><button
                                                                class="btn btn-success btn-lg pull-right btn-le-green mt-20">CONTINUAR</button></a>

                                                        <a role="button" class="btn-back" data-position-top="top3"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst3"
                                                            aria-controls="collapseFirst3" data-count-continue="3"
                                                            onclick="backScrollp('3' , '3')"><button class="btn btn-danger btn-lg pull-left btn-le-danger"
                                                                style="margin: 20px 0px;">VOLVER</button></a>




                                                    </div>
                                                </div>



                                            </div>
                                        </div>


                                    </div>





























































                                    <div class="panel panel-default mt-50" id="top5">
                                        <div class="panel-heading heading-title-zone boder-color-green" role="tab" id="heading5">
                                            <h4 class="panel-title">

                                                <h3 class="heading-text-zone"><strong>TERRAZAS, EXTERIORES Y TENDEDEROS</strong><span
                                                        id="tick-5" class="tick-success"></span></h3>


                                            </h4>
                                        </div>

                                        <div id="collapseFirst5" class="panel-collapse collapse in" role="tabpanel"
                                            aria-labelledby="heading5">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <p></p>





                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se cepillen las paredes quitando el polvo, las
                                                                        telara&ntilde;as y pelusas? Si tienen grafitti,
                                                                        holl&iacute;n, humo, u otras manchas muy
                                                                        incrustadas, deje marcado &quot;no&quot; y lea
                                                                        el &quot;m&aacute;s info&quot;</strong> <span
                                                                        class="hastip" title="En caso de que haya grafitti, holl&iacute;n, humo u otras manchas muy incrustadas en la pared, habr&aacute; un suplemento que le indicaremos despu&eacute;s de que nos mande fotograf&iacute;as respondiendo al presupuesto que le enviaremos por email una vez que haya completado esta calculadora online"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question8" value="47459">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer8"
                                                                        class="answer" data-id="8" data-quantity="0"
                                                                        data-multiple="1" id="r103420" required checked>
                                                                    <label class="radio-inline mt-10" for="r103420">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer8"
                                                                        class="answer" data-id="8" data-quantity="0"
                                                                        data-multiple="1" id="r103421" required>
                                                                    <label class="radio-inline mt-10" for="r103421">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpien los armarios por el interior y el
                                                                        exterior?</strong> <span class="hastip" title="Si los armarios est&aacute;n llenos, se vac&iacute;an para poder limpiar el interior y posteriormente se introducir&aacute;n los objetos. Si hay alguna caja se limpiar&aacute; superficialmente por fuera "><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question7" value="47457">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer7"
                                                                        class="answer" data-id="7" data-quantity="0"
                                                                        data-multiple="1" id="r103416" required checked>
                                                                    <label class="radio-inline mt-10" for="r103416">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.75" name="answer7"
                                                                        class="answer" data-id="7" data-quantity="0"
                                                                        data-multiple="1" id="r103417" required>
                                                                    <label class="radio-inline mt-10" for="r103417">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se limpie la barandilla y/o per&iacute;metro de
                                                                        la terraza (ya sea de cristal, metal o
                                                                        azulejo)?</strong> <span class="hastip" title="Para la limpieza de esta zona usaremos t&eacute;cnicas de cepillado o vaporeta seg&uacute;n los operarios lo consideren dependiendo del material y del estado en el que se encuentre la barandilla o per&iacute;metro de la terraza"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question6" value="47458">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer6"
                                                                        class="answer" data-id="6" data-quantity="0"
                                                                        data-multiple="1" id="r103418" required checked>
                                                                    <label class="radio-inline mt-10" for="r103418">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.25" name="answer6"
                                                                        class="answer" data-id="6" data-quantity="0"
                                                                        data-multiple="1" id="r103419" required>
                                                                    <label class="radio-inline mt-10" for="r103419">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Quiere que
                                                                        se barra, aspire y friegue el suelo?</strong>
                                                                    <span class="hastip" title="En el caso de los suelos de c&eacute;sped artificial, solo se aspirar&aacute; para eliminar el polvo. En ning&uacute;n caso est&aacute; incluida la hidrolimpiadora. "><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question5" value="47456">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer5"
                                                                        class="answer" data-id="5" data-quantity="0"
                                                                        data-multiple="1" id="r103414" required checked>
                                                                    <label class="radio-inline mt-10" for="r103414">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="1.00" name="answer5"
                                                                        class="answer" data-id="5" data-quantity="0"
                                                                        data-multiple="1" id="r103415" required>
                                                                    <label class="radio-inline mt-10" for="r103415">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>




















































                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <a role="button" class="btn-continue" data-position-top="top5"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst6"
                                                            aria-controls="collapseFirst6" data-count-continue="5"><button
                                                                class="btn btn-success btn-lg pull-right btn-le-green mt-20">CONTINUAR</button></a>

                                                        <a role="button" class="btn-back" data-position-top="top4"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst4"
                                                            aria-controls="collapseFirst4" data-count-continue="4"
                                                            onclick="backScrollp('4' , '4')"><button class="btn btn-danger btn-lg pull-left btn-le-danger"
                                                                style="margin: 20px 0px;">VOLVER</button></a>




                                                    </div>
                                                </div>



                                            </div>
                                        </div>


                                    </div>





























































                                    <div class="panel panel-default mt-50" id="top6">
                                        <div class="panel-heading heading-title-zone boder-color-green" role="tab" id="heading6">
                                            <h4 class="panel-title">

                                                <h3 class="heading-text-zone"><strong>ZONAS GENERALES</strong><span id="tick-6"
                                                        class="tick-success"></span></h3>


                                            </h4>
                                        </div>

                                        <div id="collapseFirst6" class="panel-collapse collapse in" role="tabpanel"
                                            aria-labelledby="heading6">
                                            <div class="panel-body">


                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <p></p>


                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>Quiere que limpiemos
                                                                        restos de yeso, pintura o lechada en suelo,
                                                                        rodapi&eacute;s, ventanas, radiadores, puertas,
                                                                        etc? Seleccione nada si no hay nada o no quiere
                                                                        que lo limpiemos, poco si tiene algunas gotitas
                                                                        muy dispersas en pocos lugares, medio si hay
                                                                        bastantes gotitas en varios lugares y mucho si
                                                                        hay bastantes restos porque el operario no tuvo
                                                                        cuidado de proteger de manchas la vivienda.</strong>
                                                                    <span class="hastip" title="Para eliminar este tipo de restos no basta con una limpieza superficial, sino que es necesario emplear m&aacute;s tiempo y productos para cada superficie"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question4" value="47424">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer4"
                                                                        class="answer" data-id="4" data-quantity="0"
                                                                        data-multiple="1" id="r103343" required checked>
                                                                    <label class="radio-inline mt-10" for="r103343">
                                                                        Nada
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="2.00" name="answer4"
                                                                        class="answer" data-id="4" data-quantity="0"
                                                                        data-multiple="1" id="r103344" required>
                                                                    <label class="radio-inline mt-10" for="r103344">
                                                                        Poco
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="4.00" name="answer4"
                                                                        class="answer" data-id="4" data-quantity="0"
                                                                        data-multiple="1" id="r103345" required>
                                                                    <label class="radio-inline mt-10" for="r103345">
                                                                        Medio
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="6.00" name="answer4"
                                                                        class="answer" data-id="4" data-quantity="0"
                                                                        data-multiple="1" id="r103346" required>
                                                                    <label class="radio-inline mt-10" for="r103346">
                                                                        Mucho
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong> &iquest;Tiene usted
                                                                        mascotas en la vivienda?</strong> <span class="hastip"
                                                                        title="Mascotas como perros, gatos, etc que est&eacute;n sueltos en casa y pueden soltar pelo adem&aacute;s de ensuciar las diferentes zonas de su hogar"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question3" value="47426">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer3"
                                                                        class="answer" data-id="3" data-quantity="0"
                                                                        data-multiple="1" id="r103350" required checked>
                                                                    <label class="radio-inline mt-10" for="r103350">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="5.00" name="answer3"
                                                                        class="answer" data-id="3" data-quantity="0"
                                                                        data-multiple="1" id="r103351" required>
                                                                    <label class="radio-inline mt-10" for="r103351">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Son ustedes
                                                                        fumadores en su domicilio o tienen alguna zona
                                                                        de fumadores? Responda s&iacute;, s&oacute;lo
                                                                        si fuma dentro del inmueble.</strong> <span
                                                                        class="hastip" title="El humo del tabaco tiende a dejar amarillentas ventanas, puertas, azulejos  y otras superficies de la vivienda; limpiar restos de nicotina es m&aacute;s complejo que si no se fuma en la vivienda"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question2" value="47428">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer2"
                                                                        class="answer" data-id="2" data-quantity="0"
                                                                        data-multiple="1" id="r103354" required checked>
                                                                    <label class="radio-inline mt-10" for="r103354">
                                                                        No
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="5.00" name="answer2"
                                                                        class="answer" data-id="2" data-quantity="0"
                                                                        data-multiple="1" id="r103355" required>
                                                                    <label class="radio-inline mt-10" for="r103355">
                                                                        S&iacute;
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>


















                                                        <div class="row mt-20">
                                                            <div class="col-xs-12">
                                                                <p class="question-budget"><strong>&iquest;Cu&aacute;l
                                                                        es el estado general de la vivienda? Entendemos
                                                                        por poco sucia cuando los ventanales,
                                                                        electrodom&eacute;sticos, ba&ntilde;os y suelos
                                                                        se limpian a fondo al menos 1 vez al mes. Muy
                                                                        sucia ser&iacute;a cuando estas zonas llevan
                                                                        m&aacute;s de 6 meses sin limpiarse a fondo. Si
                                                                        la vivienda est&aacute; muy deteriorada, clique
                                                                        en m&aacute;s info. </strong> <span class="hastip"
                                                                        title="Si hay mucho yeso, cemento, adhesivos, pintura, polvo, grasa, holl&iacute;n, cal, mohos, pelo de animales, basura o trastos desperdigados, nicotina o suciedad pegajosa adherida a objetos, zonas muy ennegrecidas del suelo, electrodom&eacute;sticos muy sucios o ventanas que lleven muchos meses sin limpiarse a fondo, etc, esta calculadora no es v&aacute;lida. Llame al tel&eacute;fono 24h: 91 0325262 para presupuesto personalizado"><small
                                                                            class="text-primary"><strong>más info</strong></small></span></p>


                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <input type="hidden" name="question1" value="47429">

                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="0.00" name="answer1"
                                                                        class="answer" data-id="1" data-quantity="0"
                                                                        data-multiple="1" id="r103356" required checked>
                                                                    <label class="radio-inline mt-10" for="r103356">
                                                                        Poco sucia
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="4.00" name="answer1"
                                                                        class="answer" data-id="1" data-quantity="0"
                                                                        data-multiple="1" id="r103357" required>
                                                                    <label class="radio-inline mt-10" for="r103357">
                                                                        Medio sucia
                                                                    </label>
                                                                </div>

                                                            </div>



                                                            <div class="col-xs-12 col-sm-3">

                                                                <div class="radio-button">
                                                                    <input type="radio" value="8.00" name="answer1"
                                                                        class="answer" data-id="1" data-quantity="0"
                                                                        data-multiple="1" id="r103358" required>
                                                                    <label class="radio-inline mt-10" for="r103358">
                                                                        Muy sucia
                                                                    </label>
                                                                </div>

                                                            </div>






                                                        </div>



                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="line-bottom"></p>
                                                            </div>
                                                        </div>























































                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-xs-12">



                                                        <button class="btn btn-success btn-lg pull-right btn-le-green mt-20"
                                                            type="submit" id="submit">SIGUIENTE</button>

                                                        <a role="button" class="btn-back" data-position-top="top5"
                                                            data-toggle="collapse" data-parent="#accordion" href="#collapseFirst5"
                                                            aria-controls="collapseFirst5" onclick="backScrollp('5', '5')"><button
                                                                class="btn btn-danger btn-lg pull-left btn-le-danger"
                                                                style="margin: 20px 0px;">VOLVER</button></a>




                                                    </div>
                                                </div>



                                            </div>
                                        </div>


                                    </div>



































                                </div>
                            </div>
                        </div>

                        <input type="hidden" value="2107" name="budget_id" id="budget_id">
                        <input type="hidden" value="18" name="province_id" id="province_id">

                    </form>

                    <input type="hidden" value="" id="hour_price">
                    <input type="hidden" value="" id="base_hour">
                    <input type="hidden" value="" id="weight">
                    <input type="hidden" value="" id="hidden_type_home">


                </div>

            </div>
        </div>
    </div>

    <div class="chek-price" style="background-color: #026da2; width: 230px;height: 70px; position: fixed; bottom: 0px; right: 0px; padding: 10px; color: #fff;z-index: 999; display: none;">
        <div class="row">
            <div class="col-xs-12 text-center">
                <span style="font-size: 13px;">Su presupuesto aproximado es de:</span>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 text-center priceService">
                <span id="priceService">0.00</span>€
            </div>
        </div>
    </div>

    <a href="javascript:;" id="scrollToTop">&#x25B2;</a>


    <script type="text/javascript">
        // Set to false if opt-in required
        var trackByDefault = true;

        function acEnableTracking() {
            var expiration = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
            document.cookie = "ac_enable_tracking=1; expires= " + expiration + "; path=/";
            acTrackVisit();
        }

        function acTrackVisit() {
            var trackcmp_email = '';
            var trackcmp = document.createElement("script");
            trackcmp.async = true;
            trackcmp.type = 'text/javascript';
            trackcmp.src = '//trackcmp.net/visit?actid=65774532&e=' + encodeURIComponent(trackcmp_email) + '&r=' +
                encodeURIComponent(document.referrer) + '&u=' + encodeURIComponent(window.location.href);
            var trackcmp_s = document.getElementsByTagName("script");
            if (trackcmp_s.length) {
                trackcmp_s[0].parentNode.appendChild(trackcmp);
            } else {
                var trackcmp_h = document.getElementsByTagName("head");
                trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
            }
        }

        if (trackByDefault || /(^|; )ac_enable_tracking=([^;]+)/.test(document.cookie)) {
            acEnableTracking();
        }
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/slider.navbar.right.js"></script>
    <script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/tooltips.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/jquery.tooltip.js"></script>
    <script>
        $(window).on('load', function () {
            $('#cover').fadeOut(1000);

        });

        $('.hastip').powerTip({
            smartPlacement: true
        });

        $(document).ready(function () {

            $('form').keypress(function (e) {
                if (e == 13) {
                    return false;
                }
            });

            $('input').keypress(function (e) {
                if (e.which == 13) {
                    return false;
                }
            });

            /*window.onbeforeunload = function (e) {

            	var e = e || window.event;

            	if (e) {

            		$.ajax({
              			type: 'get',
              			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
              			async: false
              		});
            		console.log('Abandono ejecutado');
            	}

            }*/

            window.setTimeout(function () {

                if (InfoUserLE.email && InfoUserLE.list_id_ac == "118") {
                    $.ajax({
                        type: 'get',
                        url: InfoUserLE.route + '/' + InfoUserLE.email + '/' +
                            'customerInteraction',
                        async: false
                    });
                } else if (InfoUserLE.email && InfoUserLE.list_id_ac == "119") {

                    $.ajax({
                        type: 'get',
                        url: InfoUserLE.route + '/' + InfoUserLE.email + '/' +
                            'customerInteractionEmpties',
                        async: false
                    });
                }


            }, 900000);
        });
    </script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js">
    </script>

    <script>
        $.validate({
            form: '#forms',
            lang: 'es',
        });
    </script>

    <script type="text/javascript">
        var validNavigationClose = 0;

        function endSession() {
            console.log('CLOSED');

            $.ajax({
                type: 'get',
                url: InfoUserLE.route + '/' + InfoUserLE.email + '/' + 'closedWindow',
                async: false
            });
        }

        function bindDOMEvents() {
            console.log('INIT ' + validNavigationClose);
            /*

            * unload works on both closing tab and on refreshing tab.

            */

            $(window).on('beforeunload', function () {
                if (validNavigationClose == 0) {
                    endSession();
                }
            })

            /*$(window).on('unload',function()
            {
               if (validNavigationClose==0)
               {
                 endSession();
               }
            });*/

            // Attach the event keypress to exclude the F5 refresh
            $(document).keydown(function (e) {
                var key = e.which || e.keyCode;
                if (key == 116) {
                    validNavigationClose = 1;
                }
            });

            // Attach the event click for all links in the page
            $("a").bind("click", function () {
                validNavigationClose = 1;
            });

            // Attach the event submit for all forms in the page
            $("form").bind("submit", function () {
                validNavigationClose = 1;
            });

            // Attach the event click for all inputs in the page
            $("input[type=submit]").bind("click", function () {
                validNavigationClose = 1;
            });

        }

        // Wire up the events as soon as the DOM tree is ready
        $(document).ready(function () {
            bindDOMEvents();
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <script type="text/javascript">
        var base_url = "https://presupuesto-limpieza-online.limpiezasexpress.com";
        var province_id = $('#province_id').val();
        var budget_id = $('#budget_id').val();

        function checkFirstContinue() {

            /*$('body,html').stop(true,true).animate({
            				scrollTop: $('#firstContinue').offset().top

            			},500);*/

            var m2_home = $('input[name="m2_home"]').val();

            var type_home = $('input[name="type_home"]:checked').data('home');

            $('#hidden_type_home').val(type_home);

            getBaseHour(m2_home, budget_id);
            getWeight(m2_home, budget_id);


            var type_home = $('input[name="type_home"]').is(':checked') ? $('input[name="type_home"]').val() : 0;
            var m2_home = $('input[name="m2_home"]').val();
            var stants = $('select[name="stants"]').val();


            if (!type_home || !m2_home || !stants) {


                alert('Debe rellenar los campos anteriores para poder continuar');
                return false;
            } else {
                if (m2_home < 35 || m2_home > 180) {


                    alert('Los metros cuadrados de la vivienda deben ser como mínimo 35 y máximo 180');
                    return false;

                }
            }

            $('body,html').animate({
                scrollTop: $('#firstContinue').offset().top
            }, 500);

            showCalculateBudget();
        }
    </script>

    <script type="text/javascript">
        /*$(window).scroll(function(){
	      if ($(this).scrollTop() > 135) {
	          $('#task_flyout').addClass('fixed');
	      } else {
	          $('#task_flyout').removeClass('fixed');
	      }
	  });*/
    </script>



    <script>
        $(document).ready(function () {

            $('.ytp-right-controls').on('click', function () {

                return false;
            });

            $('#scrollToTop').bind("click", function () {
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
                return false;
            });

            showQuantityQuestion();
            chechFirstQuestionEnableContinue();

            hourPrice(province_id);

            $('.type_home').on('click', function () {
                $('#hidden_type_home').val($(this).data('home'));

                answerCheked();
            });
            $('#m2_home').on('change', function () {
                var m2_home = $('#m2_home').val();

                getBaseHour(m2_home, budget_id);
                getWeight(m2_home, budget_id);

                setTimeout(function () {
                    answerCheked();
                }, 2000);
            });

            $("[data-quantity = '1']").on("click", checkedQuatity);
            $("").on("click ", checkedQuatity);


        });
        $('.answer').on('click', answerCheked);
        $('.quantity').on('change', answerCheked);
        $('.quantity').on('change', function (event) {

            if ($(this).val() == 0) {

                $(this).val('1');
                alert('El valor no debe de ser 0');
                return false;
            }
        });

        function hourPrice(province_id) {
            $.get(base_url + '/service/hour-price/' + province_id, function (data) {
                $('#hour_price').val(data);
            });
        }

        function getWeight(m2_home, budget_id) {

            if (m2_home) {
                $.get(base_url + '/service/weight/' + m2_home + '/' + budget_id, function (data) {
                    $('#weight').val(data);
                    console.log(data);
                });
            }

        }

        function getBaseHour(m2_home, budget_id) {
            if (m2_home) {
                $.get(base_url + '/service/hour-base/' + m2_home + '/' + budget_id, function (data) {
                    $('#base_hour').val(data.base_hour);
                });
            }

        }

        function showQuantityQuestion() {
            $("[data-quantity = '1']:checked").each(function () {
                if ($(this).val() > 0) {
                    $("[data-show-quantity = " + $(this).data('id') + "]").fadeIn().removeClass('dnone');
                } else {
                    $("[data-show-quantity = " + $(this).data('id') + "]").fadeOut().addClass('dnone');
                }
            });
        }

        function checkedQuatity() {
            if ($(this).val() > 0) {
                $("[data-show-quantity = " + $(this).data('id') + "]").fadeIn().removeClass('dnone');
            } else {
                $("[data-show-quantity = " + $(this).data('id') + "]").fadeOut().addClass('dnone');
            }
        }

        function answerCheked() {

            var value_answer = 0;
            var value_answer_quantity = 0;
            var value_answer_not_multiple = 0;
            var price;

            $('input.answer:checked').each(function () {
                var data_id = $(this).data('id'); // Lo sacamos para concatenar
                var data_multiple = $(this).data('multiple'); // Lo sacamos para concatenar
                if ($(this).data('quantity') == '0') {

                    if ($(this).data('multiple') == '1') {
                        var value = parseFloat($(this, 'input.answer:checked').val());
                        value_answer += value;
                    } else {

                        var value_not_multiple = parseFloat($(this, 'input.answer:checked').val());
                        value_answer_not_multiple += value_not_multiple;
                    }
                } else {

                    var value_quantity = (parseFloat($(this, 'input.answer:checked').val()) * parseFloat($(
                        'input[name="quantity' + data_id + '"]').val()));
                    value_answer_quantity += parseFloat(value_quantity);
                }
            });

            price = (parseFloat($('#base_hour').val()) + ((parseFloat(value_answer) * parseFloat($('#weight').val())) +
                parseFloat($('#hidden_type_home').val()) + parseFloat(value_answer_quantity) + parseFloat(
                    value_answer_not_multiple))) * parseFloat($('#hour_price').val());

            console.log(price);
            if (isNaN(price)) {
                var price = 0.00;
            }

            if (price < 180) {
                var price = 180;
            }

            //console.log(value_answer_not_multiple);
            $('#priceService').fadeOut();
            $('#priceService').html(price.toFixed(2)).fadeIn();

        }

        function chechFirstQuestionEnableContinue() {

            $('.first-question-enable-continue').on('click change', function () {

                var type_home = $('input[name="type_home"]:checked').val();
                var m2_home = $('input[name="m2_home"]').val();
                var stants = $('select[name="stants"]').val();



                if (type_home && m2_home && stants) {

                    if (m2_home >= 35 && m2_home <= 180) {

                        var elemento = $("#firstContinue").data('continue');


                        $('#firstContinue').attr('href', '#collapseFirst' + elemento);
                        $('#firstContinue').data('position-top', '#collapseFirst' + elemento);
                        $('#firstContinue').attr('data-toggle', 'collapse');
                        $('#firstContinue').attr('data-parent', '#accordion');
                    }


                }

            });
        }


        function showCalculateBudget() {

            $.get(base_url + '/service/reform-budget', function (data) {

                if (data.data == 1) {

                    $('.chek-price').css('display', 'block');
                    $('#scrollToBottom').css('display', 'block');

                    $('#priceService').html(data.price_service.toFixed(2)).fadeIn();
                    $('div[id="collapseFirst1"]').addClass('in');

                }
            });
        }


        function backScrollp(myEl, tick) {


            $('#tick-' + tick).css('display', 'none');

            $('body,html').animate({
                scrollTop: $('#top' + myEl).offset().top
            }, 500);
        }
    </script>

    <script src="https://code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
        crossorigin="anonymous"></script>


    <script>
        $(document).ready(function () {

            $('div[id*="collapse"]').removeClass('in');
            $(".btn-continue").on("click", function () {
                var myEl = $(this).data('position-top');
                $('body,html').animate({
                    scrollTop: $('#' + myEl).offset().top
                }, 500);

                var tick = $(this).data('count-continue');
                $('#tick-' + tick).css('display', 'block');

            });

        });
    </script>

</body>

</html>

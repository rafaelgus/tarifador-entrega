<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Elija fecha para su servicio de limpieza de inmuebles</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/favicon.png" />
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/style.css"> <!-- Resource style -->
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/modernizr.js"></script> <!-- Modernizr -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">

	<!-- tooltipster -->
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/jquery.tooltip.css">

	<!-- Confirmatiosn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/slider-navbar-right.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/css.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5048875-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-5048875-1');
	</script>

	<!-- Global site tag (gtag.js) - Google Ads: 991546510 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-991546510"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-991546510');
	</script>

	<script>
	    window.InfoUserLE = {"email":"nuevou@example.com","route":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com\/tracking\/event\/ac","route_home":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com","list_id_ac":"118"};
	</script>

	<script type="application/ld+json">
	    {
	        "@context": "http://schema.org/",
	        "@type": "Organization",
	        "name": "Limpiezas Express",
	        "url": "http://www.limpiezasexpress.com/",
	        "logo": "http://www.limpiezasexpress.com/uploads/1/0/2/8/1028263/1457599333.png",
	        "sameAs" : [
	        "https://www.facebook.com/LimpiezasExpress/",
	        "https://twitter.com/limpiezaexpress",
	        "https://www.youtube.com/user/LimpiezasExpress",
	        "https://plus.google.com/+Limpiezasexpress"
	        ]
	    }
	</script>

	    <style>
	    	#cover{
	    		background: url(https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/loading-gif.gif) no-repeat scroll center center #fff;
	    		position: fixed;
	    		height: 100%;
	    		width: 100%;
	    		overflow: hidden;
	    		z-index: 999;
	    		background-size: 140px;
	    	}
	    </style>

	<meta name="description" content="Elija la fecha de prestación del servicio de limpieza en su hogar de forma totalmente personalizada. Profesionales certificados. Consulte disponibilidad.">
	<meta name="keywords" content="limpieza profesional casas, limpieza pisos, empresa limpiar casas, empresas de limpieza para casas particulares">

	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/calendar.css" />
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/config.calendar.css" />

	<script type="application/ld+json">
	{
	  "@context" : "http://schema.org",
	  "@type" : "Product",
	  "name" : "Limpiezas a fondo profesionales",
	  "description" : "Limpiezas fin de obra, alquileres, casas habitadas, etc. Operarios, maquinaria y seguro de responsabilidad civil incluidos",
	  "url" : "https://presupuesto-limpieza-online.limpiezasexpress.com/",
	  "brand" : {
	    "@type" : "Brand",
	    "name" : "Limpiezas Express",
	    "logo" : "https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png"
	  },
	  "offers" : {
	    "@type" : "Offer",
	    "price" : "Desde 180€"
	  },
	  "aggregateRating" : {
	    "@type" : "AggregateRating",
	    "ratingValue" : "4,8",
	    "bestRating" : "5",
	    "worstRating" : "1",
	    "ratingCount" : "106"
	  }
	}
	</script>


</head>
<body>
	<div id="cover"></div>
	<nav class="navbar navbar-default" id="navbar">
	  <div class="container-fluid">
	    <div class="row">
	    	<div class="col-lg-12">
	    		<div class="navbar-header">
	    			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	    				<!-- <span class="sr-only">Toggle navigation</span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span> -->
	    				<i class="fa fa-phone" aria-hidden="true"></i>
	    			</button>
	    			<a href="https://presupuesto-limpieza-online.limpiezasexpress.com"><img src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png" alt="Logo Limpiezas Express" id="logo" class="img-responsive"></a>
	    			<!-- <a class="navbar-brand" href="" id="">Kode<strong>Working</strong></a> -->
	    		</div>

	    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    			<ul class="nav navbar-nav navbar-right">

	    					    				<li id="navbar-number"><span>Si tiene dudas sobre la contratación llame al: <a href="tel:+34910325262"><u>91 0325262</u></a></span></li>
	    					    				<!-- <li id="navbar-slider-call">

							<a id="menu-toggle" href="#" class="">¿Prefieres que te llamemos?</a>
								<div id="sidebar-wrapper">

									<a id="menu-close" href="#" class="pull-right toggle"><i class="glyphicon glyphicon-remove"></i></a>


									<h1>Slider navbar right</h1>
								</div>
	    				</li> -->

	    			</ul>
	    		</div>
	    	</div>
	    </div>
	  </div>
	</nav>


<div class="container-fluid">
	<section id="task_flyout">
		<nav>
			<ol class="cd-multi-steps text-bottom count">
				<li class="visited"><a href="javascript:;">Definir servicio</a></li>
				<li class="current"><em>Elegir fecha</em></li>
				<li class=""><em>Ver presupuesto</em></li>
				<li><em>Realizar reserva</em></li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-xs-12 col-lg-10 col-lg-offset-1">
			<div class="content-budget">
				<strong>Elija a continuación la fecha de prestación del servicio. Tenga en cuenta que los precios dependen de la demanda y podrán variar de un día para otro. </strong>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-lg-10 col-lg-offset-1">

			<div class="content-budget">

				<div class="row">

					<form method="POST" action="https://presupuesto-limpieza-online.limpiezasexpress.com/service/setup/3" accept-charset="UTF-8"><input name="_token" type="hidden" value="jCG0EBvfYO3HFLmzeThVvB5b1HZukcCG2VrCIDsu">
					<div class="col-xs-12 col-sm-6 col-md-4">

						<div class="row">
							<div class="col-xs-12">
								<div id="cRed"></div>
								Días ocupados / no disponibles <span class="hastip" title="Días en los que los equipos de operarios ya están asignados a otras reservas y por tanto, no se pueden prestar más servicios."><small class="text-primary"><strong>más info</strong></small></span>
							</div>
							<div class="col-xs-12">
								<div id="cYellow"></div>
								Días más caros <span class="hastip" title="Al igual que ocurre con los vuelos o las habitaciones de hotel, los precios serán más caros debido a la demanda y disponibilidad de los equipos de limpieza."><small class="text-primary"><strong>más info</strong></small></span>
							</div>
							<div class="col-xs-12">
								<div id="cGreen"></div>
								Días más baratos <span class="hastip" title="Al igual que ocurre con los vuelos o las habitaciones de hotel, los precios serán más baratos debido a la demanda y disponibilidad de los equipos de limpieza."><small class="text-primary"><strong>más info</strong></small></span>
							</div>

							<div class="col-xs-12">
								<div id="cWriter"></div>
								Días con precio estándar <span class="hastip" title="Días con precio habitual, es decir, sin ningún tipo de sobrecoste ni descuento."><small class="text-primary"><strong>más info</strong></small></span>
							</div>
						</div>


					</div>

					<div class="col-xs-12 col-sm-6 col-md-8">

						<div id="img-load-calendar" style="margin: 10px 0px;">
							<h5 class="text-center"><strong>En estos momentos todos nuestros servidores están trabajando para ti.</strong></h5>
							<img src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/loading-gif.gif" class="img-responsive center-block" width="140px" id="">
						</div>

						<div id="dates" style="display: none;" class="mt-20">
							<input datimg-load-calendara-format="yy-mm-dd" type="text" required value="" style="display: none;" name="date" id="dateSelect">
						</div>

						<a href="https://presupuesto-limpieza-online.limpiezasexpress.com/service/setup/3" class="link-not-date pull-right">Clique aquí si aún no tiene una fecha definida</a>
						<!-- <div class="content-info-text">
							<span class="infomation-icon"><i class="fa fa-info"></i></span>
							<p>Los días verdes son nuestros días de preferencia, por eso te haremos un 10% de descuento.</p>
						</div>
						 -->
					</div>
				</div>

				<div class="row mt-20">

					<!-- <div class="col-xs 12">
						<a onclick="history.back()" class="a-button"><button class="btn btn-danger btn-lg btn-le-danger center-block">VOLVER</button></a>


					</div> -->
					<div class="col-xs 12">
						<a href="https://presupuesto-limpieza-online.limpiezasexpress.com/service/setup/3" class="a-button"><button class="btn btn-success btn-lg btn-le-green center-block mt-20 continue pull-right" type="submit">SIGUIENTE</button></a>
					</div>

				</div>

				</form>

			</div>

		</div>

	</div>

</div>



	<script type="text/javascript">
		// Set to false if opt-in required
		var trackByDefault = true;

		function acEnableTracking() {
			var expiration = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
			document.cookie = "ac_enable_tracking=1; expires= " + expiration + "; path=/";
			acTrackVisit();
		}

		function acTrackVisit() {
			var trackcmp_email = '';
			var trackcmp = document.createElement("script");
			trackcmp.async = true;
			trackcmp.type = 'text/javascript';
			trackcmp.src = '//trackcmp.net/visit?actid=65774532&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
			var trackcmp_s = document.getElementsByTagName("script");
			if (trackcmp_s.length) {
				trackcmp_s[0].parentNode.appendChild(trackcmp);
			} else {
				var trackcmp_h = document.getElementsByTagName("head");
				trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
			}
		}

		if (trackByDefault || /(^|; )ac_enable_tracking=([^;]+)/.test(document.cookie)) {
			acEnableTracking();
		}
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/slider.navbar.right.js"></script>
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/tooltips.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/jquery.tooltip.js"></script>
	<script>

		$(window).on('load', function () {
		    $('#cover').fadeOut(1000);

		});

		$('.hastip').powerTip({
			smartPlacement: true
		});

		$(document).ready(function() {

		  $('form').keypress(function(e){
		    if(e == 13){
		      return false;
		    }
		  });

		  $('input').keypress(function(e){
		      if(e.which == 13){
		        return false;
		      }
		    });

			/*window.onbeforeunload = function (e) {

				var e = e || window.event;

				if (e) {

					$.ajax({
			  			type: 'get',
			  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
			  			async: false
			  		});
					console.log('Abandono ejecutado');
				}

			}*/

		  window.setTimeout(function(){

		  	if(InfoUserLE.email && InfoUserLE.list_id_ac == "118"){
		  		$.ajax({
		  			type: 'get',
		  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'customerInteraction',
		  			async: false
		  		});
		  	}else if(InfoUserLE.email && InfoUserLE.list_id_ac == "119"){

		  		$.ajax({
		  			type: 'get',
		  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'customerInteractionEmpties',
		  			async: false
		  		});
		  	}


		  }, 900000);
		});
	</script>

	<script
		src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js">
	</script>

	<script>
		$.validate({
			form : '#forms',
		   	lang: 'es',
		 });
	</script>

	<script type="text/javascript">
		var validNavigationClose = 0;

		function endSession()
		{
			console.log('CLOSED');

			$.ajax({
	  			type: 'get',
	  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
	  			async: false
	  		});
		}
		function bindDOMEvents() {
			console.log('INIT '+ validNavigationClose);
			/*

			* unload works on both closing tab and on refreshing tab.

			*/

			$(window).on('beforeunload', function(){
				if (validNavigationClose == 0)
				{
				  endSession();
				}
			})

			/*$(window).on('unload',function()
			{
			   if (validNavigationClose==0)
			   {
			     endSession();
			   }
			});*/

			// Attach the event keypress to exclude the F5 refresh
			$(document).keydown(function(e)
			{
			   var key=e.which || e.keyCode;
			   if (key == 116)
			   {
			     validNavigationClose = 1;
			   }
			});

			// Attach the event click for all links in the page
			$("a").bind("click", function()
			{
			   validNavigationClose = 1;
			});

			 // Attach the event submit for all forms in the page
			 $("form").bind("submit", function()
			{
			   validNavigationClose = 1;
			});

			 // Attach the event click for all inputs in the page
			 $("input[type=submit]").bind("click", function()
			{
			   validNavigationClose = 1;
			});

		}

		// Wire up the events as soon as the DOM tree is ready
		$(document).ready(function()
		{
		   bindDOMEvents();
		});
	</script>

	<script type="text/javascript" src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/moment.min.js"></script>
	<script>
		$(function(){
			$('.continue').on('click', function(e){
				var date = $("#dates").find("input").val();
				if(!date){

					$.alert({
					    title: 'ATENCIÓN',
					    content: 'Elija una fecha del calendario o clique sobre "no tengo una fecha definida"',
					});
					e.preventDefault();
				}
			});

		});
	</script>

	<script>
		$(function(){

			$('.ui-datepicker-calendar').on('click',function(){
			    alert('day with event');
			});

			setTimeout(loadCalendar, 1000);

			$.datepicker.regional['es'] = {
			 closeText: 'Cerrar',
			 prevText: '<Ant',
			 nextText: 'Sig>',
			 currentText: 'Hoy',
			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			 weekHeader: 'Sm',
			 dateFormat: "yy/mm/dd",
			 firstDay: 1,
			 isRTL: false,
			 multidate: false,
			 showMonthAfterYear: false,
			 yearSuffix: '',
			 duration: 'fast',
			 };
			 $.datepicker.setDefaults($.datepicker.regional['es']);

			var disabled_days = ["","2018/10/12","2018/11/01","","2018/09/17","2018/09/18","2018/09/19","2018/09/20","2018/09/21","2018/09/22","2018/09/23","2018/09/24"];

			var suplement_days = ["2018/09/30","2018/10/07","2018/10/14","2018/10/15"];

			var discount_days = ["2018/10/02","2018/10/03","2018/10/04","2018/10/05","2018/10/06","2018/10/08","2018/10/09","2018/10/10","2018/10/11","2018/10/13","2018/10/16","2018/10/17"];

			console.log(disabled_days);
			console.log(suplement_days);
			console.log(discount_days);
			function highlightDays(date) {

					for (var i = 0; i < disabled_days.length; i++ ){

						 for (var a = 0; a < suplement_days.length; a++){

						    for (var x = 0; x < discount_days.length; x++){

						    	if (new Date(disabled_days[i]).toString() == date.toString()) {


						    	    if(moment(disabled_days[i], "YYYY/MM/DD")._i >= moment(new Date(), "YYYY/MM/DD").format("YYYY/MM/DD")){

						    	    	return [false, 'disabled-day'];
						    	    }

						    	}

						    	if (new Date(suplement_days[a]).toString() == date.toString()) {

						    		if(moment(suplement_days[a], "YYYY/MM/DD")._i >= moment(new Date(), "YYYY/MM/DD").format("YYYY/MM/DD")){
						    			return [true, 'suplement-day'];
						    		}


						    	}

						    	if (new Date(discount_days[x]).toString() == date.toString()) {

						    		if(moment(discount_days[x], "YYYY/MM/DD")._i >= moment(new Date(), "YYYY/MM/DD").format("YYYY/MM/DD")){
						    			return [true, 'discount-day'];
						    		}


						    	}
    						}

						}
					}
					return [true, ''];
			}

			$('#dates').datepicker({
				clearBtn: true,
				language: "es",
				minDate: 0,
				beforeShowDay: highlightDays,
			 	duration: 'fast',
				onSelect: function(data){
					$('#dateSelect').val(data);
				}
			});
		});

		function loadCalendar(){

			$('#img-load-calendar').css('display', 'none');
			$('#dates').css('display', 'block');
		}
	</script>

</body>
</html>

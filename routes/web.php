<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('forma_inicial');
});
Route::view('/formainicial', 'forma_inicial');
Route::view('/presupuesto', 'presupuesto');
Route::get('/opcionpresupuesto/{id}', 'opcionesPresupuesto@index')->name('opcionpresupuesto.index');
Route::get('/guardaropcionespresupuesto/{id}', 'opcionesPresupuesto@update')->name('opciones.presupuesto');
Route::put('/guardaropcionespresupuesto/{id}', 'opcionesPresupuesto@update')->name('actualizaropciones.presupuesto');
Route::post('/guardarpresupuesto', 'PresupuestoController@store')->name('presupuesto');

Route::get('/fecha/{id}', 'PresupuestoFechaController@index')->name('opcionfecha.index');
Route::put('/fecha/{id}', 'PresupuestoFechaController@update')->name('opcionfecha.update');

Route::get('/solicitante/{id}', 'PresupuestoSolicitanteController@index')->name('solicitante.index');
Route::put('/solicitante/{id}', 'PresupuestoSolicitanteController@update')->name('solicitante.update');

Route::get('/verpresupuesto/{id}', 'PresupuestoSolicitanteController@show')->name('solicitante.show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




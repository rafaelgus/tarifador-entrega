<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Provincia;

class RegionController extends Controller
{
    public function region()
    {
        return Provincia::orderBy('name', 'ASC')->get(['id', 'name', 'precio', 'multiplica']);
    }
}

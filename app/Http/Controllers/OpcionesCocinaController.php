<?php

namespace App\Http\Controllers;

use App\Models\OpcionesCocina;
use Illuminate\Http\Request;

class OpcionesCocinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OpcionesCocina  $opcionesCocina
     * @return \Illuminate\Http\Response
     */
    public function show(OpcionesCocina $opcionesCocina)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OpcionesCocina  $opcionesCocina
     * @return \Illuminate\Http\Response
     */
    public function edit(OpcionesCocina $opcionesCocina)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OpcionesCocina  $opcionesCocina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpcionesCocina $opcionesCocina)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpcionesCocina  $opcionesCocina
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpcionesCocina $opcionesCocina)
    {
        //
    }
}

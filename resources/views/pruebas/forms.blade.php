<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Introduzca sus datos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/favicon.png" />
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/style.css"> <!-- Resource style -->
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/templates/frontend/breadcrumbs/modernizr.js"></script> <!-- Modernizr -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">

	<!-- tooltipster -->
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/jquery.tooltip.css">

	<!-- Confirmatiosn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/slider-navbar-right.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/css/css.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5048875-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-5048875-1');
	</script>

	<!-- Global site tag (gtag.js) - Google Ads: 991546510 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-991546510"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-991546510');
	</script>

	<script>
	    window.InfoUserLE = {"email":"nuevou@example.com","route":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com\/tracking\/event\/ac","route_home":"https:\/\/presupuesto-limpieza-online.limpiezasexpress.com","list_id_ac":"118"};
	</script>

	<script type="application/ld+json">
	    {
	        "@context": "http://schema.org/",
	        "@type": "Organization",
	        "name": "Limpiezas Express",
	        "url": "http://www.limpiezasexpress.com/",
	        "logo": "http://www.limpiezasexpress.com/uploads/1/0/2/8/1028263/1457599333.png",
	        "sameAs" : [
	        "https://www.facebook.com/LimpiezasExpress/",
	        "https://twitter.com/limpiezaexpress",
	        "https://www.youtube.com/user/LimpiezasExpress",
	        "https://plus.google.com/+Limpiezasexpress"
	        ]
	    }
	</script>

	    <style>
	    	#cover{
	    		background: url(https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/loading-gif.gif) no-repeat scroll center center #fff;
	    		position: fixed;
	    		height: 100%;
	    		width: 100%;
	    		overflow: hidden;
	    		z-index: 999;
	    		background-size: 140px;
	    	}
	    </style>

	<style>
		.input-group-addon{
			background: #FFF;
		}
		.fom-control{
			border-right: none !important;
			box-shadow: none !important;
		}

		#distances{
			font-size: 48px;
		}
	</style>

	<script type="application/ld+json">
	{
	  "@context" : "http://schema.org",
	  "@type" : "Product",
	  "name" : "Limpiezas a fondo profesionales",
	  "description" : "Limpiezas fin de obra, alquileres, casas habitadas, etc. Operarios, maquinaria y seguro de responsabilidad civil incluidos",
	  "url" : "https://presupuesto-limpieza-online.limpiezasexpress.com/",
	  "brand" : {
	    "@type" : "Brand",
	    "name" : "Limpiezas Express",
	    "logo" : "https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png"
	  },
	  "offers" : {
	    "@type" : "Offer",
	    "price" : "Desde 180€"
	  },
	  "aggregateRating" : {
	    "@type" : "AggregateRating",
	    "ratingValue" : "4,8",
	    "bestRating" : "5",
	    "worstRating" : "1",
	    "ratingCount" : "106"
	  }
	}
	</script>


</head>
<body>
	<div id="cover"></div>
	<nav class="navbar navbar-default" id="navbar">
	  <div class="container-fluid">
	    <div class="row">
	    	<div class="col-lg-12">
	    		<div class="navbar-header">
	    			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	    				<!-- <span class="sr-only">Toggle navigation</span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span>
	    				<span class="icon-bar"></span> -->
	    				<i class="fa fa-phone" aria-hidden="true"></i>
	    			</button>
	    			<a href="https://presupuesto-limpieza-online.limpiezasexpress.com"><img src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/images/logo-limpiezas-express.png" alt="Logo Limpiezas Express" id="logo" class="img-responsive"></a>
	    			<!-- <a class="navbar-brand" href="" id="">Kode<strong>Working</strong></a> -->
	    		</div>

	    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    			<ul class="nav navbar-nav navbar-right">

	    					    				<li id="navbar-number"><span>Si tiene dudas sobre la contratación llame al: <a href="tel:+34910325262"><u>91 0325262</u></a></span></li>
	    					    				<!-- <li id="navbar-slider-call">

							<a id="menu-toggle" href="#" class="">¿Prefieres que te llamemos?</a>
								<div id="sidebar-wrapper">

									<a id="menu-close" href="#" class="pull-right toggle"><i class="glyphicon glyphicon-remove"></i></a>


									<h1>Slider navbar right</h1>
								</div>
	    				</li> -->

	    			</ul>
	    		</div>
	    	</div>
	    </div>
	  </div>
	</nav>


<div class="container-fluid">

	<section id="task_flyout">
		<nav>
			<ol class="cd-multi-steps text-bottom count">
				<li class="visited"><a href="javascript:;">Definir servicio</a></li>
				<li class="current"><em>Elegir fecha</em></li>
				<li class=""><em>Ver presupuesto</em></li>
				<li><em>Realizar reserva</em></li>
			</ol>
		</nav>
	</section>

	<div class="row" style="margin-top: -30px;">
		<div class="col-xs-12 col-lg-10 col-lg-offset-1">

			<div class="content-budget">

				<div class="row">
					<div class="col-xs-12 text-center">
						<h1>
							Introduzca sus datos para recibir su<br>presupuesto personalizado
						</h1>
					</div>
				</div>

				<div class="row mt-20">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">

						<!-- <div class="content-info-text">
							<span class="infomation-icon"><i class="fa fa-info"></i></span>
							<p></p>
						</div> -->

						<div class="alert alert-danger">
							<p><i class="fa fa-clock-o"></i> Actualmente este servicio est&aacute; siendo visto por 16 personas</p>
						</div>
					</div>
				</div>
				<form action="https://presupuesto-limpieza-online.limpiezasexpress.com/service/setup/4" method="POST" id="form-customer">

				<div class="row mt-20">
					<div class="col-xs-12 col-sm-6 col-sm-offset-3">

							<input type="hidden" name="_token" value="jCG0EBvfYO3HFLmzeThVvB5b1HZukcCG2VrCIDsu">
							<div class="form-group">

								<div class="input-group">
									<input type="text" name="name" class="form-control input-height fz-16 boder-color-green" data-validation="required" value="Prueba 5" placeholder="Nombre y apellidos" required>

									<div class="input-group-addon boder-color-green"><span class="hastip" title="Introduzca su nombre para enviarle su presupuesto personalizado"><small class="text-primary"><strong>más info</strong></small></span></div>
								</div>
							</div>

							<div class="form-group">

								<div class="input-group">
									<input type="email" name="email" class="form-control input-height fz-16 boder-color-green" data-validation="required" value="nuevou@example.com" placeholder="Email de contacto" required>


									<div class="input-group-addon boder-color-green"><span class="hastip" title="Introduzca un mail para poder recuperar su presupuesto"><small class="text-primary"><strong>más info</strong></small></span></div>
								</div>

							</div>

							<div class="form-group">

								<div class="input-group">
									<input type="text" name="phone" class="form-control input-height fz-16 boder-color-green" value="+58412204444" placeholder="Teléfono de contacto" data-validation="length" data-validation-length="min8" required>


									<div class="input-group-addon boder-color-green"><span class="hastip" title="Introduzca un teléfono válido para gestionar su servicio en caso de que reserve"><small class="text-primary"><strong>más info</strong></small></span></div>
								</div>
							</div>

							<div class="form-group">


								<div class="input-group">
									<input type="text" name="address" class="form-control input-height fz-16 boder-color-green" id="autocomplete" placeholder="Dirección prestación servicio" onFocus="geolocate()" data-validation="required" value="alicante" required>


									<div class="input-group-addon boder-color-green"><span class="hastip" title="Dirección concreta donde se prestará el servicio o al menos el municipio"><small class="text-primary"><strong>más info</strong></small></span></div>
								</div>

							</div>

							<div class="form-group">

								<label for="condition" style="font-weight: normal;"><input type="checkbox" name="condition" required id="condition"> Acepta usted que nuestra empresa MIB, S.L. con CIF B84920587 trate la información que nos facilita con el fín de prestarle el servicio solicitado, informarle sobre servicios relacionados, y si lo contrata, realizarle su facturación.</label>
							</div>

							<div  class="form-group">
								<a href="https://www.limpiezasexpress.com/condiciones-legales.html" target="_blank">Conozca los términos de uso, condiciones legales, y sus derechos</a>
							</div>

							<input type="hidden" name="lat" value="" id="lat">
							<input type="hidden" name="lon" value="" id="lon">

							<input type="hidden" name="distance_km" value="0" id="distance_km">

							<input type="hidden" name="format_string_to_array_distance_km" value="0" id="format_string_to_array_distance_km">

							<div id="distances"></div>
					</div>
				</div>

				<!-- <div class="row" style="margin-top: 80px;">
					<div class="col-xs-6 col-xs-offset-3">

							<button class="btn btn-danger btn-lg center-block"><a onclick="history.back()"> VOLVER </a> </button>

							<input class="btn btn-success btn-lg center-block" style="margin-top: 10px;" type="submit" value="CONTINUAR" id="submit">
					</div>
				</div> -->

				<div class="row mt-20">
					<div class="col-xs-12">
						<!-- <a onclick="history.back()"><button class="btn btn-danger btn-lg btn-le-danger mt-20">VOLVER</button></a> -->

						<input class="btn btn-success btn-lg btn-le-green mt-20 pull-right" type="submit" value="SIGUIENTE" id="submit">
					</div>
				</div>

				</form>

			</div>

		</div>

	</div>
</div>



	<script type="text/javascript">
		// Set to false if opt-in required
		var trackByDefault = true;

		function acEnableTracking() {
			var expiration = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
			document.cookie = "ac_enable_tracking=1; expires= " + expiration + "; path=/";
			acTrackVisit();
		}

		function acTrackVisit() {
			var trackcmp_email = '';
			var trackcmp = document.createElement("script");
			trackcmp.async = true;
			trackcmp.type = 'text/javascript';
			trackcmp.src = '//trackcmp.net/visit?actid=65774532&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
			var trackcmp_s = document.getElementsByTagName("script");
			if (trackcmp_s.length) {
				trackcmp_s[0].parentNode.appendChild(trackcmp);
			} else {
				var trackcmp_h = document.getElementsByTagName("head");
				trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
			}
		}

		if (trackByDefault || /(^|; )ac_enable_tracking=([^;]+)/.test(document.cookie)) {
			acEnableTracking();
		}
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/slider.navbar.right.js"></script>
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/tooltips.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
	<script src="https://presupuesto-limpieza-online.limpiezasexpress.com/assets/js/jquery.tooltip.js"></script>
	<script>

		$(window).on('load', function () {
		    $('#cover').fadeOut(1000);

		});

		$('.hastip').powerTip({
			smartPlacement: true
		});

		$(document).ready(function() {

		  $('form').keypress(function(e){
		    if(e == 13){
		      return false;
		    }
		  });

		  $('input').keypress(function(e){
		      if(e.which == 13){
		        return false;
		      }
		    });

			/*window.onbeforeunload = function (e) {

				var e = e || window.event;

				if (e) {

					$.ajax({
			  			type: 'get',
			  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
			  			async: false
			  		});
					console.log('Abandono ejecutado');
				}

			}*/

		  window.setTimeout(function(){

		  	if(InfoUserLE.email && InfoUserLE.list_id_ac == "118"){
		  		$.ajax({
		  			type: 'get',
		  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'customerInteraction',
		  			async: false
		  		});
		  	}else if(InfoUserLE.email && InfoUserLE.list_id_ac == "119"){

		  		$.ajax({
		  			type: 'get',
		  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'customerInteractionEmpties',
		  			async: false
		  		});
		  	}


		  }, 900000);
		});
	</script>

	<script
		src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js">
	</script>

	<script>
		$.validate({
			form : '#forms',
		   	lang: 'es',
		 });
	</script>

	<script type="text/javascript">
		var validNavigationClose = 0;

		function endSession()
		{
			console.log('CLOSED');

			$.ajax({
	  			type: 'get',
	  			url: InfoUserLE.route+'/'+InfoUserLE.email+'/'+'closedWindow',
	  			async: false
	  		});
		}
		function bindDOMEvents() {
			console.log('INIT '+ validNavigationClose);
			/*

			* unload works on both closing tab and on refreshing tab.

			*/

			$(window).on('beforeunload', function(){
				if (validNavigationClose == 0)
				{
				  endSession();
				}
			})

			/*$(window).on('unload',function()
			{
			   if (validNavigationClose==0)
			   {
			     endSession();
			   }
			});*/

			// Attach the event keypress to exclude the F5 refresh
			$(document).keydown(function(e)
			{
			   var key=e.which || e.keyCode;
			   if (key == 116)
			   {
			     validNavigationClose = 1;
			   }
			});

			// Attach the event click for all links in the page
			$("a").bind("click", function()
			{
			   validNavigationClose = 1;
			});

			 // Attach the event submit for all forms in the page
			 $("form").bind("submit", function()
			{
			   validNavigationClose = 1;
			});

			 // Attach the event click for all inputs in the page
			 $("input[type=submit]").bind("click", function()
			{
			   validNavigationClose = 1;
			});

		}

		// Wire up the events as soon as the DOM tree is ready
		$(document).ready(function()
		{
		   bindDOMEvents();
		});
	</script>

	<script>
	// This example displays an address form, using the autocomplete feature
	// of the Google Places API to help users fill in the information.
	/*getToolTips('1');*/

	var autocomplete;
	var get_distances = [];

	function initAutocomplete() {
	  // Create the autocomplete object, restricting the search to geographical
	  // location types.
	  autocomplete = new google.maps.places.Autocomplete(
	      /** @type  {!HTMLInputElement} */(document.getElementById('autocomplete')),
	      {types: ['geocode'],componentRestrictions: {country: 'es'}});

	  // When the user selects an address from the dropdown, populate the address
	  // fields in the form.

	  google.maps.event.addListener(autocomplete, 'place_changed', function () {
	        fillInAddress();
            setTimeout(function(){
            		$('#submit').attr('disabled', false).val('CONTINUAR');

            		//$('#distance_km').attr('value', $('#distances').text());

  			}, 6000);
	      });
	}

	function fillInAddress() {

		var place = autocomplete.getPlace();

		$('#lat').attr('value', place.geometry.location.lat());
		$('#lon').attr('value', place.geometry.location.lng());
		$('#submit').attr('disabled', true).val('Por favor espere...');

		mapquestAPIRouteMatrix(place);
		//var distances_group_1 = distancesMatrix(["40.657396,-4.686385"],place);

        //console.log(distances_group_1);
	}
	// [START region_geolocation]
	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
	  /*if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(function(position) {
	      var geolocation = {
	        lat: position.coords.latitude,
	        lng: position.coords.longitude
	      };
	      var circle = new google.maps.Circle({
	        center: geolocation,
	        radius: position.coords.accuracy
	      });
	      autocomplete.setBounds(circle.getBounds());
	    });
	  }*/
	}
	// [END region_geolocation]


	function distancesMatrix(origin, place){

		var distanceService = new google.maps.DistanceMatrixService();
		    distanceService.getDistanceMatrix({
		        origins: origin,
		        destinations: [{lat: place.geometry.location.lat(), lng: place.geometry.location.lng()}],
		        travelMode: google.maps.TravelMode.DRIVING,
		        unitSystem: google.maps.UnitSystem.METRIC,
		        durationInTraffic: true,
		        avoidHighways: false,
		        avoidTolls: true
		    },
		    function (response, status) {
		    	console.log(response);
		        if (status !== google.maps.DistanceMatrixStatus.OK) {
		            $('#result').html('Erros '+status).fadeIn();
		        } else {

		        	var get_distances = [];
		        	var origins = response.originAddresses;
		        	   var destinations = response.destinationAddresses;

		        	   for (var i = 0; i < origins.length; i++) {
		        	     var results = response.rows[i].elements;
		        	     for (var j = 0; j < results.length; j++) {
		        	       var element = results[j];
		        	      	var text = Object.values(element.distance)[0];
		        	      	var push = parseFloat(text.split(" ")[0].replace('.', ''));
		        	       get_distances.push(push);

		        	       var min = Math.min.apply(null, get_distances);

		        	      	//$('#distances').append(min+', ');
		        	      	$('#format_string_to_array_distance_km').attr('value',1);
		        	      	//$('#distances').html(min);

		        	       var from = origins[i];
		        	       var to = destinations[j];
		        	     }
		        	   }
		        }
		    });

	}

	function mapquestAPIRouteMatrix(place){

		L.mapquest.key = 'Q2YAOjMLntH3PS6Ap291BT8Jl80rSz1R';

		var origin = place.geometry.location.lat()+','+place.geometry.location.lng();

		var location = [origin, "40.657396,-4.686385"];
		console.log(location);
		var data = {
          'locations': [],
          'options': {
            'allToAll': false,
          	'manyToOne': true,
          }
        };
		data.locations = location;
		var directions = L.mapquest.directions();
		directions.routeMatrix(data, routeMatrixCallback)
	}

	function routeMatrixCallback(error, response) {
	  console.log(response);

	  $('#distance_km').attr('value',response.distance);
	  $('#format_string_to_array_distance_km').attr('value',1);
	  //$('#distances').html($('#distance_km').val());

	}
	    </script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSSzB4m32-hBr_SijnA9DlDceWXjPNBLc&libraries=places&callback=initAutocomplete"
	        async defer></script>

	<script src="https://api.mqcdn.com/sdk/mapquest-js/v1.3.1/mapquest.js"></script>


</body>
</html>

<?php

namespace App\Http\Controllers;

use App\Models\PresupuestoSolicitante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\PresupuestoEnviado;

use App\Models\Presupuesto;
use App\Models\PresupuestoFecha;
use App\Models\PresupuestoVivienda;
use App\Models\OpcionesBanoAseo;
use App\Models\OpcionesCocina;
use App\Models\OpcionesDormitorio;
use App\Models\OpcionesTerraza;
use App\Models\OpcionesVentana;

class PresupuestoSolicitanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $presup = Presupuesto::find($id);

        return view('datosusuario', ['presup' => $presup]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PreupuestoSolicitante  $preupuestoSolicitante
     * @return \Illuminate\Http\Response
     */
    public function show(PresupuestoSolicitante $presupuestoSolicitante, $id)
    {
        $presup = Presupuesto::find($id);
        $presupuesto = Presupuesto::where('id', $id)
            ->with([
                'presupuestoVivienda',
                'opcionesCocina',
                'opcionesBanoAseo',
                'opcionesDormitorio',
                'opcionesVentana',
                'opcionesTerraza',
                'presupuestoFecha',
                'presupuestoSolicitante'
            ])
            ->first();
        Mail::send(new PresupuestoEnviado($presupuesto));

        //dd([$presupuesto]);
        return view('presupuesto_show', ['presup' => $presup, 'presupuesto' => $presupuesto]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PreupuestoSolicitante  $preupuestoSolicitante
     * @return \Illuminate\Http\Response
     */
    public function edit(PresupuestoSolicitante $presupuestoSolicitante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PreupuestoSolicitante  $preupuestoSolicitante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PresupuestoSolicitante $presupuestoSolicitante, $id)
    {
        $presup = Presupuesto::find($id);

        $fecha = PresupuestoSolicitante::where('presupuesto_id', $id)->update([
            'nombre' => $request->nombre,
            'email' => $request->email,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
        ]);




        return redirect()->route('solicitante.show', ['id' => $presup->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PresupuestoSolicitante  $preupuestoSolicitante
     * @return \Illuminate\Http\Response
     */
    public function destroy(PresupuestoSolicitante $presupuestoSolicitante)
    {
        //
    }
}

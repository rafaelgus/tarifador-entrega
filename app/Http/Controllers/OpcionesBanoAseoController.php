<?php

namespace App\Http\Controllers;

use App\Models\OpcionesBanoAseo;
use Illuminate\Http\Request;

class OpcionesBanoAseoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OpcionesBanoAseo  $opcionesBanoAseo
     * @return \Illuminate\Http\Response
     */
    public function show(OpcionesBanoAseo $opcionesBanoAseo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OpcionesBanoAseo  $opcionesBanoAseo
     * @return \Illuminate\Http\Response
     */
    public function edit(OpcionesBanoAseo $opcionesBanoAseo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OpcionesBanoAseo  $opcionesBanoAseo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpcionesBanoAseo $opcionesBanoAseo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpcionesBanoAseo  $opcionesBanoAseo
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpcionesBanoAseo $opcionesBanoAseo)
    {
        //
    }
}

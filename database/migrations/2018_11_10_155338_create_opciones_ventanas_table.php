<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesVentanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_ventanas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vent_persianas_id');
            $table->string('vent_persianas_name');
            $table->decimal('vent_persianas_precio', 12, 2);
            $table->decimal('vent_persianas_multiplica', 4, 2);
            $table->integer('vent_persianas_venec_id');
            $table->string('vent_persianas_venec_name');
            $table->decimal('vent_persianas_venec_precio', 12, 2);
            $table->decimal('vent_persianas_venec_multiplica', 4, 2);
            $table->integer('vent_marco_railes_id');
            $table->string('vent_marco_railes_name');
            $table->decimal('vent_marco_railes_precio', 12, 2);
            $table->decimal('vent_marco_railes_multiplica', 4, 2);
            $table->integer('vent_cont_vent_id');
            $table->string('vent_cont_vent_name');
            $table->decimal('vent_cont_vent_precio', 12, 2);
            $table->decimal('vent_cont_vent_multiplica', 4, 2);
            $table->integer('vent_panoramica_id');
            $table->string('vent_panoramica_name');
            $table->decimal('vent_panoramica_precio', 12, 2);
            $table->decimal('vent_panoramica_multiplica', 4, 2);   
            $table->integer('presupuesto_id');
            $table->string('presup_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_ventanas');
    }
}

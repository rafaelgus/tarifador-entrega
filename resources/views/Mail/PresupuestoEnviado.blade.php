@component('mail::message') # Su presupuesto personalizado:


## Datos del servicio:
**Servicio:** {{ $presupuesto->servicio }}

**Subservicio:** {{ $presupuesto->subservicio }}

**Provincia:** {{ $presupuesto->provincia }}

**Fecha del Serviciop:** {{Carbon\Carbon::parse($presupuesto->presupuestoFecha->fecha)->format('d-M-Y')  }}

## Datos de la Vivienda

**Tipo de Vivienda:** {{ $presupuesto->presupuestoVivienda->tipo_vivienda_name }}

**Dimensiones:**  {{ $presupuesto->presupuestoVivienda->dimen_vivienda_name }} m2.

**Cantidad de estancias:** {{ $presupuesto->presupuestoVivienda->estanc_vivienda_name }}


# Total del Presupuesto: {{ $presupuesto->total_presupuesto}}  €

@component('mail::button', ['url'=> 'http://laravelcleaninstall.test/verpresupuesto/'.$presupuesto->id, 'color'=> 'green'])
Si quieres mas detalles

@endcomponent

## Gracias por presupuestar nuestros servicios

@endcomponent

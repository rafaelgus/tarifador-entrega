<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesTerrazasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_terrazas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('terra_cepill_pared_id');
            $table->string('terra_cepill_pared_name');
            $table->decimal('terra_cepill_pared_precio', 12, 2);
            $table->decimal('terra_cepill_pared_multiplica', 4, 2);
            $table->integer('terra_arm_int_ext_id');
            $table->string('terra_arm_int_ext_name');
            $table->decimal('terra_arm_int_ext_precio', 12, 2);
            $table->decimal('terra_arm_int_ext_multiplica', 4, 2);
            $table->integer('terra_baranda_id');
            $table->string('terra_baranda_name');
            $table->decimal('terra_baranda_precio', 12, 2);
            $table->decimal('terra_baranda_multiplica', 4, 2);
            $table->integer('terra_suelo_id');
            $table->string('terra_suelo_name');
            $table->decimal('terra_suelo_precio', 12, 2);
            $table->decimal('terra_suelo_multiplica', 4, 2);    
            $table->integer('presupuesto_id');
            $table->string('presup_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_terrazas');
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Servicio;

class ServiciosController extends Controller
{
    public function servicios()
    {
        return Servicio::orderBy('name', 'ASC')->get(['id', 'name', 'precio', 'multiplica']);
    }

    public function subservicios(Servicio $servicio)
    {
        return $servicio->subservicios()->orderBy('name', 'ASC')->get(['id', 'name', 'precio', 'multiplica']);
    }
}

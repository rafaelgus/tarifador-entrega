<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;

class OpcionesVentana extends Model
{
    protected $table = 'opciones_ventanas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'vent_persianas_id',
        'vent_persianas_name',
        'vent_persianas_precio',
        'vent_persianas_multiplica',
        'vent_persianas_venec_id',
        'vent_persianas_venec_name',
        'vent_persianas_venec_precio',
        'vent_persianas_venec_multiplica',
        'vent_marco_railes_id',
        'vent_marco_railes_name',
        'vent_marco_railes_precio',
        'vent_marco_railes_multiplica',
        'vent_cont_vent_id',
        'vent_cont_vent_name',
        'vent_cont_vent_precio',
        'vent_cont_vent_multiplica',
        'vent_panoramica_id',
        'vent_panoramica_name',
        'vent_panoramica_precio',
        'vent_panoramica_multiplica',
        'presupuesto_id',
        'presup_numero',
        'vent_secc_presup_total',

    ];



     /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function opcionesterrazas()
    {
        return $this->belongsTo(Presupuesto::class);
    }

}

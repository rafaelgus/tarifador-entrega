<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\Models\PresuestoVivienda;
use App\Models\OpcionesCocina;
use App\Models\OpcionesBanoAseo;
use App\Models\OpcionesDormitorio;
use App\Models\OpcionesVentana;
use App\Models\OpcionesTerraza;
use App\Models\PresuestoFecha;
use App\Models\PresuestoSolicitante;

class PresupuestoAdmin extends Model
{
    use CrudTrait;
    protected $table = 'presupuestos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'numero_presup',
        'servicio_id',
        'servicio',
        'servicio_precio',
        'subservicio_id',
        'subservicio',
        'subservicio_precio',
        'provincia_id',
        'provincia',
        'provincia_precio',
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function presupuestoVivienda()
    {
        return $this->hasMany(PresupuestoVivienda::class);
    }

    public function opcionesCocina()
    {
        return $this->hasMany(OpcionesCocina::class);
    }

    public function opcionesBanoAseo()
    {
        return $this->hasMany(OpcionesBanoAseo::class);
    }

    public function opcionesDormitorio()
    {
        return $this->hasMany(OpcionesDormitorio::class);
    }

    public function opcionesVentana()
    {
        return $this->hasMany(OpcionesVentana::class);
    }

    public function opcionesTerraza()
    {
        return $this->hasMany(OpcionesTerraza::class);
    }

    public function presupuestoFecha()
    {
        return $this->hasMany(PresupuestoVivienda::class);
    }

    public function presupuestoSolicitante()
    {
        return $this->hasMany(PresupuestoSolicitante::class);
    }

}

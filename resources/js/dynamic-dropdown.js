$(function () {
    dynamicDropdown('/api/servicios', '#servicio_id');

    $('#servicio_id').change(function () {
        let url = `/api/subservicios/${this.value}`;
        let target = '#subservicio_id';
        dynamicDropdown(url, target);
    });

    $('#subservicio_id').change(function () {
        let url = `/api/provincia/`;
        let target = '#region_id';
        dynamicDropdown(url, target);
    });

});


function dynamicDropdown(url, selector) {
    $.get(url, function (data) {

        let $select = $(selector);

        $select.find('option').not(':first').remove();

        let options = [];

        $.each(data, function (index, item) {
            options.push(`<option value="${item.id}|${item.name}|${item.precio}">${item.name}</option>`);
        })

        $select.append(options);
    })
};



/* $(function () {
    dynamicDropdownProvincia('/api/provincia', '#region_id');
}); */


/* function dynamicDropdownProvincia(url, selector) {
    $.get(url, function (data) {

        let $select = $(selector);

        $select.find('option').not(':first').remove();

        let options = [];

        $.each(data, function (index, item) {
            options.push(`<option value="${item.id}">${item.region_name}</option>`);
        })

        $select.append(options);
    })
} */

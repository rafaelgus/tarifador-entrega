<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesBanoAseosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_bano_aseos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ban_int_arm_id');
            $table->string('ban_int_arm_name');
            $table->decimal('ban_int_arm_precio', 12, 2);
            $table->decimal('ban_int_arm_multiplica', 4, 2);
            $table->integer('ban_mamp_id');
            $table->string('ban_mamp_name');
            $table->decimal('ban_mamp_precio', 12, 2);
            $table->decimal('ban_mamp_multiplica', 4, 2);
            $table->integer('ban_sanit_id');
            $table->string('ban_sanit_name');
            $table->decimal('ban_sanit_precio', 12, 2);
            $table->decimal('ban_sanit_multiplica', 4, 2);
            $table->integer('ban_azuljta_pared_id');
            $table->string('ban_azuljta_pared_name');
            $table->decimal('ban_azuljta_pared_precio', 12, 2);
            $table->decimal('ban_azuljta_pared_multiplica', 4, 2);
            $table->integer('ban_hongos_id');
            $table->string('ban_hongos_name');
            $table->decimal('ban_hongos_precio', 12, 2);
            $table->decimal('ban_hongos_multiplica', 4, 2);
            $table->integer('ban_sueljta_id');
            $table->string('ban_sueljta_name');
            $table->decimal('ban_sueljta_precio', 12, 2);
            $table->decimal('ban_sueljta_multiplica', 4, 2);
            $table->integer('presupuesto_id');
            $table->string('presup_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_bano_aseos');
    }
}

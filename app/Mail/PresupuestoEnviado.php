<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Presupuesto;

class PresupuestoEnviado extends Mailable
{
    use Queueable, SerializesModels;


    public $presupuesto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Presupuesto $presupuesto)
    {
        $this->presupuesto = $presupuesto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->presupuesto->presupuestoSolicitante->email, $this->presupuesto->presupuestoSolicitante->nombre)
            ->subject('Presupuesto:'. " " .$this->presupuesto->numero_presup)
            ->markdown('Mail.PresupuestoEnviado');
    }
}
